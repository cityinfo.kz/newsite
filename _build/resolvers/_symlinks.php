<?php
/** @var xPDOTransport $transport */
/** @var array $options */
/** @var modX $modx */
if ($transport->xpdo) {
    $modx =& $transport->xpdo;
    $dev = MODX_BASE_PATH . 'newSite/';
    /** @var xPDOCacheManager $cache */
    $cache = $modx->getCacheManager();
    if (file_exists($dev) && $cache) {
        if (!is_link(MODX_ASSETS_PATH . 'components/newsite')) {
            $cache->deleteTree(
                MODX_ASSETS_PATH . 'components/newsite/',
                ['deleteTop' => true, 'skipDirs' => false, 'extensions' => []]
            );
            echo $dev . 'assets/components/newsite';
            symlink($dev . 'assets/components/newsite', MODX_ASSETS_PATH . 'components/newsite');
        }
        if (!is_link(MODX_CORE_PATH . 'components/newsite')) {
            $cache->deleteTree(
                MODX_CORE_PATH . 'components/newsite/',
                ['deleteTop' => true, 'skipDirs' => false, 'extensions' => []]
            );
            symlink($dev . 'core/components/newsite', MODX_CORE_PATH . 'components/newsite');
        }
    }
}

return true;