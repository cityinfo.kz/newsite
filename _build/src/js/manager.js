import axios from 'axios';
import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/images/marker-shadow.png';
import MicroModal from 'micromodal';
import { mapInit } from './exchange';

const API_URL = '/assets/components/newsite/action.php';

function removeDisable(data) {
  for (let i = 0; i < data.length; i++) {
    document.getElementById(data[i]).removeAttribute('disabled');
  }
}

function addDisable(data) {
  for (let i = 0; i < data.length; i++) {
    document.getElementById(data[i]).setAttribute('disabled', '');
  }
}

function getExchangePoints(cityId) {
  const data = new FormData();
  data.append('city', cityId);
  data.append('action', 'courses/list');
  try {
    return axios.post(API_URL, data, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        charset: 'UTF-8',
      },
    });
  } catch (error) {
    throw new Error(error);
  }
}

function fillExchangePointsSelect(points) {
  let output = '<option value="-1">Выберите обменный пункт</option>';
  for (let i = 0; i < points.length; i++) {
    const line = points[i];
    const gross = line.gross === '1' ? 'ОПТ' : 'РОЗНИЦА';
    output +=
      '<option value="' +
      line.id +
      '">' +
      line['user_id'] +
      ' - ' +
      gross +
      '-' +
      line.name +
      '</option>';
  }
  const exchange = document.getElementById('exchange');
  exchange.innerHTML = output;
  removeDisable(['exchange']);
}

function showResponseMessage(response) {
  const data = response.data;
  // Success!
  const divMessage = document.getElementById('message');
  const exchange = document.getElementById('exchange');
  if (data.success === true) {
    document.getElementById('exchangeData').style.display = 'none';
    divMessage.style.display = 'block';
    divMessage.classList.add(
      'bg-green-100',
      'border-green-400',
      'text-green-700'
    );
    divMessage.innerHTML = data.message;
    exchange.selectedIndex = 0;
    const citySelect = document.getElementById('city');
    getExchangePoints(citySelect.value)
      .then(function(response) {
        fillExchangePointsSelect(response.data);
      })
      .catch(function(error) {
        console.log(error);
      });
  } else {
    document.getElementById('exchangeData').style.display = 'none';
    divMessage.style.display = 'block';
    divMessage.classList.add('bg-red-100', 'border-red-400', 'text-red-700');
    divMessage.innerHTML = data.message;
    exchange.selectedIndex = 0;
  }
}

function checkboxHandler() {
  if (this.checked === true) {
    this.value = 1;
  } else {
    this.value = 0;
  }
}

document.addEventListener('DOMContentLoaded', function() {
  const citySelect = document.getElementById('city');
  if (citySelect) {
    const form = new FormData();
    form.append('cities', 'true');
    form.append('action', 'courses/list');
    axios
      .post(API_URL, form, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          charset: 'UTF-8',
        },
      })
      .then(function(response) {
        const data = response.data;
        let output = '';
        for (let i = 0; i < data.length; i++) {
          const line = data[i];
          output +=
            '<option value="' + line.id + '">' + line.name + '</option>';
        }
        citySelect.innerHTML += output;
      })
      .catch(console.log);

    // Следим за checkbox
    const checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (const checkbox of checkboxes) {
      checkbox.addEventListener('change', checkboxHandler);
    }

    // Сохранение данных
    const saveButton = document.getElementById('saveButton');
    saveButton.addEventListener('click', function() {
      const exchangeData = document.getElementById('exchangeData');
      const inputs = exchangeData.getElementsByTagName('input');
      const selects = exchangeData.getElementsByTagName('select');
      const formDataObject = {};
      for (const input of inputs) {
        formDataObject[input.name] = input.value;
      }
      for (const select of selects) {
        formDataObject[select.name] = select.value;
      }
      if (Object.keys(formDataObject).length !== 0) {
        const form = new FormData();
        form.append('data', JSON.stringify(formDataObject));
        form.append('action', 'courses/update');
        axios
          .post(API_URL, form, {
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              charset: 'UTF-8',
            },
          })
          .then(showResponseMessage)
          .catch(console.log);
      }
    });

    // Удаление данных
    const deleteButton = document.querySelector('#deleteButton');
    deleteButton.addEventListener('click', function() {
      const form = new FormData();
      form.append('action', 'courses/delete');
      form.append('id', document.querySelector('#exchange').value);

      axios
        .post(API_URL, form, {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            charset: 'UTF-8',
          },
        })
        .then(showResponseMessage)
        .catch(console.log);
    });
  }
});

/**
 * Событие по отслеживанию изменений в DOM
 * При выборе города - снимать класс у селекта обменников и кнопки сохранить
 * При сбросе города - делать селект обменников и кнопку сохранить - disabled
 */
const observeDOM = (function() {
  const MutationObserver =
      window.MutationObserver || window.WebKitMutationObserver,
    eventListenerSupported = window.addEventListener;

  return function(obj, callback) {
    if (MutationObserver) {
      // define a new observer
      const obs = new MutationObserver(function(mutations, observer) {
        if (mutations[0].addedNodes.length || mutations[0].removedNodes.length)
          callback();
      });
      // have the observer observe foo for changes in children
      obs.observe(obj, { childList: true, subtree: true });
    } else if (eventListenerSupported) {
      obj.addEventListener('DOMNodeInserted', callback, false);
      obj.addEventListener('DOMNodeRemoved', callback, false);
    }
  };
})();

const exchangeElement = document.getElementById('exchange');
exchangeElement &&
  observeDOM(exchangeElement, function() {
    const exchange = document.getElementById('exchange');
    exchange.addEventListener('change', function() {
      const divMessage = document.getElementById('message');
      divMessage.style.display = 'none';
      const data = new FormData();
      data.append('exchange', this.value);
      data.append('action', 'courses/get');
      if (this.value !== '-1') {
        axios
          .post(API_URL, data, {
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              charset: 'UTF-8',
            },
          })
          .then(function(response) {
            const data = response.data[0];
            for (const key in data) {
              if (data.hasOwnProperty(key)) {
                const el = document.querySelector('[name="' + key + '"]');
                if (el !== undefined && el !== null) {
                  if (
                    key === 'day_and_night' ||
                    key === 'published' ||
                    key === 'hidden' ||
                    key === 'gross'
                  ) {
                    if (+data[key] === 1) {
                      el.checked = true;
                      el.value = 1;
                    } else {
                      el.checked = false;
                      el.value = 0;
                    }
                  } else {
                    if (key === 'atms') {
                      el.selectedIndex = 0;
                      if (data[key]) {
                        el.value = data[key];
                      }
                    } else {
                      el.value = data[key];
                    }
                  }
                }
              }
            }
            removeDisable(['saveButton', 'showModalButton']);
            document.getElementById('exchangeData').style.display = 'block';
            mapInit('manager-map');
          })
          .catch(function(error) {
            console.log(error);
          });
      } else {
        addDisable(['saveButton', 'showModalButton']);
        document.getElementById('exchangeData').style.display = 'none';
      }
    });
  });
const cityElement = document.getElementById('city');

cityElement &&
  observeDOM(cityElement, function() {
    const citySelect = document.getElementById('city');
    citySelect.addEventListener('change', function() {
      const divMessage = document.getElementById('message');
      divMessage.style.display = 'none';
      document.getElementById('exchangeData').style.display = 'none';
      if (this.value !== '-1') {
        try {
          getExchangePoints(this.value).then(function(response) {
            fillExchangePointsSelect(response.data);
          });
        } catch (error) {
          console.log(error);
        }
      } else {
        const exchange = document.getElementById('exchange');
        exchange.selectedIndex = 0;
        addDisable(['exchange', 'saveButton', 'showModalButton']);
        document.getElementById('exchangeData').style.display = 'none';
      }
    });
  });

document.addEventListener('DOMContentLoaded', function() {
  MicroModal.init();
});
