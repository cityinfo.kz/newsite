import axios from 'axios';
import AutoComplete from './auto-complete';

const searchUrl = '/service/search.html';
let timerID = 0;
const showSpinner = () => {
  const spinner = document.querySelector('#spinner');
  spinner.style.display = 'block';
};
const hideSpinner = () => {
  const spinner = document.querySelector('#spinner');
  spinner.style.display = 'none';
};
const setSearchResults = companies => {
  if (Array.isArray(companies)) {
    let rows = '';
    for (const company of companies) {
      rows += `
    <div class="w-full sm:w-1/2 p-2">
        <a
            class="block search-result p-1 cursor-pointer border border-gray-400 hover:bg-gray-100 hover:shadow"
            href="/company-card.html?id=${company.id}">
            <div class="text-black text-center text-lg leading-none w-full py-2 mb-1">${company.name}</div>
            <div class="flex items-center">
                <div class="flex items-center px-2">
                    <img src="/${company.logo}" class="object-fill" alt="${company.name}">
                </div>
                <div class="leading-normal">
                    <div class="ml-2">`;
      if (company.adress) {
        rows += `<p class="text-gray-700">Адрес: ${company.adress}</p>`;
      }
      if (company.keyword) {
        rows += `<p class="text-gray-700">Совпадение: ${company.keyword}</p>`;
      }
      rows += `</div>
                </div>
            </div>
        </a>
    </div>
`;
    }
    const container = document.querySelector('.search-results');
    container.innerHTML = rows;
  }
};

const getCategoryButtons = () => {
  return document.querySelectorAll('.search-category');
};

const setActiveCategory = catID => {
  const categories = getCategoryButtons();
  const activeClass = 'active';
  for (const category of categories) {
    if (+category.dataset.id === +catID) {
      if (!category.classList.contains(activeClass)) {
        category.classList.add(activeClass);
      }
    } else {
      if (category.classList.contains(activeClass)) {
        category.classList.remove(activeClass);
      }
    }
  }
};

const categoryClick = catID => {
  showSpinner();
  setActiveCategory(catID);
  if (typeof yaCounter35354850 !== 'undefined') {
    // todo update textContent
    const yaData = { category_click: catID };
    yaCounter35354850.reachGoal('category_click', yaData);
  }
  const data = new FormData();
  data.append('idCat', catID);
  return axios
    .post(searchUrl, data)
    .then(response => {
      const companies = response.data;
      if (companies.length) {
        setSearchResults(companies);
      }
    })
    .catch(error => {
      console.log('error', error);
    })
    .finally(() => {
      hideSpinner();
    });
};

const search = value => {
  const formData = new FormData();
  formData.append('s_jq', value);
  showSpinner();
  axios
    .post(searchUrl, formData)
    .then(response => {
      setSearchResults(response.data);
    })
    .catch(e => {
      console.log(e);
    })
    .finally(() => {
      hideSpinner();
    });
};

const redirectWithKeyword = keyword => {
  const params = new URLSearchParams();
  if (keyword.length > 2) {
    params.set('keyword', keyword);
  }
  const divider = params.toString().length ? '?' : '';
  location.href = '/search-page.html' + divider + params.toString();
};

const scrollToResult = () => {
  const categories = document.querySelectorAll('.search-category');
  const count = categories.length;
  if (count) {
    categories[count - 1].scrollIntoView({
      block: 'start',
      behavior: 'smooth',
    });
  }
};

document.addEventListener('DOMContentLoaded', () => {
  const searchInput = document.getElementById('searchInput');
  const searchButton = document.getElementById('searchButton');
  const searchInputHead = document.getElementById('searchInputHead');
  const searchButtonHead = document.getElementById('searchButtonHead');

  const categories = getCategoryButtons();
  const searchParams = new URLSearchParams(window.location.search);
  const catID = searchParams.get('catID');
  if (catID) {
    categoryClick(catID).then(() => {
      scrollToResult();
    });
  }
  const keyword = searchParams.get('keyword');
  if (keyword && keyword.length > 2) {
    searchInput.value = keyword;
    search(keyword);

    scrollToResult();
  }

  if (categories) {
    for (const category of categories) {
      category.addEventListener(
        'click',
        categoryClick.bind(this, category.dataset.id)
      );
    }
  }

  searchButton &&
    searchButton.addEventListener('click', () => {
      timerID && clearTimeout(timerID);
      timerID = setTimeout(() => {
        showSpinner();
        const data = new FormData();
        data.append('s_jq', searchInput.value);
        axios
          .post(searchUrl, data)
          .then(response => {
            setSearchResults(response.data);
          })
          .catch(e => {
            console.log(e);
          })
          .finally(() => {
            hideSpinner();
          });
      }, 100);
    });
  searchInput &&
    searchInput.addEventListener('keyup', e => {
      if (e.code === 'Enter' && e.target.value.length > 2) {
        timerID && clearTimeout(timerID);
        timerID = setTimeout(() => {
          search(e.target.value);
        }, 300);
      }
    });
  if (searchInput) {
    window.AC = new AutoComplete({
      selector: searchInput,
      cache: false,
      source: function(term, response) {
        if (term.length > 2) {
          showSpinner();
          axios
            .get(searchUrl + '?term=' + term)
            .then(result => {
              response(result.data);
            })
            .catch(error => {
              console.log(error);
            })
            .finally(() => {
              hideSpinner();
            });
        }
      },
      renderItem: function(item, search) {
        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
        const re = new RegExp('(' + search.split(' ').join('|') + ')', 'gi');
        return (
          '<div class="autocomplete-suggestion" data-val="' +
          item +
          '">' +
          item.replace(re, '<b>$1</b>') +
          '</div>'
        );
      },
      onSelect: function(e, term, item) {
        const value = item.getAttribute('data-val');
        search(value);
      },
      delay: 300,
    });
  }

  searchInputHead &&
    searchInputHead.addEventListener('keyup', e => {
      if (e.code === 'Enter') {
        redirectWithKeyword(e.target.value);
      }
    });

  searchButtonHead &&
    searchButtonHead.addEventListener('click', e => {
      e.preventDefault();
      const value = (searchInputHead && searchInputHead.value) || '';
      redirectWithKeyword(value);
    });
});
