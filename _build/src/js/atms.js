let atmMap;

function getCard(name, address, wh, bank) {
  return (
    '<div><p class="mb-2"><b>' +
    name +
    '</b></p><p class="mb-2"><b>Адрес: </b>' +
    address +
    '</p><p class="mb-2"><b>Часы работы: </b>' +
    wh +
    '</p><p class="mb-2"><b>Банк: </b>' +
    bank +
    '</p></div>'
  );
}

// Получение АТМ по банку
function getAtms(newBank) {
  const searchParams = new URLSearchParams(window.location.search);
  const bank = newBank || searchParams.get('bank') || 'all';
  // удаляем все объекты с карты
  atmMap.geoObjects.removeAll();
  const { terminals, names, colors } = window.atms;
  for (const key in terminals) {
    // создаем колекцию геоОбъектов
    const collection = new ymaps.GeoObjectCollection(null, {
      preset: colors[key],
    });
    // добавляем коллекцию к карте
    atmMap.geoObjects.add(collection);
    if (bank && bank !== 'all') {
      if (key !== bank) continue;
    }
    const atms = terminals[key];
    for (const atm of atms) {
      // Добавляем метку
      const placemark = new ymaps.Placemark([atm.lon, atm.lat], {
        balloonContent: getCard(atm.name, atm.address, atm.wh, names[key]),
      });
      // добавляем в коллекцию
      collection.add(placemark);
    }
  }
  // добавляем на карту и корректируем зум
  atmMap
    .setBounds(atmMap.geoObjects.getBounds(), { checkZoomRange: true })
    .then(function() {
      if (atmMap.getZoom() > 15) atmMap.setZoom(15);
    });
}

const setActiveAtm = atms => {
  const searchParams = new URLSearchParams(window.location.search);
  const selectedBank = searchParams.get('bank');
  if (atms && selectedBank) {
    for (const atm of atms) {
      if (atm.dataset.bank === selectedBank) {
        atm.classList.remove('bg-blue-500');
        atm.classList.add('bg-orange-500');
      } else {
        atm.classList.remove('bg-orange-500');
        atm.classList.add('bg-blue-500');
      }
    }
  }
};

// получение АТМ банка
function getAtmsByBank() {
  const bank = this.dataset.bank;
  const params = new URLSearchParams();
  if (bank) {
    params.set('bank', bank);
    window.history.replaceState(
      {},
      document.title,
      window.location.pathname + '?' + params.toString()
    );
  }
  const atms = document.querySelectorAll('.atm');
  setActiveAtm(atms);
  getAtms(bank);
}

function init() {
  // Создание экземпляра карты.
  atmMap = new ymaps.Map('atmMap', {
    center: [49.962089, 82.61101],
    zoom: 10,
    type: 'yandex#map',
    controls: ['zoomControl'],
    behaviors: ['default', 'scrollZoom'],
  });
  getAtms();
}

function domReady() {
  const atms = document.querySelectorAll('.atm');
  if (atms) {
    setActiveAtm(atms);
    for (const atm of atms) {
      atm.addEventListener('click', getAtmsByBank);
    }
  }
  typeof ymaps !== 'undefined' && ymaps.ready(init);
}

document.addEventListener('DOMContentLoaded', domReady);
