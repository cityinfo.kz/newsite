import axios from 'axios';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/images/marker-shadow.png';
import MicroModal from 'micromodal';

let leafletMap;
let markersLayer;

const apiUrl = '/assets/components/newsite/action.php';

const addMarker = e => {
  // Add marker to map at click location;
  markersLayer.clearLayers();
  const marker = L.marker(e.latlng);
  const lon = document.getElementById('longitude');
  if (lon) {
    lon.value = e.latlng.lng;
  }
  const lat = document.getElementById('latitude');
  if (lat) {
    lat.value = e.latlng.lat;
  }

  markersLayer.addLayer(marker).addTo(leafletMap);
};

const initMarkers = () => {
  let lat = document.getElementById('latitude').value;
  let lon = document.getElementById('longitude').value;
  if (lat && lon) {
    lat = parseFloat(lat.replace(/,/, '.'));
    lon = parseFloat(lon.replace(/,/, '.'));
    if (lat > 0 && lon > 0) {
      addMarker({ latlng: { lat: lat, lng: lon } });
      leafletMap.setView([lat, lon], 15);
    }
  }
};

const mapInit = (containerId = 'map') => {
  const map = document.getElementById(containerId);
  if (map) {
    const lat = 49.94950784554141;
    const lon = 82.61629666457671;
    if (leafletMap) {
      markersLayer.clearLayers();
    } else {
      leafletMap = L.map(containerId, {
        center: [lat, lon],
        zoom: 13,
        scrollWheelZoom: false,
      });
      markersLayer = new L.LayerGroup();
    }
    initMarkers();
    leafletMap.on('click', addMarker);

    L.tileLayer('https://osm.apihit.com/?z={z}&x={x}&y={y}&s={s}', {
      attribution:
        'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a>',
      maxZoom: 18,
    }).addTo(leafletMap);
  }
};

function deleteExchangePoint(e) {
  e.preventDefault();
  const formData = new FormData(this);
  axios
    .post(apiUrl, formData)
    .then(function(response) {
      if (response.data.success === true) {
        location.href = response.data.data.data.redirect;
      }
    })
    .catch(function(error) {
      console.log(error);
    });
}

function saveExchangePoint(e) {
  e.preventDefault();
  const form = document.querySelector('#form');
  const inputs = form.querySelectorAll('input');
  // const map = document.querySelector('#map');

  const result = document.querySelector('#info-result');
  result.innerHTML = '';
  result.classList.remove(
    'hidden',
    'bg-red-100',
    'border-red-400',
    'text-red-700'
  );

  // map.classList.remove('border', 'border-red-400');
  for (let i = 0; i < inputs.length; i++) {
    inputs[i].classList.remove('bg-red-100', 'border-red-400', 'text-red-700');
  }

  const errors = form.querySelectorAll('.error');
  for (let i = 0; i < errors.length; i++) {
    errors[i].innerHTML = '';
  }
  const formData = new FormData(form);
  axios
    .post(apiUrl, formData)
    .then(response => {
      if (response.data.success === false) {
        result.classList.add('bg-red-100', 'border-red-400', 'text-red-700');
        result.innerHTML = 'Ошибка при сохранении формы!';
        const errors = response.data.data.data;
        for (const key in errors) {
          const field = document.querySelector('#' + key);
          field.classList.add('border-red-500');
          if (!field.classList.contains('border')) {
            field.classList.add('border');
          }
          field.nextElementSibling.innerText = errors[key];
        }
      } else {
        result.classList.add(
          'bg-green-100',
          'border-green-400',
          'text-green-700'
        );
        result.innerHTML = 'Данные успешно сохранены!';
        location.href = window.redirectUrl;
      }
    })
    .catch(function(error) {
      console.log(error);
    });
}

function changeType(e) {
  const type = e.target.value;
  let toHide = 'retail';
  if (toHide === type) {
    toHide = 'gross';
  }
  const postfix = 'Points';

  const form = document.getElementById('massUpdateForm');
  if (form) {
    const toHideElements = form.getElementsByClassName(toHide + postfix);
    if (toHideElements.length) {
      toHideElements[0].style.display = 'none';
    }
    const toShowElements = form.getElementsByClassName(type + postfix);
    if (toShowElements.length) {
      toShowElements[0].style.removeProperty('display');
    }
    const toReset = form.querySelectorAll('input[name="points[]"]');
    if (toReset.length) {
      for (const checkbox of toReset) {
        checkbox.checked = false;
      }
    }
  }
}

function saveMassUpdateForm(e) {
  e.preventDefault();
  const updateStatusElement = document.querySelector('#massUpdateStatus');
  updateStatusElement.classList.remove(
    'bg-red-100',
    'border-red-400',
    'text-red-700',
    'hidden'
  );
  const form = document.querySelector('#massUpdateForm');
  const formData = new FormData(form);
  axios
    .post(apiUrl, formData)
    .then(response => {
      if (!response.data.success) {
        updateStatusElement.classList.add(
          'bg-red-100',
          'border-red-400',
          'text-red-700'
        );
        let errors = '';
        for (const error of response.data.data.data) {
          errors += '<li>' + error + '</li>';
        }
        updateStatusElement.innerHTML = errors;
      } else {
        updateStatusElement.classList.add(
          'bg-green-100',
          'border-green-400',
          'text-green-700'
        );
        updateStatusElement.innerHTML = response.data.data.message;
      }
    })
    .catch(error => {
      console.log(error);
    });
}

document.addEventListener('DOMContentLoaded', function() {
  // mapInit();
  MicroModal.init();

  const saveButton = document.querySelector('#save');
  if (saveButton) {
    saveButton.addEventListener('click', saveExchangePoint);
  }
  const deleteForms = document.querySelectorAll('.delete-ep');

  if (deleteForms.length > 0) {
    for (let i = 0; i < deleteForms.length; i++) {
      deleteForms[i].addEventListener('submit', deleteExchangePoint);
    }
  }

  const massUpdateType = document.getElementsByName('type');
  if (massUpdateType) {
    for (const radio of massUpdateType) {
      radio.addEventListener('change', changeType);
    }
  }

  const massUpdateSaveButton = document.querySelector('#massUpdateButton');
  if (massUpdateSaveButton) {
    massUpdateSaveButton.addEventListener('click', saveMassUpdateForm);
  }
});

export { addMarker, initMarkers, mapInit };
