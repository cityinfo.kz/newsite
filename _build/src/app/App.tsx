import * as React from 'react';
import { useState, useEffect, useCallback } from 'react';
import axios from 'axios';
import { io } from "socket.io-client";
import TableWithMap from '@app/components/TableWithMap';
import { useDispatch, useSelector } from 'react-redux';
import {
  setRates,
  updateRate as updateRateAction,
  setGross,
} from './store/rates/actions';
import { setSocketError } from './store/errors/actions';
import { AppState } from './store';
import FullTable from './components/Tables/Full';

interface Props {
  cityId: number;
  mapCenter: number[];
}

const socket = io(process.env.SOCKET_URL as string, {
  withCredentials: true,
  path: '/websocket',
  transports: ['websocket'],
});

const App = (props: Props) => {
  const { mapCenter, cityId } = props;

  const [loading, setLoading] = useState(true);
  const [showFull, setShowFull] = useState(true);

  const { gross, rates } = useSelector((state: AppState) => state.rates);
  const errors = useSelector((state: AppState) => state.errors);

  const dispatch = useDispatch();

  const updateRate = useCallback(
    (newRate: ExchangeRateUpdate) => {
      let itemIndex = null;

      rates.forEach((rate, index) => {
        if (rate.id === newRate.id) {
          itemIndex = index;
        }
      });

      // updateRate also updates Best
      dispatch(updateRateAction(newRate, itemIndex));
    },
    [rates, dispatch]
  );

  const getRates = useCallback(() => {
    setLoading(true);
    axios
      .get(process.env.API_URL + '/courses/' + cityId)
      .then(response => {
        dispatch(setRates(response.data));
      })
      .catch(error => {
        console.log(error);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [cityId, dispatch]);

  const toggleShowFull = () => {
    setShowFull(!showFull);
  };

  useEffect(() => {
    getRates();
  }, [getRates]);

  useEffect(() => {
    // on reconnection, reset the transports option, as the Websocket
    // connection may have failed (caused by proxy, firewall, browser, ...)
    const onReconnectAttempt = () => {
      socket.io.opts.transports = ['polling', 'websocket'];
    };

    const onReconect = () => {
      dispatch(setSocketError(false));
      getRates();
    };

    const onConnectError = () => {
      if (!errors.socketError) {
        dispatch(setSocketError(true));
      }
    };

    const onConnect = () => {
      socket.emit('join', `${cityId}`);
    };

    const onUpdate = (data: ExchangeRateUpdate) => {
      updateRate(data);
    };

    socket.on('reconnect_attempt', onReconnectAttempt);

    socket.on('reconnect', onReconect);

    socket.on('connect_error', onConnectError);

    socket.on('connect', onConnect);

    socket.on('update', onUpdate);

    return () => {
      socket.off('reconnect_attempt', onReconnectAttempt);
      socket.off('reconnect', onReconect);
      socket.off('connect_error', onConnectError);
      socket.off('connect', onConnect);
      socket.off('update', onUpdate);
    };
  }, [cityId, socket, dispatch, errors, updateRate, getRates]);

  const tabActiveClass =
    'inline-block py-2 px-4 text-white bg-pelorous-500 text-white font-semibold cursor-pointer';
  const tabInActiveClass =
    'inline-block py-2 px-4 text-blue-500 hover:underline font-semibold cursor-pointer';

  return (
    <>
      <div className="flex w-full justify-between mb-2">
        <ul className="flex list-none">
          <li className="mr-1">
            <a
              className={!gross ? tabActiveClass : tabInActiveClass}
              onClick={() => dispatch(setGross(false))}
            >
              Розница
            </a>
          </li>
          <li>
            <a
              className={gross ? tabActiveClass : tabInActiveClass}
              onClick={() => dispatch(setGross(true))}
            >
              Опт
            </a>
          </li>
        </ul>
        <ul className="flex list-none">
          <li>
            <a
              className="inline-block py-2 px-4 text-white cursor-pointer bg-green-600"
              onClick={toggleShowFull}
            >
              Переключить режим отображения
            </a>
          </li>
        </ul>
      </div>

      {showFull ? (
        <FullTable />
      ) : (
        <TableWithMap mapCenter={mapCenter} loading={loading} />
      )}
    </>
  );
};

export default App;
