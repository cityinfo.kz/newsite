import * as React from 'react';
import { render } from 'react-dom';
import App from './App';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import AppState from './store';

const store = createStore(AppState);

interface InitialData {
  cityId: number;
  mapCenter: number[];
}

let initialData: InitialData = {
  cityId: 4,
  mapCenter: [49.95, 82.61],
};

if ((window as any).initialData !== undefined) {
  initialData = (window as any).initialData;
}

const root = document.getElementById('root');
if (root) {
  render(
    <Provider store={store}>
      <App {...initialData} />
    </Provider>,
    document.getElementById('root')
  );
}
