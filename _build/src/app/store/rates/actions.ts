import {
  ActionTypes,
  SET_BEST_RATES,
  SET_GROSS,
  SET_RATES,
  SET_SELECTED_POINT,
  SET_SORT_BY,
  UPDATE_RATE,
} from './types';

const setRates = (rates: RatesObject): ActionTypes => {
  return {
    type: SET_RATES,
    payload: rates,
  };
};

const updateRate = (rate: ExchangeRate, index: number): ActionTypes => {
  return {
    type: UPDATE_RATE,
    payload: {
      rate,
      index,
    },
  };
};

const setBestRates = (bestRatesObject: BestRatesObject): ActionTypes => {
  return {
    type: SET_BEST_RATES,
    payload: bestRatesObject,
  };
};

const setGross = (value: boolean): ActionTypes => {
  return {
    type: SET_GROSS,
    payload: value,
  };
};

const setSortBy = (sortBy: string): ActionTypes => {
  return {
    type: SET_SORT_BY,
    payload: sortBy,
  };
};

const setSelectedPoint = (selected: number): ActionTypes => {
  return {
    type: SET_SELECTED_POINT,
    payload: selected,
  };
};

export {
  setRates,
  updateRate,
  setBestRates,
  setGross,
  setSortBy,
  setSelectedPoint,
};
