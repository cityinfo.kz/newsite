import {
  ActionTypes,
  SET_BEST_RATES,
  SET_GROSS,
  SET_RATES,
  SET_SELECTED_POINT,
  SET_SORT_BY,
  UPDATE_RATE,
} from './types';

export interface State {
  rates: ExchangeRate[];
  best: BestRatesObject;
  nbRates: NbRates[];
  gross: boolean;
  sortBy: string;
  selected: number;
}

const initialState: State = {
  rates: [],
  best: {
    retail: {},
    gross: {},
  },
  nbRates: [],
  gross: false,
  sortBy: 'date_update',
  selected: 0,
};

const ratesReducer = (state = initialState, action: ActionTypes): State => {
  switch (action.type) {
    case SET_RATES:
      return {
        ...state,
        ...action.payload,
      };
    case UPDATE_RATE:
      const { rate: newRate, index = null } = action.payload;

      const gross = newRate.gross;
      let best = state.best.retail;
      if (gross) {
        best = state.best.gross;
      }
      const { rates } = state;

      if (!newRate.published && index) {
        rates.splice(index, 1);
      } else if (newRate.published) {
        if (index === null) {
          rates.push(newRate);
        } else {
          delete newRate.city_id;
          rates[index] = newRate;
        }

        for (const property of Object.keys(best)) {
          if (property.substr(0, 3) === 'buy') {
            if (newRate[property] > best[property]) {
              best[property] = newRate[property];
            }
          } else {
            if (newRate[property] < best[property]) {
              best[property] = newRate[property];
            }
          }
        }
      }


      const newState = {
        ...state,
        rates,
      };

      if (gross) {
        newState.best.gross = best;
      } else {
        newState.best.retail = best;
      }

      return newState;
    case SET_BEST_RATES:
      return {
        ...state,
        best: action.payload,
      };

    case SET_GROSS:
      return {
        ...state,
        gross: action.payload,
      };
    case SET_SORT_BY:
      return {
        ...state,
        sortBy: action.payload,
      };
    case SET_SELECTED_POINT:
      return {
        ...state,
        selected: action.payload,
      };
    default:
      return state;
  }
};

export { ratesReducer };
