declare interface ExchangeRate {
  id: number;
  name: string;
  buyUSD: number;
  sellUSD: number;
  buyEUR: number;
  sellEUR: number;
  buyRUB: number;
  sellRUB: number;
  buyCNY: number;
  sellCNY: number;
  buyGBP: number;
  info: string;
  phones: string | string[];
  date_update: number;
  day_and_night: number;
  published: number;
  company_id: number;
  longitude: number;
  latitude: number;
  gross: boolean;
  atms: string | null;

  [key: string]: any;
}

declare interface ExchangeRateUpdate extends ExchangeRate {
  city_id: number;
}

declare interface BestRates {
  [key: string]: number;
}

declare interface BestRatesObject {
  retail: BestRates;
  gross: BestRates;
}

declare interface RatesObject {
  rates: ExchangeRate[];
  best?: BestRatesObject;
  nbRates?: NbRates[];
}

declare interface NbRates {
  code: string;
  value: number;
}

declare module '*.module.css' {
  interface IClassNames {
    [className: string]: string;
  }
  const classNames: IClassNames;
  export = classNames;
}
