import * as React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { connect } from 'react-redux';
import {
  YMaps,
  Map,
  ZoomControl,
  Placemark,
  YMapsApi,
} from 'react-yandex-maps';
import PopupContent from './PopupContent';
import Spinner from '../Spinner';
import { setSelectedPoint } from '@app/store/rates/actions';
import { State as RatesState } from '@app/store/rates/reducers';
import { AppState } from '@app/store';

interface Props {
  rates: RatesState;
  selectedPointId: number;
  mapCenter: number[];
  setSelectedPoint: Function;
}
interface State {
  loading: boolean;
}

class YMap extends React.Component<Props, State> {
  activePlacemarkPreset = 'islands#nightCircleDotIcon';
  defaultPlacemarkPreset = 'islands#circleDotIcon';

  state: State = {
    loading: true,
  };

  yMaps: YMapsApi = {};
  mapRef: any;

  handleBalloonOpen = (e: any) => {
    e.get('target').options.set('preset', this.activePlacemarkPreset);
  };

  handleBalloonClose = (e: any) => {
    e.get('target').options.set('preset', this.defaultPlacemarkPreset);
    this.props.setSelectedPoint(0);
  };

  getBounds = (): number[][] | [] => {
    const bounds: number[][] = [];
    const { rates, gross } = this.props.rates;

    rates.map(rate => {
      if (rate.latitude && rate.longitude && gross === !!rate.gross) {
        bounds.push([rate.latitude, rate.longitude]);
      }
    });

    return bounds;
  };

  handleMapLoad = (ymaps: YMapsApi) => {
    this.setState({ loading: false });
    this.yMaps = ymaps;
    this.setCenter();
  };

  setCenter = (ref: any = this.mapRef) => {
    const bounds = this.getBounds();
    const ymaps = this.yMaps;
    if (ymaps) {
      if (bounds.length > 1) {
        ref.setBounds(ymaps.util.bounds.fromPoints(bounds));
      } else if (bounds.length === 1) {
        ref.setCenter(...bounds);
      }
    }
  };

  setMapInstanceRef = (ref: any) => {
    if (ref && !this.mapRef) {
      this.mapRef = ref;
    }
  };

  shouldComponentUpdate(nextProps: Props, nextState: State) {
    switch (true) {
      case nextProps.rates.rates !== this.props.rates.rates:
        return true;
      case nextProps.selectedPointId &&
        nextProps.selectedPointId !== this.props.selectedPointId:
        return true;
      case nextProps.rates.gross !== this.props.rates.gross:
        return true;
      default:
        return nextState !== this.state;
    }
  }

  componentDidUpdate(prevProps: Props) {
    if (
      prevProps.rates.rates !== this.props.rates.rates ||
      prevProps.rates.gross !== this.props.rates.gross
    ) {
      this.setCenter();
    }
  }

  render() {
    const { rates, gross } = this.props.rates;
    const placeMarks = rates.map(rate => {
      if (rate.latitude && rate.longitude && gross === !!rate.gross) {
        const balloonContent = <PopupContent rate={rate} />;

        return (
          <Placemark
            key={rate.id}
            geometry={[rate.latitude, rate.longitude]}
            properties={{
              balloonContent: renderToStaticMarkup(balloonContent),
            }}
            options={{
              preset: this.defaultPlacemarkPreset,
              hideIconOnBalloonOpen: false,
              balloonOffset: [0, -10],
            }}
            onBalloonClose={this.handleBalloonClose}
            onBalloonOpen={this.handleBalloonOpen}
            instanceRef={(ref: any) => {
              ref &&
                rate.id === this.props.selectedPointId &&
                ref.balloon.open();
            }}
          />
        );
      }
    });

    return (
      <React.Fragment>
        <Spinner show={this.state.loading} />
        <YMaps
          query={{
            load:
              'Map,Placemark,control.ZoomControl,util.bounds,geoObject.addon.balloon',
          }}
        >
          <Map
            state={{
              center: this.props.mapCenter,
              zoom: 14,
              behaviors: ['default', 'scrollZoom'],
            }}
            className="YMap"
            onLoad={this.handleMapLoad}
            instanceRef={this.setMapInstanceRef}
          >
            <ZoomControl
              options={{
                size: 'small',
              }}
            />
            {placeMarks}
          </Map>
        </YMaps>
      </React.Fragment>
    );
  }
}

const actionCreators = {
  setSelectedPoint,
};

const mapStateToProps = (state: AppState) => ({
  rates: state.rates,
});

export default connect(mapStateToProps, actionCreators)(YMap);
