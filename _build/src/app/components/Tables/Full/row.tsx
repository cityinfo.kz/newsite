import * as React from 'react';
import { Fragment } from 'react';
import * as dayjs from 'dayjs';
import PhoneNumber from 'awesome-phonenumber';
import * as styles from './row.module.css';
// @ts-ignore
import { currencies } from '@app/constants.ts';
// @ts-ignore
import { getBuy, getSell } from '@app/utils.ts';

interface Props {
  rate: ExchangeRate;
  best: BestRates;
  selectPoint: Function;
}

const Row = (props: Props) => {
  const { rate, best } = props;
  const dayjsDate = dayjs.unix(rate.date_update);
  const time = dayjsDate.format('HH:mm:ss ');
  const date = dayjsDate.format('DD.MM.YYYY');
  const phones = (rate.phones as string[]).map((phone, index) => {
    const pn = new PhoneNumber(phone, 'RU');
    let row = null;
    if (pn.isValid()) {
      row = (
        <a
          href={pn.getNumber('rfc3966')}
          className="block text-pelorous-700 underline mb-1"
        >
          {pn.getNumber('international')}
        </a>
      );
    } else {
      row = phone;
    }
    return <React.Fragment key={index}>{row}</React.Fragment>;
  });
  const currencyHelperClass = 'block md:hidden text-xs text-gray-700 mb-1';

  const currenciesTds = currencies.map((currency: string) => {
    const currencyBuy = getBuy(rate, currency, best);
    const currencySell = getSell(rate, currency, best);

    const buyClass = currencyBuy.best ? styles.BestBuy : undefined;
    const sellClass = currencySell.best ? styles.BestSell : undefined;
    return (
      <Fragment key={currency}>
        <td className="text-center">
          <span className={currencyHelperClass}>Покуп. {currency}</span>
          <span className={buyClass}>{currencyBuy.value}</span>
        </td>
        <td className="text-center">
          <span className={currencyHelperClass}>Прод. {currency}</span>
          <span className={sellClass}>{currencySell.value}</span>
        </td>
      </Fragment>
    );
  });

  const title = props.rate.name;
  let link = null;
  if (props.rate.company_id) {
    link = (
      <a
        href={'/company-card.html?id=' + props.rate.company_id}
        target="_blank"
        rel="noopener noreferrer"
      >
        {props.rate.name}
      </a>
    );
  }

  return (
    <tr key={rate.id} onClick={props.selectPoint(props.rate.id)}>
      <td>
        <p className={styles.Title}>{link ? link : title}</p>
        {rate.atms && (
          <div className={styles.Atms}>
            <a className={styles.Button} href={`/atms.html?bank=${rate.atms}`}>
              Банкоматы
            </a>
          </div>
        )}
        <p className={styles.Info}>{props.rate.info}</p>
        <p className={styles.Date}>
          <span>{time}</span>&nbsp;
          <span>{date}</span>
        </p>
      </td>
      {currenciesTds}
      <td className={['text-center'].join(' ')}>{phones}</td>
    </tr>
  );
};

export default Row;
