import * as React from 'react';
import { Fragment } from 'react';
import { connect } from 'react-redux';
// @ts-ignore
import Tooltip from 'react-simple-tooltip';
import { AppState } from '@app/store';
import { currencies, currencyFlags } from '@app/constants.ts';
import { State as RatesState } from '@app/store/rates/reducers';
import { setSortBy, setRates } from '@app/store/rates/actions';
import Row from './row';
import * as styles from './index.module.css';
import { sortRates, getTableTitle } from '@app/utils';

interface Props {
  rates: RatesState;
  setSortBy: Function;
  setRates: Function;
}

const FullTable = (props: Props) => {
  const { rates, best, gross, nbRates } = props.rates;

  let bestCourses = best.retail;
  if (gross) {
    bestCourses = best.gross;
  }
  const setSortBy = (value: string) => {
    props.setSortBy(value);
    props.setRates({ rates: sortRates(rates, value) });
  };

  const title = getTableTitle(gross);
  const currencyThs = currencies.map((currency: string) => (
    <th colSpan={2} key={currency} className="text-center">
      <div className="flex justify-around items-center">
        <img src={currencyFlags[currency]} alt={currency} />
        <span>{currency}</span>
      </div>
      <div
        className="border-t pt-1 mt-1 border-gray-600 cursor-help"
        style={{ cursor: 'help' }}
      >
        <Tooltip
          content="Официальный курс Национального банка"
          placement="bottom"
          fontSize="12px"
          padding={5}
        >
          {nbRates.length && nbRates.find(rate => rate.code === currency).value}
        </Tooltip>
      </div>
    </th>
  ));

  const currencySortTh = currencies.map((currency: string) => (
    <Fragment key={currency}>
      <th
        className="cursor-pointer"
        onClick={setSortBy.bind(null, 'buy' + currency)}
      >
        покуп
      </th>
      <th
        className="cursor-pointer"
        onClick={setSortBy.bind(null, 'sell' + currency)}
      >
        прод
      </th>
    </Fragment>
  ));

  const rows = rates.map(
    rate =>
      gross === !!rate.gross && (
        <Row
          rate={rate}
          best={bestCourses}
          selectPoint={() => {}}
          key={rate.id}
        />
      )
  );
  return (
    <div className="overflow-x-auto">
      <table cellPadding="0" cellSpacing="0" className="exchangeTable">
        <thead>
          <tr>
            <th rowSpan={2} className="text-center title">
              Обменный пункт
            </th>
            {currencyThs}
            <th rowSpan={2} className="text-center phones">
              Телефоны
            </th>
          </tr>
          <tr>{currencySortTh}</tr>
        </thead>
        <tbody>
          <tr>
            <td colSpan={12}>
              <h5 className={styles.RatesTitle}>{title}</h5>
            </td>
          </tr>
          {rows}
        </tbody>
      </table>
    </div>
  );
};

const mapStateToProps = (state: AppState) => ({
  rates: state.rates,
});

const actionCreators = {
  setSortBy,
  setRates,
};

export default connect(mapStateToProps, actionCreators)(FullTable);
