<?php

return [
    '_base' => [
        'file' => '_base',
        'description' => 'New site Base Template',
    ],
    '_main' => [
        'file' => '_main',
        'description' => 'New site main page template',
    ],
    '_rates' => [
        'file' => '_rates',
        'description' => 'City template',
    ],
    '_page' => [
        'file' => '_page',
        'description' => 'Page template',
    ],
    '_transport' => [
        'file' => '_transport',
        'description' => 'Template for transport pages',
    ],
    '_search' => [
        'file' => '_search',
        'description' => 'Search template',
    ],
    '_company_card' => [
        'file' => '_company_card',
        'description' => 'Company card template',
    ],
    '_accounts' => [
        'file' => '_accounts',
        'description' => 'Accounts page template',
    ],
    '_register' => [
        'file' => '_register',
        'description' => 'Register template',
    ],
    '_register_confirmation' => [
        'file' => '_register_confirmation',
        'description' => 'Register confirmation template',
    ],
    '_exchange_base' => [
        'file' => '_exchange_base',
        'description' => 'New site Base Template for Exchange Rates',
    ],
    '_exchange_my_points' => [
        'file' => '_exchange_my_points',
        'description' => 'My exchange points template',
    ],
    '_exchange_add_point' => [
        'file' => '_exchange_add_point',
        'description' => 'Add exchange point template',
    ],
    '_atms' => [
        'file' => '_atms',
        'description' => 'Template for atms on map',
    ],
    '_afisha' => [
        'file' => '_afisha',
        'description' => 'Template for afisha page',
    ],
    '_fuel_prices' => [
        'file' => '_fuel_prices',
        'description' => 'Fuel prices template',
    ],
    '_exchange_manager' => [
        'file' => '_exchange_manager',
        'description' => 'Template for manager edit courses'
    ]
];