<?php

return [
    '_rates_app' => [
        'file' => '_rates_app',
        'description' => 'Rates react app',
    ],
    '_search' => [
        'file' => '_search',
        'description' => 'Search snippet',
    ],
    '_search_categories' => [
        'file' => '_search_categories',
        'description' => 'Search categories snippet',
    ],
    '_cron_autovokzal_kz' => [
        'file' => '_cron_autovokzal_kz',
        'description' => 'Autovokzal.kz parser',
    ],
    '_cron_kurs_kz' => [
        'file' => '_cron_kurs_kz',
        'description' => 'Kurs.kz parsers',
    ],
    '_company_card' => [
        'file' => '_company_card',
        'description' => 'Company card snippet',
    ],
    '_exchange_mass_update' => [
        'file' => '_exchange_mass_update',
        'description' => 'Exchange mass update',
    ],
    '_exchange_add_point_form' => [
        'file' => '_exchange_add_point_form',
        'description' => 'Exchange add point form',
    ],
    '_exchange_get_points' => [
        'file' => '_exchange_get_points',
        'description' => 'Get exchange points',
    ],
    '_get_youtube_video_id' => [
        'file' => '_get_youtube_video_id',
        'description' => 'Split youtube share link https://youtu.be/cChkPnpN15c, get video id, ex.: cChkPnpN15c',
    ],
    '_get_fuel_prices' => [
        'file' => '_get_fuel_prices',
        'description' => 'Get fuel prices',
    ],
];