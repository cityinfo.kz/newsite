<?php

return [
    '_register_email' => [
        'file' => '_register_email',
        'description' => 'Email activation message',
    ],
    '_register_form' => [
        'file' => '_register_form',
        'description' => 'Register form',
    ],
    '_exchange_mass_update_form' => [
        'file' => '_exchange_mass_update_form',
        'description' => 'Exchange points mass update form',
    ],
    '_exchange_mass_update_form_rows' => [
        'file' => '_exchange_mass_update_form_rows',
        'description' => 'Exchange points mass update form rows',
    ],
    '_exchange_add_point_form' => [
        'file' => '_exchange_add_point_form',
        'description' => 'Exchange add point form',
    ],
    '_exchange_point' => [
        'file' => '_exchange_point',
        'description' => 'Exchange point',
    ],
    '_exchange_points' => [
        'file' => '_exchange_points',
        'description' => 'Exchange points wrapper',
    ],
    '_login_form' => [
        'file' => '_login_form',
        'description' => 'Login form',
    ],
    '_user_menu' => [
        'file' => '_user_menu',
        'description' => 'User left menu',
    ],
    '_fuel_prices_row' => [
        'file' => '_fuel_prices_row',
        'description' => 'Fuel prices table row',
    ],
    '_fuel_prices_outer' => [
        'file' => '_fuel_prices_outer',
        'description' => 'Fuel prices table',
    ],
];