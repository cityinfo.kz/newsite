const path = require('path');
const base = path.resolve(__dirname);
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const Dotenv = require('dotenv-webpack');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const AssetsManifest = require('webpack-assets-manifest');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = (env, argv) => {
  const mode = argv.mode === 'production' ? 'production' : 'development';
  return {
    mode,
    entry: {
      main: ['./_build/src/js/main.js', './_build/src/css/main.css'],
      'company-card': './_build/src/js/company-card.js',
      app: './_build/src/app/index.tsx',
      search: './_build/src/js/search.js',
      'auto-complete': './_build/src/js/auto-complete.js',
      exchange: './_build/src/js/exchange.js',
      atms: './_build/src/js/atms.js',
      manager: './_build/src/js/manager.js',
    },
    output: {
      path: base + '/assets/components/newsite/',
      publicPath: '/assets/components/newsite/',
      chunkFilename: 'js/[name].[contenthash:8].min.js',
      filename: 'js/[name].[contenthash:8].min.js',
    },
    optimization: {
      minimize: mode === 'production',
      minimizer: [new TerserPlugin({}), new OptimizeCSSAssetsPlugin({})],
      splitChunks: {
        chunks: mode === 'production' ? 'all' : 'async',
        maxInitialRequests: Infinity,
        cacheGroups: {
          vendor: {
            test: /[\\/]node_modules[\\/]/,
            reuseExistingChunk: true,
            name(module) {
              const packageName = module.context.match(
                /[\\/]node_modules[\\/](.*?)([\\/]|$)/
              )[1];

              return packageName.replace('@', '');
            },
          },
        },
      },
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: [
                '@babel/preset-typescript',
                [
                  '@babel/preset-env',
                  {
                    modules: false,
                  },
                ],
              ],
            },
          },
        },
        {
          test: /\.(ts|tsx)$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: [
                '@babel/preset-typescript',
                '@babel/preset-react',
                '@babel/preset-env',
              ],
              plugins: [
                'react-css-modules',
                ['@babel/plugin-proposal-class-properties', { loose: true }],
                'react-hot-loader/babel',
              ],
            },
          },
        },
        {
          test: /\.module\.css$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'style-loader',
            },
            {
              loader: 'css-loader',
              options: {
                importLoaders: 1,
                modules: {
                  localIdentName: '[name]__[local]___[hash:base64:5]',
                },
              },
            },
            {
              loader: 'postcss-loader',
              options: {
                config: {
                  path: path.resolve(__dirname, 'postcss.config.js'),
                  ctx: { mode: mode },
                },
              },
            },
          ],
        },
        {
          test: /^(?!.*?\.module).*\.css$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                publicPath: path.resolve(__dirname, 'css/'),
              },
            },
            {
              loader: 'css-loader',
              options: {
                importLoaders: 1,
              },
            },
            {
              loader: 'postcss-loader',
              options: {
                config: {
                  path: path.resolve(__dirname, 'postcss.config.js'),
                  ctx: { mode: mode },
                },
              },
            },
          ],
        },
        {
          test: /\.(gif|svg|jpg|png)$/,
          loader: 'file-loader',
          options: {
            outputPath: 'images',
            publicPath: '../images',
            name: '[name].[ext]',
          },
        },
      ],
    },
    resolve: {
      extensions: ['.js', '.tsx', '.ts'],
      alias: {
        '@app': path.resolve(__dirname, '_build/src/app'),
      },
    },
    plugins: [
      new CleanWebpackPlugin({
        verbose: false,
        cleanStaleWebpackAssets: true,
        dry: mode !== 'production',
        cleanOnceBeforeBuildPatterns: ['**/*', '!action.php'],
      }),
      new MiniCssExtractPlugin({
        path: base + '/assets/components/newsite/css/',
        publicPath: '/assets/components/newsite/css/',
        chunkFilename: 'css/[name].[contenthash:8].min.css',
        filename: 'css/[name].[contenthash:8].min.css',
      }),
      new CopyWebpackPlugin([
        {
          from: base + '/_build/src/images',
          to: base + '/assets/components/newsite/images',
        },
      ]),
      new Dotenv({
        defaults: true,
        silent: true,
      }),
      new ImageminPlugin({
        test: /\.(jpe?g|png|gif|svg)$/i,
        disable: mode !== 'production',
        pngquant: {
          quality: '85-90',
        },
      }),
      new AssetsManifest({
        output: base + '/assets/components/newsite/manifest.json',
        publicPath: true,
      }),
    ],
  };
};
