<?php

/**
 * The base class for exchangeRates.
 */
class exchangeRates extends newSite
{
    /**
     * ID городов, где нужно скрывать не круглосуточные обменные пункты
     * @var array
     */
    public $checkToHide = [
        4,
        5,
    ];

    protected $pdoFetch;

    /**
     * city_id берётся из таблицы exchCityNames
     */
    protected $cities = [
        4 => 3218, // Усть-Каменогорск city_id = 4
        5 => 3341, // Усть-Каменогорск ОПТ city_id = 5
        6 => 3680, // Риддер
        3 => 3078, // Нур-Султан
        1 => 3423, // Павлодар
        2 => 3079, // Алматы
        7 => 4427, // Алматы Опт
        8 => 4418, // Нур-Султан Опт
    ];

    /**
     * @param modX $modx
     * @param array $config
     */
    public function __construct(modX &$modx, array $config = [])
    {
        $this->modx =& $modx;

        $this->config = array_merge([
            /* Разрешенные к изменению через api поля */
            'allowedFields' => [
                'day_and_night',
                'company_id',
                'sorting',
                'published',
                'hidden',
                'info',
                'phones',
                'longitude',
                'latitude',
                'gross',
            ],

            'currencyFields' => [
                'buyUSD',
                'sellUSD',
                'buyEUR',
                'sellEUR',
                'buyRUB',
                'sellRUB',
                'buyCNY',
                'sellCNY',
                'buyGBP',
                'sellGBP',
                'buyXAU',
                'sellXAU',
            ],

            'requiredFields' => [
                'id',
                'name',
                'info',
                'date_update',
                'phones',
            ],
        ], $config);

        $this->pdoFetch = $this->modx->getService('pdoFetch');

        parent::__construct($modx, $this->config);
    }


    /**
     * Returns city_id equal to `exchCityNames`.`id`
     * @param int $resourceId
     * @return false|int|string
     */
    public function getCityIdByResourceId($resourceId)
    {
        return array_search($resourceId, $this->cities);
    }


    public function massUpdate($postData)
    {
        $errors = $this->massUpdateValidate($postData);
        if (!empty($errors)) {
            return $this->error('Ошибка при сохранении формы', $errors);
        }
        $query = $this->modx->newQuery('exchangeRate');
        $query->where([
            'id:IN' => $postData['points'],
            'user_id' => $this->modx->user->id,
        ]);
        $points = $this->modx->getIterator('exchangeRate', $query);
        if ($points) {
            foreach ($points as $point) {
                $emptyCourses = 0;
                foreach ($this->config['currencyFields'] as $currency) {
                    if ($postData[$currency] < 1) {
                        $emptyCourses++;
                    }
                    $point->set($currency, $postData[$currency]);
                }
                if ($emptyCourses === count($this->config['currencyFields'])) {
                    $point->set('hidden', 1);
                } else {
                    $point->set('hidden', 0);
                }
                $point->save();
            }
        } else {
            $errors = ['Не найдено подходящих обменных пунктов!'];
            return $this->error('Ошибка при сохранении формы', $errors);
        }
        return $this->success('Данные успешно сохранены');
    }

    public function massUpdateValidate(&$post)
    {
        $this->currenciesFormat($post);
        $errors = [];
        if (!in_array($post['type'], ['gross', 'retail'])) {
            $errors[] = 'Непрвильный тип обменных пунктов';
        }
        $validPoints = [];
        if (!empty($post['points'])) {
            foreach ($post['points'] as $point) {
                $point = intval($point);
                if ($point > 0) {
                    $validPoints[] = $point;
                }
            }
        }

        if (!isset($post['points']) || (isset($post['points']) && empty($post['points'])) || empty($validPoints)) {
            $errors[] = 'Выберите хотя бы один обменный пункт!';
        }

        $post['points'] = $validPoints;

        return $errors;
    }

    public function currenciesFormat(&$currencies)
    {
        foreach ($this->config['currencyFields'] as $currency) {

            $value = floatval(str_replace(',', '.', $currencies[$currency]));

            if ($value !== 0) {
                $value = floatval(number_format(abs($value), 2, '.', ''));
            }

            $currencies[$currency] = $value;
        }
    }

    /**
     * Возвращение ошибки
     * @param string $message
     * @param null|array $data
     * @return array
     */
    public function error($message, $data = null)
    {
        return $this->response(false, ['message' => $message, 'data' => $data]);
    }

    /**
     * @param boolean $success
     * @param array $data
     * @return array
     */
    private function response($success, $data)
    {
        return ['success' => $success, 'data' => $data];
    }

    /**
     * Возвращение успешного ответа
     * @param string $message
     * @param null|array $data
     * @return array
     */
    public function success($message, $data = null)
    {
        return $this->response(true, ['message' => $message, 'data' => $data]);
    }

    /**
     * Сохранение курсов валют обменного пункта пользователем
     * @param array $postData
     * @return array
     */
    public function saveExchangePoint($postData)
    {

        $errors = $this->validate($postData);

        if (!empty($errors)) {
            return $this->error('Ошибка при сохранении формы', $errors);
        }

        $new = false;

        if (empty($postData['id'])) {
            /* @var exchangeRate $exchangeRate */
            $exchangeRate = $this->modx->newObject('exchangeRate');
            $exchangeRate->set('published', 0);
            $new = true;
        } else {
            if (!$exchangeRate = $this->modx->getObject('exchangeRate', ['id' => (int)$postData['id']])) {
                return $this->error('Could not get object');
            }
        }

        $emptyCurrencies = 0;
        foreach ($this->config['currencyFields'] as $currency) {

            if ($postData[$currency] < 1) {
                $emptyCurrencies++;
            }

            $exchangeRate->set($currency, $postData[$currency]);
        }
        if ($emptyCurrencies === count($this->config['currencyFields'])) {
            $exchangeRate->set('hidden', 1);
        } else {
            $exchangeRate->set('hidden', 0);
        }

        $exchangeRate->set('name', preg_replace('/[^\w, "\'\-()*.]+/u', '', $postData['name']));
        $exchangeRate->set('phones', $postData['phones']);
        $exchangeRate->set('info', $postData['info']);
        $exchangeRate->set('date_update', time());
        if($postData['latitude'] && $postData['longitude']){
            $exchangeRate->set('latitude', $postData['latitude']);
            $exchangeRate->set('longitude', $postData['longitude']);
        }
        $exchangeRate->set('city_id', intval($postData['city_id']));
        $exchangeRate->set('user_id', $this->modx->user->id);
        $exchangeRate->set('gross', intval($postData['gross']));


        if (!$exchangeRate->save()) {
            return $this->error('Ошибка при сохранении данных');
        }

        /**
         * Записываем в лог изменений
         */
        $rateLog = $this->modx->newObject('exchangeRateLog');
        $data = $exchangeRate->toArray();
        $data['exchanger_id'] = $data['id'];
        unset($data['id']);
        $rateLog->fromArray($data);
        $rateLog->save();

        /**
         * Данные для сокета
         */
        $toSocket = $exchangeRate->toArray();

        unset($toSocket['exchanger_id']);
        $this->sendToSocketApi(json_encode($toSocket));


        if ($new) {

            $query = $this->modx->newQuery('exchCityName');
            $query->select(['exchCityName.*']);
            $query->where(['id' => $exchangeRate->get('city_id')]);
            $query->prepare();
            $query->stmt->execute();
            $city = $query->stmt->fetchAll(PDO::FETCH_ASSOC)[0];
            $message = '[Новый обменный пункт] - г.' . $city['name'] . ', название: ' . $exchangeRate->get('name');
            $this->sendMessageToManagers($message);

            $this->sendMessageToEmail($message, '[Новый обменный пункт]', 'emailsender');
        }
        return $this->success('Данные успешно сохранены');
    }

    public function validate(&$post)
    {
        $this->currenciesFormat($post);

        $post['latitude'] = str_replace(',', '.', floatval($post['latitude']));
        $post['longitude'] = str_replace(',', '.', floatval($post['longitude']));

        $errors = [];

        if (empty($post['city_id'])) {
            $errors['city_id'] = 'Поле город обязательно для заполнения';
        }
        if (empty($post['name'])) {
            $errors['name'] = 'Поле Название обязательно для заполнения';
        }
        if (empty($post['info'])) {
            $errors['info'] = 'Поле Адрес обязательно для заполнения';
        }
        if (empty($post['phones'])) {
            $errors['phones'] = 'Поле Телефон обязательно для заполнения';
        }

        return $errors;
    }

    /**
     * Send exchange rate to API
     * @param string $data
     */
    public function sendToSocketApi(string $data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.cityinfo.kz/courses/update/");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $headers = [
            'courses-token: db4d7131-be6a-4922-8e22-42cdafcc954k',
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data),
        ];

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_exec($ch);

        curl_close($ch);
    }

    /**
     * Удаление Обменного пункта
     * @param array $post
     * @return array
     */
    public function deleteExchangePoint($post)
    {
        $id = $post['id'];
        $userID = $this->modx->user->id;
        /* @var exchangeRate $point */
        if ($point = $this->modx->getObject('exchangeRate', $id)) {

            if ($point->get('user_id') === $userID || $this->modx->getAuthenticatedUser('mgr')) {
                $point->set('deleted', 1);
                $point->set('deleted_at', time());

                $data = $point->toArray();

                if (!$point->save()) {
                    return $this->error('Ошибка при удалении обменного пункта!');
                }
                /**
                 * Записываем в лог изменений
                 */
                $rateLog = $this->modx->newObject('exchangeRateLog');
                $rateLog->fromArray($data);
                $rateLog->set('user_id', $this->modx->user->id);
                $rateLog->save();

                // Отправляем данные в socket.io
                $data['published'] = 0;
                $this->sendToSocketApi(json_encode($data));

                return $this->success('Удаление выполнено успешно',
                    ['redirect' => $this->modx->makeUrl(3093, '', '', 'https')]);
            } else {
                return $this->error('Access denied!');
            }
        }

        return $this->error('Access denied!');
    }

    /**
     * Get ExchangeRates from exchange_rates table
     * @param int $cityId
     * @param string $type
     * @param string $sortby
     * @param string $sortdir
     * @return array
     */
    public function getExchangeRates($cityId, $type = 'all', $sortby = 'name', $sortdir = 'ASC')
    {
        $pdo = $this->pdoFetch;
        $where = [
            'city_id' => $cityId,
            'date_update:>=' => strtotime(date('d.m.Y')),
            'hidden' => 0,
            'published' => 1,
            'deleted' => 0,
        ];

        switch ($type) {
            case 'retail':
                $where['gross'] = 0;
                break;
            case 'gross':
                $where['gross'] = 1;
                break;
            default:
                break;
        }

        if (in_array($cityId, $this->checkToHide)) {
            $where['date_update:>='] = strtotime(date('d.m.Y', strtotime('-1 day')));
        }
        $config = [
            'class' => 'exchangeRate',
            'select' => [
                'exchangeRate' => implode(',', $this->config['requiredFields']) . ',' .
                    implode(',', $this->config['currencyFields']) . ',day_and_night,company_id',
            ],
            'where' => $where,
            'limit' => 1000,
            'return' => 'data',
        ];

        $config['sortby'] = $sortby;
        $config['sortdir'] = $sortdir;

        $pdo->setConfig($config);
        $rates = $pdo->run();
        if ($rates) {
            $hours = strtotime(date("H:i"));
            foreach ($rates as &$rate) {
                $toHide = (($hours >= strtotime("20:00") || $hours <= strtotime("08:00")) && !$rate['day_and_night']);
                if (in_array($cityId, $this->checkToHide) && $toHide) {
                    $rate['hidden'] = 1;
                }
            }
        }
        return $rates;
    }

    /**
     * Best sell/buy rates calculation
     * @param array $rates
     * @return array
     */
    public function setBestRates($rates = [])
    {
        $bestSell = [
            'sellUSD' => 10000,
            'sellEUR' => 10000,
            'sellRUB' => 10000,
            'sellCNY' => 10000,
            'sellGBP' => 10000,
            'sellXAU' => 1000000,
        ];

        $bestBuy = [
            'buyUSD' => 1,
            'buyEUR' => 1,
            'buyRUB' => 1,
            'buyCNY' => 1,
            'buyGBP' => 1,
            'buyXAU' => 1,
        ];

        $today = date('Y-m-d');

        if ($rates) {
            foreach ($rates as &$rate) {
                $rateDate = date('Y-m-d', $rate['date_update']);
                if (!$rate['hidden'] && $today === $rateDate) {
                    foreach ($bestBuy as $key => &$value) {
                        $rate[$key] = floatval($rate[$key]);
                        if ($rate[$key] != 0 && $rate[$key] > $value) {
                            $value = $rate[$key];
                        }
                    }
                    foreach ($bestSell as $key => &$value) {
                        $rate[$key] = floatval($rate[$key]);
                        if ($rate[$key] != 0 && $rate[$key] < $value) {
                            $value = $rate[$key];
                        }
                    }
                }
            }
        }

        return array_merge($bestSell, $bestBuy);
    }

    /**
     * Get National Bank rates
     * @return array
     */
    public function getNBRates()
    {
        $query = $this->modx->newQuery('nbRate');
        $query->where(['code' => 'EUR'], xPDOQuery::SQL_OR);
        $query->where(['code' => 'USD'], xPDOQuery::SQL_OR);
        $query->where(['code' => 'RUB'], xPDOQuery::SQL_OR);
        $query->where(['code' => 'CNY'], xPDOQuery::SQL_OR);
        $query->where(['code' => 'GBP'], xPDOQuery::SQL_OR);
        $query->prepare();
        $query->stmt->execute();
        return $query->stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Получаем курсы валют с сайта нацбанка
     *
     * @param null|string $date дата
     * @return array|bool
     */
    public function getCurrencyRates($date = null)
    {
        if ($date != null) {
            $date = strtotime($date);
            $date = date('d.m.Y', $date);
        } else {
            $date = date('d.m.Y');
        }
        $link = $this->modx->getOption('nationalbank_url', '',
                'https://www.nationalbank.kz/rss/get_rates.cfm?fdate=') . $date;
        $rates = [];

        $agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_USERAGENT, $agent); //make it act decent
        curl_setopt($ch, CURLOPT_URL, $link);         //set the $url to where your request goes
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //set this flag for results to the variable
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); //This is required for HTTPS certs if
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); //you don't have some key/password action

        /* execute the request */
        $result = curl_exec($ch);
        curl_close($ch);
        libxml_use_internal_errors(true);

        if ($newXml = simplexml_load_string($result)) {
            foreach ($newXml->item as $item) {
                $rates[] = [
                    'name' => strval($item->fullname),
                    'code' => strval($item->title),
                    'value' => str_replace(',', '.', strval($item->description)),
                    'quant' => strval($item->quant),
                    'index' => strval($item->index) ?: 'N',
                    'change' => strval($item->change) ?: '0.00',
                ];
            }

            return $rates;
        }

        return false;
    }

    /**
     * Записываем в базу курсы валют нацбанка
     *
     * @param array $rates массив курсов
     * @return bool
     */
    public function insertCurrencyRates($rates)
    {
        if ($rates) {
            foreach ($rates as $rate) {
                if (!$db_rate = $this->modx->getObject('nbRate', ['code' => $rate['code']])) {
                    $db_rate = $this->modx->newObject('nbRate', ['code' => $rate['code']]);
                }
                $db_rate->fromArray($rate);
                if (!$db_rate->save()) {
                    $this->modx->log(1, '[CurrencyRates:Error] Could not save into db with code - ' . $rate['code']);
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Метод возвращает список городов с курсами
     *
     * @return mixed
     */
    public function api_getListCities()
    {
        $config = [
            'class' => 'exchCityName',
            'select' => [
                'exchCityName' => 'id,name',
            ],
            'where' => [
                'deleted' => 0,
            ],
            'limit' => 1000,
            'return' => 'json',
        ];

        $this->pdoFetch->setConfig($config);

        return $this->pdoFetch->run();
    }

    /**
     * Возвращает список обменных пунктов по ID родителя/города
     *
     * @param $cityId
     * @return mixed
     */
    public function api_getListExchange($cityId)
    {
        $where = [
            'city_id' => $cityId,
            'deleted' => 0,
        ];
        $config = [
            'class' => 'exchangeRate',
            'select' => [
                'exchangeRate' => 'id,name,user_id,gross,published,hidden,deleted',
            ],
            'where' => $where,
            'limit' => 1000,
            'return' => 'json',
        ];
        $config['sortby'] = 'name';
        $config['sortdir'] = 'ASC';

        $this->pdoFetch->setConfig($config);

        return $this->pdoFetch->run();
    }

    /**
     * Возвращает данные по обменному курсу
     *
     * @param $id
     * @return mixed
     */
    public function api_getExchangeCourses($id)
    {
        $where = [
            'id' => $id,
        ];
        $config = [
            'class' => 'exchangeRate',
            'select' => [
                'exchangeRate' => '*',
            ],
            'where' => $where,
            'limit' => 1,
            'return' => 'json',
        ];

        $this->pdoFetch->setConfig($config);

        return $this->pdoFetch->run();
    }

    /**
     * Сохраняем данные обменного пункта при редактировании Оператором
     *
     * @param $data array массив данных с формы
     * @return string
     */
    public function api_setExchangeCourses($data)
    {
        /** @var exchangeRate $rate */
        if ($rate = $this->modx->getObject('exchangeRate', ['id' => (int)$data['id']])) {
            $allowedFields = array_unique(array_merge($this->config['allowedFields'],
                $this->config['requiredFields']));

            $fields = [];

            foreach ($allowedFields as $field) {
                if (in_array($field, $allowedFields) && array_key_exists($field, $data)) {
                    $value = $data[$field];
                    if (in_array($field, ['day_and_night', 'published', 'hidden', 'gross'])) {
                        switch ($value) {
                            case 1:
                                $value = 1;
                                break;
                            case 0:
                            default:
                                $value = 0;
                                break;
                        }
                    }

                    $fields[$field] = $value;
                }
            }

            $emptyCourses = 0;
            foreach ($this->config['currencyFields'] as $currency) {
                $value = floatval(str_replace(',', '.', $data[$currency]));

                if ($value !== 0) {
                    $value = floatval(number_format(abs($value), 2, '.', ''));
                }
                if ($value < 1) {
                    $emptyCourses++;
                }
                $fields[$currency] = $value;
            }
            if ($emptyCourses === count($this->config['currencyFields'])) {
                $fields['hidden'] = 1;
            } else {
                $fields['hidden'] = 0;
            }

            $fields['id'] = (int)$data['id'];
            $fields['date_update'] = time();
            $fields['name'] = preg_replace('/[^\w, "\'\-()*.]+/u', '', $data['name']);
            $fields['atms'] = $data['atms'] ? $data['atms'] : null;
            $fields['longitude'] = $data['longitude'] ? $data['longitude'] : null;
            $fields['latitude'] = $data['latitude'] ? $data['latitude'] : null;
            
            $rate->fromArray($fields);

            if (!$rate->save()) {
                $this->modx->log(modX::LOG_LEVEL_ERROR,
                    'Error during course save via manager edit: ' . $rate->toJSON());
                $response = [
                    'success' => false,
                    'message' => 'Ошибка при сохранении данных, обратитесь к администратору!',
                ];

            } else {
                /**
                 * Записываем в лог изменений
                 */
                $rateLog = $this->modx->newObject('exchangeRateLog');
                $data = $rate->toArray();
                $data['exchanger_id'] = $data['id'];
                $data['user_id'] = $this->modx->user->id;
                unset($data['id']);
                $rateLog->fromArray($data);
                $rateLog->save();

                $this->sendToSocketApi(json_encode($rate->toArray()));

                $response = [
                    'success' => true,
                    'message' => 'Данные успешно сохранены!',
                ];
            }
        } else {
            $response = [
                'success' => false,
                'message' => 'Не получен id обменного пункта!',
            ];
        }

        return $this->modx->toJSON($response);
    }
}
