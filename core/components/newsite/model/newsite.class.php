<?php

class newSite
{
    /** @var modX $modx */
    public $modx;
    public $proxy_path = 'http://62.77.155.165:7000/?url=';

    /**
     * @param modX $modx
     * @param array $config
     */
    public function __construct(modX &$modx, array $config = [])
    {
        $this->modx =& $modx;
        $corePath = MODX_CORE_PATH . 'components/newsite/';
        $assetsUrl = MODX_ASSETS_URL . 'components/newsite/';
        $assetsPath = MODX_ASSETS_PATH . 'components/newsite/';

        $this->config = array_merge([
            'corePath' => $corePath,
            'assetsPath' => $assetsPath,
            'modelPath' => $corePath . 'model/',
            'processorsPath' => $corePath . 'processors/',

            'connectorUrl' => $assetsUrl . 'connector.php',
            'assetsUrl' => $assetsUrl,

            'cssUrl' => $assetsUrl . 'css/',
            'jsUrl' => $assetsUrl . 'js/',
        ], $config);
    }


    /**
     * Initialize App
     */
    public function initialize()
    {
        $this->modx->addPackage('newsite', $this->config['modelPath']);
    }

    /**
     * @param modSystemEvent $event
     * @param array $scriptProperties
     */
    public function handleEvent(modSystemEvent $event, array $scriptProperties)
    {
        extract($scriptProperties);
        switch ($event->name) {
            case 'OnBeforeRegisterClientScripts':
                $manifest = $this->config['assetsPath'] . 'manifest.json';
                if (file_exists($manifest)) {
                    $files = json_decode(file_get_contents($manifest), true);
                    foreach ($files as $key => $file) {
                        if (strpos($file, '.css')) {
                            $this->modx->regClientCSS($file);
                        } elseif (strpos($file, '.js')) {
                            // exclude manager from register
                            if (!strpos($file, 'manager') || $this->modx->user->isAuthenticated('mgr')) {
                                $this->modx->regClientScript($file);
                            }
                        }
                    }
                }
                break;
        }
    }

    /**
     * Loads dependency
     */
    public function loadParser()
    {
        if (!class_exists('simple_html_dom')) {
            require_once dirname(__FILE__) . "/simple_html_dom.php";
        }
    }

    public function sendExceptionToEmail(\Exception $e, $subject, $toEmail = 'admin')
    {
        $message = $e->getMessage();
        return $this->sendMessageToEmail($message, $subject, $toEmail);
    }

    public function sendMessageToEmail($message, $subject, $toEmail = 'emailsender')
    {
        switch ($toEmail) {
            case 'emailsender':
                $toEmail = $this->modx->getOption('emailsender');
                break;
            case 'admin':
            default:
                $toEmail = $this->modx->getOption('admin_email');
                break;
        }
        return $this->sendEmail($message, $subject, $toEmail);
    }

    private function sendEmail($message, $subject, $toEmail)
    {
        $this->modx->getService('mail', 'mail.modPHPMailer');
        $this->modx->mail->set(modMail::MAIL_BODY, $message);
        $this->modx->mail->set(modMail::MAIL_FROM, $this->modx->getOption('emailsender'));
        $this->modx->mail->set(modMail::MAIL_FROM_NAME, $this->modx->getOption('site_name'));
        $this->modx->mail->set(modMail::MAIL_SUBJECT, $subject);
        $this->modx->mail->address('to', $toEmail);
        if (!$this->modx->mail->send()) {
            $this->modx->log(modX::LOG_LEVEL_ERROR,
                'An error occurred while trying to send the email: '
                . $this->modx->mail->mailer->ErrorInfo);
            return false;
        }
        $this->modx->mail->reset();
        return true;
    }

    /**
     * Send message to Manager's group in Telegram
     * @param string $text
     * @param bool $markdown
     */
    public function sendMessageToManagers($text, $markdown = false)
    {
        $token = "863581330:AAH-bAHq6UmKrMVAEsK4zTW0BpDUUFlQtJE";
        $this->sendMessageToTelegramGroup($text, $token, -1001431127532, $markdown);
    }

    /**
     * Send message to telegram group
     * @param string $text
     * @param string $token
     * @param int $chat_id
     * @param bool $markdown
     */
    private function sendMessageToTelegramGroup($text, $token, $chat_id, $markdown = false)
    {
        $data = [
            'chat_id' => $chat_id,
            'text' => $text,
            'parse_mode' => 'HTML',
        ];
        if ($markdown) {
            $data['parse_mode'] = "Markdown";
        }
        $options = stream_context_create([
            'http' =>
                [
                    'timeout' => 5 //10 seconds
                ],
        ]);
        @file_get_contents("https://api.telegram.org/bot{$token}/sendMessage?" . http_build_query($data), false,
            $options);
    }

}