<?php
/** @var modX $modx */
/** @var array $scriptProperties */
/** @var newSite $newSite */
switch ($modx->event->name) {
    case 'OnMODXInit':
        if ($newSite = $modx->getService('newSite', 'newSite', MODX_CORE_PATH . 'components/newsite/model/')) {
            $modx->getService('exchangeRates', 'exchangeRates', MODX_CORE_PATH . 'components/newsite/model/');
            $newSite->initialize();
        }
        break;
    default:
        if ($newSite = $modx->getService('newSite')) {
            $newSite->handleEvent($modx->event, $scriptProperties);
        }
}