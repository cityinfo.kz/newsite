<?php
/** @var modX $modx */

$pdoTools = $modx->getService('pdoTools');
$pdoFetch = $modx->getService('pdoFetch');
$tplRows = $modx->getOption('tpl_rows', $scriptProperties, '_exchange_mass_update_form_rows');
$tplForm = $modx->getOption('tpl_form', $scriptProperties, '_exchange_mass_update_form');

$user = $modx->getAuthenticatedUser('web');

$buyCurrencies = [
    'buyUSD' => [
        'label' => 'Покупка USD',
        'tabindex' => 5,
    ],
    'buyEUR' => [
        'label' => 'Покупка EUR',
        'tabindex' => 7,
    ],
    'buyRUB' => [
        'label' => 'Покупка RUB',
        'tabindex' => 9,
    ],
    'buyCNY' => [
        'label' => 'Покупка CNY',
        'tabindex' => '11',
    ],
    'buyGBP' => [
        'label' => 'Покупка GBP',
        'tabindex' => 13,
    ],
    'buyXAU' => [
        'label' => 'Покупка Золота',
        'tabindex' => 15,
    ],
];

$sellCurrencies = [
    'sellUSD' => [
        'label' => 'Продажа USD',
        'tabindex' => 6,
    ],
    'sellEUR' => [
        'label' => 'Продажа EUR',
        'tabindex' => 8,
    ],
    'sellRUB' => [
        'label' => 'Продажа RUB',
        'tabindex' => 10,
    ],
    'sellCNY' => [
        'label' => 'Продажа CNY',
        'tabindex' => 12,
    ],
    'sellGBP' => [
        'label' => 'Продажа GBP',
        'tabindex' => 14,
    ],
    'sellXAU' => [
        'label' => 'Продажа Золота',
        'tabindex' => 16,
    ],
];

$buyRows = $pdoTools->parseChunk($tplRows, [
    'currencies' => $buyCurrencies,
]);
$sellRows = $pdoTools->parseChunk($tplRows, [
    'currencies' => $sellCurrencies,
]);


$where = [
    'user_id' => $user->id,
    'deleted' => 0,
];
$config = [
    'class' => 'exchangeRate',
    'select' => [
        'exchangeRate' => 'id,name,gross,city_id',
    ],
    'where' => $where,
    'return' => 'data',
];
$config['sortby'] = 'id';
$config['sortdir'] = 'DESC';

$pdoFetch->setConfig($config);

$points = $pdoFetch->run();

$grossPoints = [];
$retailPoints = [];

foreach ($points as $point) {
    if ($point['gross']) {
        $grossPoints[] = $point;
    } else {
        $retailPoints[] = $point;
    }
}

$showForm = false;
$retail = false;
$gross = false;

if (count($grossPoints) > 1) {
    $gross = true;
}
if (count($retailPoints) > 1) {
    $retail = true;
}

if ($retail || $gross) {
    $showForm = true;
}

return $pdoTools->parseChunk($tplForm, [
    'buyRows' => $buyRows,
    'sellRows' => $sellRows,
    'showForm' => $showForm,
    'retail' => $retail,
    'gross' => $gross,
    'retailPoints' => $retailPoints,
    'grossPoints' => $grossPoints,
]);