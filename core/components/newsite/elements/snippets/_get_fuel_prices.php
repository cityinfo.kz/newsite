<?php

/**
 *
 */
/** @var modX $modx */
/** @var array $scriptProperties */
$pdo = $modx->getService('pdoFetch');
$pdoTools = $modx->getService('pdoTools');

$showEmpty = $modx->getOption('showEmpty', $scriptProperties, false);
$sortby = $modx->getOption('sortby', $scriptProperties, 'id');
$sortdir = $modx->getOption('sortdir', $scriptProperties, 'ASC');
$userID = $modx->getOption('userID', $scriptProperties, null);
$activeOnly = $modx->getOption('activeOnly', $scriptProperties, 0);
$totalVar = $modx->getOption('totalVar', $scriptProperties, 'total');

// Если передан ключ сортировки, то проверяем его и устанавливаем
if (isset($_GET['gasoline'])) {
    if (in_array($_GET['gasoline'],
        ['ai80', 'ai92', 'ai92ru', 'ai93', 'ai95', 'ai96', 'ai98', 'diesel', 'diesel_winter', 'gas'])) {
        $sortby = $_GET['gasoline'];
    }
}
$where = [];
if ($activeOnly) {
    $where['gasolineStation.active'] = 1;
}
/*
 * выбираем только те заправки, где дата обновления сегодня
 * и которые прошли модерацию
 */
/*    $where = [
        'fp.date_update:>='      => strtotime(date("d.m.Y")),
    ];*/
if (!$showEmpty) {
    $wh = [
        'fp.ai80:!='          => null,
        'fp.ai92:!='          => null,
        'fp.ai92ru:!='        => null,
        'fp.ai93:!='          => null,
        'fp.ai95:!='          => null,
        'fp.ai96:!='          => null,
        'fp.ai98:!='          => null,
        'fp.diesel:!='        => null,
        'fp.diesel_winter:!=' => null,
        'fp.gas:!='           => null,
    ];
    $where = array_merge($where, $wh);
}
if (!empty($userID)) {
    $where['gasolineStation.user_id'] = $userID;
}
$config = [
    'class'    => 'gasolineStation',
    'select'   => [
        'gasolineStation' => 'id,name,city_id,user_id,active,description,address',
        'fp'              => 'ai80,ai92,ai92ru,ai93,ai95,ai95prime,ai96,ai98,diesel,diesel_winter,gas,date_update,station_id',
        'city'            => 'city.name as city',
    ],
    'leftJoin' => [
        'fp'   => [
            'class' => 'fuelPrice',
            'on'    => 'gasolineStation.id = fp.station_id',
        ],
        'city' => [
            'class' => 'City',
            'on'    => 'city.id = gasolineStation.city_id',
        ],
    ],
    'where'    => $where,
    'return'   => 'data',
    'limit'    => 100,
    'sortby'   => $sortby,
    'sortdir'  => $sortdir,
];

$pdo->setConfig($config);
$stations = $pdo->run();

$rows = null;
$output = null;
$total = 0;
if (!empty($stations)) {
    $total = count($stations);
    foreach ($stations as $station) {
        $rows .= $pdoTools->parseChunk('_fuel_prices_row', [
            'id'            => $station['id'],
            'name'          => $station['name'],
            'address'       => $station['address'],
            'ai80'          => $station['ai80'],
            'ai92'          => $station['ai92'],
            'ai92ru'        => $station['ai92ru'],
            'ai93'          => $station['ai93'],
            'ai95'          => $station['ai95'],
            'ai95prime'     => $station['ai95prime'],
            'ai96'          => $station['ai96'],
            'ai98'          => $station['ai98'],
            'diesel'        => $station['diesel'],
            'diesel_winter' => $station['diesel_winter'],
            'gas'           => $station['gas'],
            'description'   => $station['description'],
            'date_update'   => $station['date_update'],
            'active'        => $station['active'],
            'city'          => $station['city'],
        ]);
    }
    $output = $pdoTools->parseChunk('_fuel_prices_outer', [
        'rows' => $rows,
    ]);
}
$modx->toPlaceholder($totalVar, $total);
if (empty($tplOuter)) {
    return $rows;
}
return $output;

