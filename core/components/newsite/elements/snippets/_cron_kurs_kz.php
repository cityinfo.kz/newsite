<?php
/** @var modX $modx */
/** @var newSite $newSite */
$newSite = $modx->getService('newSite');

$newSite->loadParser();

if (!$exCourses = $modx->getService('exchangeCourses')) {
    return 'Could not load exchangeCourses class!';
}

$newSite->loadParser();

$cities = [
    'almaty',
    'astana',
];
$names = [
    'almaty' => 'Алматы',
    'astana' => 'Астана',
];

try {
    foreach ($cities as $city) {
        $url = $newSite->proxy_path . 'https://kurs.kz/index.php?mode='
            . $city;
        if ($html = file_get_html($url, false, null, null)) {
            $script = $html->find('script', 4);
            $string = $script->innertext;

            $string = str_replace('var punkts = ', '', $string);
            $string = str_replace(
                "var globalTown = '" . $names[$city] . "';",
                '',
                $string
            );
            $string = trim($string);
            $string = rtrim($string, ';');
            $courses = json_decode($string);
            

            if ($courses || empty($courses)) {
                $arr = [];
                foreach ($courses as $course) {
                    $phones = '';
                    if ($course->phones) {
                        $phones = implode(', ', $course->phones);
                    }
                    $arr[$course->id] = [
                        'exchanger_id' => 7777 . $course->id,
                        'name' => $course->name,
                        'info' => $course->address,
                        'date_update' => $course->date,
                        //'actualTime' => $course->actualTime,
                        'hidden' => !(int)$course->workmodes->worknow,
                        'day_and_night' => (int)$course->workmodes->worknow, // нужно, что бы показывать круглосуточные пункты
                        //'workmodes' => $course->workmodes,
                        //'closed' => (int)$course->workmodes->closed,
                        'buyUSD' => $course->data->USD[0],
                        'sellUSD' => $course->data->USD[1],
                        'buyEUR' => $course->data->EUR[0],
                        'sellEUR' => $course->data->EUR[1],
                        'buyRUB' => $course->data->RUB[0],
                        'sellRUB' => $course->data->RUB[1],
                        'buyCNY' => $course->data->CNY[0],
                        'sellCNY' => $course->data->CNY[1],
                        'buyGBP' => $course->data->GBP[0],
                        'sellGBP' => $course->data->GBP[1],
                        'buyXAU' => $course->data->GOLD[0],
                        'sellXAU' => $course->data->GOLD[1],
                        'published' => (int)$course->workmodes->worknow,
                        'phones' => $phones,
                        'gross' => $course->wholesale ? 1 : 0,
                    ];

                    // Выставляем ID городов
                    if ($city === 'almaty') {
                        $arr[$course->id]['city_id'] = 2;
                    } elseif ($city === 'astana') {
                        $arr[$course->id]['city_id'] = 3;
                    }

                    // Проверяем нужно ли скрывать обменный пункт
                    $emptyFieldCount = 0;
                    foreach ($arr[$course->id] as $key => $value) {
                        if (in_array($key, $exCourses->config['currencyFields'])) {
                            if (floatval($value) == 0) {
                                $emptyFieldCount++;
                            }
                        }
                    }
                    // Скрываем, если все курсы пустые
                    if ($emptyFieldCount === 10) {
                        $arr[$course->id]['hidden'] = 1;
                    }
                }

                foreach ($arr as $course) {
                    $query = [
                        'exchanger_id' => $course['exchanger_id'],
                        'city_id' => $course['city_id'],
                    ];
                    if ($c = $modx->getObject('exchangeRate', $query)) {
                    } else {
                        $c = $modx->newObject('exchangeRate', $query);

                        $newSite->sendMessageToManagers('[Новый обменный пункт] - <b>' . $names[$city] . '</b> ' . $course['name']);
                    }
                    $c->fromArray($course);
                    $c->save();
                    // send to API
                    $toSocket = $c->toArray();
                    unset($toSocket['exchanger_id']);

                    $exCourses->sendToSocketApi(json_encode($toSocket));
                }

            } else {
                throw new \Exception(
                    'Invalid JSON while parsing courses from kurs.kz, snippet: kurskz.cron.php'
                );
            }
        } else {
            throw new \Exception(
                'Could not get courses from url = ' . $url
            );
        }
    }
    echo 'true';
} catch (\Exception $e) {
    $newSite->sendExceptionToEmail($e, '[cityinfo.kz] Error during parsing');
    echo 'false';
}
