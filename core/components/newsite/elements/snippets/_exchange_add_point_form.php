<?php
/** @var modX $modx */
/** @var array $scriptProperties */

$pdoTools = $modx->getService('pdoTools');

$tplForm = $modx->getOption('tpl_form', $scriptProperties, '_exchange_add_point_form');
$user = $modx->getAuthenticatedUser('web');
// pID - exchange point ID
// Получаем ID ОП
$pID = (int)$_REQUEST['pid'];


$query = $modx->newQuery('exchCityName');
$query->select(['exchCityName.id', 'exchCityName.name']);
$query->where([
    'deleted' => 0,
]);
$query->prepare();
$query->stmt->execute();
$cities = $query->stmt->fetchAll(PDO::FETCH_ASSOC);


$modx->regClientStartupScript('<script type="text/javascript">
window.redirectUrl = "' . $modx->makeUrl(3093, '', '', 'https') . '";
</script>',
    true);

// update
if (!empty($pID)) {
    /** @var $point exchangeRate */
    if (!$point = $modx->getObject('exchangeRate', ['id' => $pID, 'user_id' => $user->id, 'deleted' => 0])) {
        return $modx->lexicon('exchangeRate_wrong_user');
    }

    $point = $point->toArray();
    $point['cities'] = $cities;
    $point['name'] = htmlentities($point['name'], ENT_QUOTES);
    $point['info'] = htmlentities($point['info'], ENT_QUOTES);
    $point['phones'] = htmlentities($point['phones'], ENT_QUOTES);
    $rows = $pdoTools->parseChunk($tplForm, $point);

    // Возвращаем форму
    return $rows;
}

return $pdoTools->getChunk($tplForm, [
    'cities' => $cities,
]);