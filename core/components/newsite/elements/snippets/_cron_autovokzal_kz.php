<?php
/** @var modX $modx */
/** @var newSite $newSite */
$newSite = $modx->getService('newSite');

$newSite->loadParser();

if (isset($_GET['parseAutovokzal'])) {
    if ($_GET['parseAutovokzal'] == 'cityinfoAuthParser1992xoDDD') {
        $url = 'https://autovokzal.kz/schedule/';
        if ($html = file_get_html(
            $newSite->proxy_path . $url,
            false,
            null,
            null
        )) {
            $content = $html->find('#body-id #content-id table');

            $table = '';
            foreach ($content as $cont) {
                $cont->class = 'table is-bordered';
                $table = $cont->outertext;
            }
            if ($table) {
                $resource = $modx->getObject('modResource', 571);
                $content = '<div class="overflow-x-auto">' . $table . '</div>';
                $resource->set('content', $content);
                $resource->save();
            }
        }
    }
}