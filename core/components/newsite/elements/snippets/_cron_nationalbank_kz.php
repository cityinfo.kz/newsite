<?php
if (isset($_GET['cronnbupdatecourse'])) {
    /* @var $exchangeRates exchangeRates */
    if (!$exchangeRates = $modx->getService('exchangeRates')) {
        return 'Could not load exchangeCourses class!';
    }

    $courses = $exchangeRates->getCurrencyRates();
    if (!$exchangeRates->insertCurrencyRates($courses)) {
        $modx->log(1, 'Не удалось вставить курсы нац.банка в базу данных');
    }
}
