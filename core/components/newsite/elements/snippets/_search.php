<?php
// $_POST['idCat'];
//константа для вывода количество строк в подсказках, не более указанной цифры
define("COUNT_STR", 10);
// Лимит для результатов поиска по категориям и запросу
$limit = 50;
$parsetext = array();
$search_arr = array();
//              НАЧАЛО       авто подсказки====================================
if (isset($_GET['term']) and strlen($_GET['term']) > 2) {//автоподсказки


    $text = trim($_GET['term']);
    $text = addslashes($text);
//            НАЧАЛО Склееный запрос  =============================
    $martin = array();
    $text = str_replace('"', '', $text);

    //разбиваем строку поиска на отдельные слова, и записываем в массив
    $parsetext = preg_split("/[\s,.;:()\"]+/", $text);

    // ищем список всех компаний кроме ЗАРЕГИСТРИРОВАННЫХ по введённому слову,
    $sql = "SELECT TTagValue.value_tgvl as result, count(*) AS KOL
    FROM tcomponents
    INNER JOIN tlink_com ON tcomponents.id_com = tlink_com.id_com
    INNER JOIN TTagValue ON TTagValue.id_tgvl = tlink_com.id_val
    INNER JOIN TInfoObject ON TInfoObject.id_iobj = TTagValue.id_iobj
    WHERE (tcomponents.value_com LIKE '$parsetext[0]%'";
    //если количество введённых слов больше одного то добавляем их в запрос
    if (count($parsetext) > 1) {
        for ($i = 1; $i < count($parsetext); $i++) {
            $sql .= " OR value_com LIKE '%$parsetext[$i]%'";
        }
    }
    //указываем тип клиентов которых ищем
    $sql .= ")  AND (TInfoObject.id_clntp=1 OR TInfoObject.id_clntp=4)";
    //группируем данные по найденной компании в заросе, считаем количество совпадений для каждой
    //компании и упорядочиваем  по максимальному количеству
    $sql .= " GROUP BY TTagValue.id_iobj ORDER BY KOL DESC";
    $sql .= " LIMIT 0," . COUNT_STR;
    $q = $modx->prepare($sql);
    $q->execute();
    $result = $q->fetchAll(PDO::FETCH_ASSOC);

    //$result = $modx->db->query($sql);
    //считаем количество полученных строк, и вычисляем остаток количества строк в подсказки который нужно заполнить
    $stroki = COUNT_STR - count($result);
    //распечатываем результаты запроса в переменную подсказки
    foreach ($result as $myrow) {

        $martin[] = $myrow['result'];
    };


// Если слов больше чем 1, то делаем запрос на точное соответствие
    if (count($parsetext) > 1) {
        $sql = <<<SQL
SELECT TTagValue.value_tgvl as result, count(*) AS KOL
    FROM tcomponents
    INNER JOIN tlink_com ON tcomponents.id_com = tlink_com.id_com
    INNER JOIN TTagValue ON TTagValue.id_tgvl = tlink_com.id_val
    INNER JOIN TInfoObject ON TInfoObject.id_iobj = TTagValue.id_iobj
    WHERE (TTagValue.value_tgvl LIKE "%$text%") AND (TInfoObject.id_clntp=1 OR TInfoObject.id_clntp=4)
    GROUP BY TTagValue.id_iobj ORDER BY KOL DESC
    LIMIT 1
SQL;

        $q = $modx->prepare($sql);
        $q->execute();
        $result = $q->fetchAll(PDO::FETCH_ASSOC);
        // если находим результат, то вставляем его в начало массива
        if (count($result) > 0) {
            array_unshift($martin, $result[0]['result']);
        }
    }

    //print_r($result);//проверочный код
//            КОНЕЦ Склееный запрос  =========================

    if ($stroki > 0) {
        //            НАЧАЛО ЗАПРОС или или или============================

        $sql = "SELECT value_com as result
FROM tcomponents
INNER JOIN tlink_com
ON tcomponents.id_com = tlink_com.id_com
INNER JOIN TTagValue
ON TTagValue.id_tgvl = tlink_com.id_val
INNER JOIN TInfoObject
ON TInfoObject.id_iobj = TTagValue.id_iobj
WHERE (tcomponents.value_com LIKE '%$parsetext[0]%'";
        if (count($parsetext) > 1) {
            for ($i = 1; $i < count($parsetext); $i++) {

                $sql .= " OR value_com LIKE '%$parsetext[$i]%'";
            }
        }
        $sql .= ") AND (TInfoObject.id_clntp = 1 OR TInfoObject.id_clntp = 4)
GROUP BY tcomponents.id_com
   LIMIT 0, $stroki";

        $q = $modx->prepare($sql);
        $q->execute();
        $result = $q->fetchAll(PDO::FETCH_ASSOC);
        //считаем количество полученных строк, и вычисляем остаток количества строк в подсказки который нужно заполнить
        $stroki = $stroki - count($result);
        //дописываем результаты в подсказки
        foreach ($result as $myrow) {
            $martin[] = $myrow['result'];
        };
    };// КОНЕЦ $stroki > 0
    //КОНЕЦ ЗАПРОСА или или или
    //удаляем повторяющиеся элементы
    $alternate = array_unique($martin);
    //распечатываем результаты запросов как подсказки
    $alternate = json_encode(array_values($alternate));
    return $alternate;

}//                 КОНЕЦ    авто подсказки====================================
elseif (isset($_POST['idCat'])) {// поиск фирм по категории
    //занесение данных в статистику активности категории
    $idCat = intval($_POST['idCat']);
//    $sql = "INSERT INTO tCatStat (id_cat, ip) values ($idCat, " . $_SERVER['REMOTE_ADDR'] . ")";
//    $q = $modx->prepare($sql);
//    $q->execute();
    $sql = <<<SQL
SELECT  
tTv1.id_iobj as id, TInfoObject.name_iobj AS name,
tTv2.value_tgvl AS adress, img_company.img_src AS logo
FROM TCategory
INNER JOIN  `TPrintLink` ON `TPrintLink`.`id_cat` = `TCategory`.`id_cat`
INNER JOIN `TPrintCat` on `TPrintCat`.`id_pcat` = `TPrintLink`.`id_pb`
INNER JOIN TTagValue AS tTv1 ON(TCategory.id_cat=tTv1.id_tgcl)
INNER JOIN TInfoObject ON(tTv1.id_iobj = TInfoObject.id_iobj)
LEFT JOIN TTagValue AS tTv2 ON(TInfoObject.id_iobj=tTv2.id_iobj AND (tTv2.id_tag = 3 OR tTv2.id_tag = 4 OR tTv2.id_tag = 16))
LEFT JOIN img_company ON (tTv1.id_iobj = img_company.id_iobj AND img_company.type = 1)

WHERE (TPrintCat.id_pcat = $idCat AND (TInfoObject.id_clntp = 1 OR TInfoObject.id_clntp = 4))
GROUP BY TInfoObject.id_iobj LIMIT $limit
SQL;

    $q = $modx->prepare($sql);
    $q->execute();
    $rsltByCat = $q->fetchAll(PDO::FETCH_ASSOC);
    if (count($rsltByCat) > 0) {
        $catData = $rsltByCat;
        foreach ($catData as &$firm) {
            if ($firm['logo'] === null || (is_file($firm['logo']) === false)) {
                $firm['logo'] = 'assets/images/min_company_logo.png';
            }
        }
        shuffle($catData);
        return json_encode($catData);
    }

} else {//поиск фирм по тэгам

//                  НАЧАЛО поиск списка фирм ====================================
    //если из формы пришла переменная
    if (isset($_POST['s_jq'])) {
        //обрабатываем эту переменную
        $text = trim($_POST['s_jq']);
        // Иза за замены ТОО не удавалось найти фирму по Юр. названию
        //$text = str_replace('ТОО', '', $text);
        $text = addslashes($text);
        if (strlen($text) < 3) {
            return json_encode('Слишком короткий запрос!');
        }
//             Склееный запрос  =============================
        $martin = array();
        //$text = str_replace('"', '', $text);
        //разбиваем строку поиска на отдельные слова, и записываем в массив
        $keywords = preg_split("/[\s,.;:()\"]+/", $text);
        foreach ($keywords as $keyword)
            if (strlen($keyword) > 2) {
                $parsetext[] = $keyword;
            }
        /**
         * Проверяем есть ли соответствие между запросом и названием фирмы
         */
        $sqlFull = "SELECT TTagValue.value_tgvl as keyword, TInfoObject.name_iobj as name, TInfoObject.id_iobj as id 
                        , img_company.img_src as logo, COUNT(id) as KOL FROM TTagValue LEFT JOIN TInfoObject ON TInfoObject.id_iobj = TTagValue.id_iobj
                        LEFT JOIN img_company ON (img_company.id_iobj = TInfoObject.id_iobj and img_company.type = 1)
                        WHERE 
                        (LOWER(name_iobj) LIKE LOWER('%$text%') OR LOWER(TTagValue.value_tgvl) LIKE '%$text%') AND (TInfoObject.id_clntp=1 OR TInfoObject.id_clntp=4)
                        GROUP BY TTagValue.id_tgvl
                         ORDER BY KOL DESC LIMIT 1";
        $q = $modx->prepare($sqlFull);
        $q->execute();
        $result = $q->fetchAll(PDO::FETCH_ASSOC);
        /**
         * Если есть, то выводим эту фирму выше других (т.е. первой)
         */
        $output = [];
        $do_not_show = null;
        if (count($result) > 0) {
            $output[] = $result[0];
        }
        $sql = "SELECT TTagValue.value_tgvl as keyword 
, TInfoObject.name_iobj as name
, TInfoObject.id_iobj as id
, img_company.img_src as logo
, count(id) AS KOL  
FROM TTagValue 
INNER JOIN TInfoObject ON TInfoObject.id_iobj = TTagValue.id_iobj 
INNER JOIN TTag ON TTag.id_tag = TTagValue.id_tag 
left JOIN img_company ON (img_company.id_iobj = TInfoObject.id_iobj and img_company.type = 1)
WHERE 
(value_tgvl LIKE '%$text%'";
        if (count($parsetext) > 1) {
            foreach ($parsetext as $keyword) {
                $sql .= " OR value_tgvl LIKE '%$keyword%'";
            }
        }
        # echo $startpoint;
        #HAVING KOL=>".$startpoint."
        $sql .= ") AND (TInfoObject.id_clntp=1 OR TInfoObject.id_clntp=4) 
GROUP BY name HAVING KOL>=1
ORDER BY KOL DESC, TInfoObject.name_iobj ASC ";
# убрал >= для invest audit
# из-за этого не выводил Дом Торговый ДОМ HAVING KOL=".count($parsetext)."
        /*----------------------------------------------------------------------------*/

        $sql .= " LIMIT 0, " . $limit;
        $q = $modx->prepare($sql);
        $q->execute();
        $result = $q->fetchAll(PDO::FETCH_ASSOC);

        if (count($result) > 1) {
            foreach ($result as $company) {
                unset($company['KOL']);
                $output[] = $company;
            }
        }

        if (count($output) > 0) {
            foreach ($output as &$company) {
                if (!$company["logo"] || !is_file($company["logo"])) {
                    $company['logo'] = 'assets/images/min_company_logo.png';
                }
            }
        }

        echo json_encode($output);
        die();
//        if ($output) {
//            $output = '<table class="table">' . $output;
//        } else {
//            $output = '<table class="table">';
//        }
//
//
////==================================================================================
//        $idcompany = array();
//        $adress = array();
//        foreach ($result2 as $row1) {
//            $idcompany[] = $row1['id_iobj'];
//        }
//
//        $UIdcompany = array_unique($idcompany);
//        //достаю данные адреса компании
//
//        if (count($UIdcompany) > 0) {
//            $sqlquery = 'SELECT value_tgvl,id_iobj FROM TTagValue WHERE ( id_tag=4 OR id_tag=3 OR id_tag=16)) AND (';
//            $cikl = 0;
//            foreach ($UIdcompany as $key => $value) {
//                if ($cikl == 0) {
//                    $sqlquery .= ' id_iobj=' . $value . '';
//                } else {
//                    $sqlquery .= ' OR id_iobj=' . $value . '';
//                };
//                $cikl++;
//            }
//            $sqlquery .= ')';
//            $q = $modx->prepare($sqlquery);
//            $q->execute();
//            $rezaltik = $q->fetchAll(PDO::FETCH_ASSOC);
//            foreach ($rezaltik as $row2) {
//                if (!empty($adress[$row2['id_iobj']])) {
//                    $adress[$row2['id_iobj']] .= '; ' . $row2['value_tgvl'];
//                } else {
//                    $adress[$row2['id_iobj']] = $row2['value_tgvl'];
//                }
//            }
//        } //else {
//        // Закомментировал, при поиске геркулеса выдавалась строка ниже
//        // $output .= '<div class="item_null">Данные по запросу не найдены. Попробуйте ещё раз!</div>';
//        //}
////==============================================================================
//
//        $myrow = $result;
//
//        $obj = array();
//        for ($i = 0; $i < count($myrow); $i++) {
//            foreach ($myrow as $key => $value) {
//                $obj[$myrow[$i]["id_iobj"]] = $myrow[$i];
//            }
//        }
//
//        // Отключил перемешивание
//        /*  if(count($obj)>1){shuffle($obj);}
//            */
//        foreach ($obj as $key => $value) {
//            if ($do_not_show == $value["id_iobj"]) {
//                continue;
//            }
//            if ($value["img_src"] != null && is_file($value["img_src"])) {
//                $img_src = $value["img_src"];
//
//
//            } else {
//                $img_src = 'assets/images/min_company_logo.png';
//
//            }
//
//            $output .= '
//                    <tr>
//                        <td class="logo_rows">
//                            <a class="yaCompany" href="/company_card.html?id=' . $value["id_iobj"] . '" target="_blank" data-company="' . $value["name_iobj"] . '">
//                                <img src="/' . $img_src . '"/>
//                            </a>
//                        </td>
//                        <td class="info_rows">
//                            <p class="comp_name">
//                                <a class="yaCompany" href="/company_card.html?id=' . $value["id_iobj"] . '" target="_blank" data-company="' . $value["name_iobj"] . '">' . $value["name_iobj"] . '</a>
//                            </p>';
//            if (isset($adress[$value['id_iobj']]) and $value['id_iobj'] != '') {
//                $output .= '<p><span class="dop_class">Адрес:</span>' . $adress[$value['id_iobj']] . '</p>';
//            }
//            /*<p><span class="dop_class">Найдено:</span>' . $value["value_tgvl"] . '</a></p>*/
//            $output .= '</td>
//                        <td class="info_price is-hidden-touch">
//                            <a target="_blank" href="/company_card.html?id=' . $value["id_iobj"] . '"><button class="button is-success" >Подробнее</button></a>
//                            <a target="_blank" href="/company_card.html?id=' . $value["id_iobj"] . '#price"><button class="button is-info-blue" >Прайс-лист</button></a>
//                        </td>
//                    </tr>
//        ';
//        };
//
//
//        $output .= '</table>';
//
//        return $output;


    }//                  КОНЕЦ поиск списка фирм ====================================
    else {
        return json_encode($err["sr_null"] = "Enter your data!");

    }
}