<?php
if (isset($_GET['id']) and ($_GET['id'] !== '')) {

    $companyID = (int)$_GET['id'];
    if ($companyID === 0) {
        return false;
    }

    $sql = 'SELECT "Официальное наименование" AS `ukazat`
     , `name_iobj` AS `val`
     , "official_name" AS `type`
     , 1 AS "order_t", NULL AS category

FROM
  `TInfoObject`
WHERE
  `TInfoObject`.`id_iobj` =' . $companyID . '
UNION
SELECT `TTag`.`capt_tag` AS `ukazat`
     , `TTagValue`.`value_tgvl` AS `val`
     , `name_tag` AS `type`
     , TTag.order_tag AS "order_t", TCategory.name_cat AS category
FROM
  `TTagValue`
LEFT JOIN `TTag`
ON `TTagValue`.`id_tag` = `TTag`.`id_tag`
LEFT JOIN `TInfoObject`
ON `TTagValue`.`id_iobj` = `TInfoObject`.`id_iobj`
  LEFT JOIN `TCategory` ON `TCategory`.`id_cat` = `TTagValue`.id_tgcl
WHERE
  `TTagValue`.`id_iobj` = ' . $companyID . '
  AND (`TInfoObject`.`id_clntp` = 1
  OR `TInfoObject`.`id_clntp` = 4
  OR `TInfoObject`.`id_clntp` = 5)
  UNION
SELECT "Логотип" AS `ukazat` , `img_src` AS `val` , "cln_logo" AS `type` , 1000 AS "order_t", "" AS category
FROM `img_company`
WHERE `img_company`.`id_iobj` =' . $companyID . ' AND `img_company`.`type`=1
UNION
SELECT "Карта" AS `ukazat` , `img_src` AS `val` , "cln_map" AS `type` , 1100 AS "order_t", "" AS category
FROM `img_company`
WHERE `img_company`.`id_iobj` =' . $companyID . ' AND `img_company`.`type`=2
ORDER BY
  order_t, type';//and `TTag`.`id_tag` in (2,3,4,5,6,7,8,9,13,14,15,16,20,21,25,27)
    $q = $modx->prepare($sql);
    $q->execute();
    $res = $q->fetchAll(PDO::FETCH_ASSOC);

    $sqlNew = 'SELECT * FROM  img_company_new WHERE id_iobj=' . $companyID . ' LIMIT 1;';
    $qNew = $modx->prepare($sqlNew);
    $qNew->execute();
    $resNew = $qNew->fetchAll(PDO::FETCH_ASSOC);

    if (count($res) > 0) {
        $ukazat = '';
        $val = '';
        $company = '';
        $id_com = '';
        $r = 0;
        $companyname = '';

//            Юрид имя
        $cln_full_name = '';
        $cln_name = '';
        $cln_full_address = '';
        $cln_logo = 'assets/images/company_logo.png';
        $cln_map = 'assets/images/map.png';

        $cln_address = [];
        $cln_phone = [];
        $cln_fax = [];
        $cln_sote_phone = [];
        $cln_orient = [];
        $cln_email = [];
        $cln_chieff = [];
        $cln_main_bk = [];
        // $cln_work_dscr = '';
        $cln_web_address = [];
        $cln_work_mode = [];
        // $cln_trade_name = '';
        $cln_key = [];
        $cln_icq = [];
        $cln_skype = [];
        $cln_referal = [];
        $cln_donate_msg = [];
        $cln_partners = [];
//            Объединили с cln_phone
        $cln_base_phone = [];
        $cln_base_work = [];
        $cln_web_gorod = [];

        //Категория
        $categoryarr = [];
        //Тэги
        $tags = [];

        foreach ($res as $row) {
            $phone = '';
            if (in_array($row['type'], ['cln_sote_phone', 'cln_raw_phone', 'cln_base_phone', 'cln_phone'])) {
                $phone = preg_replace('~\D+~', '', $row['val']);
                if (strlen($phone) > 6) {
                    $phone = '+' . $phone;
                }
            }
            //Логотип
            if ($row['type'] == 'cln_logo' and $row['val'] != '') {
                $cln_logo = $row['val'];
            }
            //Логотип
            //Карта
            if ($row['type'] == 'cln_map' and $row['val'] != '') {
                $cln_map = $row['val'];
            }
            //Карта
            //Категория
            if ($row['category']) {
                $categoryarr[] = $row['val'];
                //ТЭГИ
                /*if($row['type'] == "cln_trade_name" or $row['type']=="cln_work_dscr")
                {
                    $tags[] = $row['val'];
                }*/
                //ТЭГИ
            }
            //Категория

            //Юридическое название компании
            if ($row['type'] == 'cln_full_name') {
                $cln_full_name = $row['val'];
            }
            if ($row['type'] == 'official_name') {
                $companyname = $row['val'];
            }
            //Юридическое название компании
            //Наименование компании
            if ($row['type'] == 'cln_name') {
                $cln_name = $row['val'];
            }
            //Наименование компании
            //Адрес компании
            if (in_array($row['type'], ['cln_full_address', 'cln_address', 'cln_adt_address'])) {
                $cln_address[] = $row['val'];
            }
            //Адрес компании

            //Телефоны компании
            if (in_array($row['type'], ['cln_raw_phone', 'cln_base_phone', 'cln_phone'])) {
                $cln_phone[] = $phone;
            }
            //Телефоны компании
            //Факсы компании
            if (in_array($row['type'], ['cln_fax', 'cln_raw_fax'])) {
                $cln_fax[] = $row['val'];
            }
            //Факсы компании
            //Сотовый телефон
            if ($row['type'] == 'cln_sote_phone') {
                $cln_sote_phone[] = $row['val'];
            }
            //Сотовый телефон
            //Ориентир
            if ($row['type'] == 'cln_orient') {
                $cln_orient[] = $row['val'];
            }
            //Ориентир
//                Email
            if ($row['type'] == 'cln_email') {
                if ($row['val']) {
                    $cln_email[] = $row['val'];
                }
            }
//                Email
//            Директор
            if ($row['type'] == 'cln_chief') {
                if ($row['val']) {
                    $cln_chieff[] = $row['val'];
                }
            }
//                Директор
//            Глав.Бух

            if ($row['type'] == 'cln_main_bk') {
                if ($row['val']) {
                    $cln_main_bk[] = $row['val'];
                }
            }
//                            Глав бух
            /*//                        Описание деятельности
            if($row['type']=="cln_work_dscr")
            {
                if($cln_work_dscr!='')
                {
                    $cln_work_dscr .= ', '.$row['val'];
                }
                else{
                    $cln_work_dscr = $row['val'];
                }
            }else
            //                Основной вид деятельности*/
            if ($row['type'] == 'cln_web_address') {
                if ($row['val']) {
                    $cln_web_address[] = $row['val'];
                }
            }

//            Основной виде деятельности
//            Режим работы
            if ($row['type'] == 'cln_work_mode') {
                $cln_work_mode[] = $row['val'];
            }
//            Режим работы
//            Товар
            /* if($row['type']=="cln_trade_name")
             {
                 if($cln_trade_name!='')
                 {
                     $cln_trade_name .= ', '.$row['val'];
                 }
                 else{
                     $cln_trade_name = $row['val'];
                 }
             }*/
//            Товар
//            Группа
            if ($row['type'] == 'cln_key') {
                $cln_key[] = $row['val'];
            }
//            Группа
//            ICQ
            if ($row['type'] == 'cln_icq') {
                $cln_icq[] = $row['val'];
            }
//            ICQ
//            SKYPE
            if ($row['type'] == 'cln_skype') {
                $cln_skype[] = $row['val'];
            }
//            Skype
//            Ссылка
            if ($row['type'] == 'cln_referal') {
                if ($row['val']) {
                    $cln_referal[] = $row['val'];
                }
            }
//            Ссылка
//            Акция
            if ($row['type'] == 'cln_donate_msg') {
                $cln_donate_msg[] = $row['val'];
            }
//            Акция
//            Партнер
            if ($row['type'] == 'cln_partners') {
                $cln_partners[] = $row['val'];
            }
//            Партнер
//            Осн. вид деятельности
            if ($row['type'] == 'cln_base_work') {
                $cln_base_work[] = $row['val'];
            }
//            Основной виде деятельности
//            Web Gorod
            if ($row['type'] == 'cln_web_gorod') {
                $cln_web_gorod[] = $row['val'];
            }

//                Web gorod
        }
        //ТОВАРЫ И УСЛУГИ
        $tags_unique = array_unique($tags);

        $logo = '';
        if (count($resNew) == 1) {
            $file = 'assets/images/company_logo/' . $resNew[0]['logo_ic'];
            if (is_file($file)) {
                $logo = $file;
            }
        }
        $prices = [];
        // Какая-то проверка на наличие прайс-листов
        $query2 = $modx->prepare('SELECT description FROM new_user_attributes WHERE state=' . $companyID . ' LIMIT 1');
        $query2->execute();
        $qRes2 = $query2->fetchAll(PDO::FETCH_ASSOC);
        // Есть ли у фирмы прайсы
        if (count($qRes2) > 0) {
            $query = $modx->prepare('SELECT id_tcg, name_tcg, name_group, edizm_tcg, cena_tcg,desc_tcg, kod_tcg,
(SELECT value_tgvl FROM TTagValue tc WHERE tc.id_tgvl = tcg.id_cat) namecat,
(SELECT name_group FROM tinfo_clients_group tg WHERE tg.id_group = tcg.id_group AND tg.id_com=tcg.id_com LIMIT 1 ) groupsm
FROM   tinfo_clients_goods  tcg
WHERE tcg.id_com =' . $companyID . ' ORDER BY id_tcg');
            $query->execute();

            $prices = $query->fetchAll(PDO::FETCH_ASSOC);

        }
        $photos = [];

        $query = $modx->prepare('SELECT * FROM `companies_cards_meta` WHERE id_iobj=' . $companyID . ' LIMIT 1');
        $query->execute();
        $images = $query->fetchAll(PDO::FETCH_ASSOC);
        if (count($images) > 0) {
            $photos = json_decode($images['0']['images']);

        }

        $data = [
            'title' => $companyname,
            'title_legal' => $cln_full_name,
            'addresses' => $cln_address,
            'phones' => $cln_phone,
            'faxes' => $cln_fax,
            'mobile_phones' => $cln_sote_phone,
            'emails' => $cln_email,
            'websites' => $cln_web_address,
            'work_modes' => $cln_work_mode,
            'landmarks' => $cln_orient,
            'chief' => $cln_chieff,
            'categories' => $categoryarr,
            'prices' => $prices,
            'photos' => $photos,
            'logo' => $logo,
        ];

        if (count($resNew) == 1) {
            $res = $resNew[0];
            $data['coordinates'] = $res['latlog_ic'];
            $data['map_address'] = $res['adress_ic'];
        } else {
            $data['static_map'] = $cln_map;
        }

        return $data;
    } else {
        $modx->sendRedirect($modx->makeUrl($modx->getOption('error_page')));
    }
} else {
    $modx->sendRedirect($modx->makeUrl($modx->getOption('error_page')));
}