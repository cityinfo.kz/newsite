<?php
$setIcoPath = "/assets/images/icon/";
$sql = <<<SQL
select `TPrintCat`.`id_pcat` AS `id`,`TPrintCat`.`name_pcat` AS `name`
from `TPrintCat` 
INNER JOIN `TPrintLink` on `TPrintCat`.`id_pcat` = `TPrintLink`.`id_pb`
INNER JOIN  `TCategory` ON `TPrintLink`.`id_cat` = `TCategory`.`id_cat`
INNER JOIN `TIconPrintCat` ON `TPrintCat`.`id_pcat` = `TIconPrintCat`.`id_pcat`
where  `TPrintCat`.`id_pbook` = 9 and `TPrintCat`.`pid_pcat` <> 0
group by `TPrintCat`.`id_pcat`
order by `TPrintCat`.`name_pcat`,`TPrintCat`.`id_pcat`,`TCategory`.`name_cat`
SQL;

$q = $modx->prepare($sql);
$q->execute();

$categoriesWithIcons = [
    ["id" => 936, "title" => "АВТОМОБИЛИ", "icon" => "fas fa-car"],
    ["id" => 938, "title" => "БЕЗОПАСНОСТЬ", "icon" => "fas fa-user-shield"],
    ["id" => 937, "title" => "БИЗНЕС", "icon" => "fas fa-business-time"],
    ["id" => 939, "title" => "БИЛЕТЫ", "icon" => "fas fa-ticket-alt"],
    ["id" => 947, "title" => "ВЛАСТИ, ОБЩЕСТВО", "icon" => "fas fa-flag-usa"],
    ["id" => 940, "title" => "ДОМАШНИЕ ЖИВОТНЫЕ", "icon" => "fas fa-paw"],
    ["id" => 943, "title" => "ДОСУГ И ИСКУССТВО", "icon" => "fas fa-hat-wizard"],
    ["id" => 935, "title" => "КОМПЬЮТЕРЫ", "icon" => "fas fa-desktop"],
    ["id" => 942, "title" => "КРАСОТА И ЗДОРОВЬЕ", "icon" => "fas fa-running"],
    ["id" => 944, "title" => "МЕДИЦИНА", "icon" => "fas fa-heartbeat"],
    ["id" => 945, "title" => "НЕДВИЖИМОСТЬ", "icon" => "far fa-building"],
    ["id" => 946, "title" => "ОБРАЗОВАНИЕ", "icon" => "fas fa-university"],
    ["id" => 948, "title" => "РЕМОНТ", "icon" => "fas fa-tools"],
    ["id" => 949, "title" => "РЕСТОРАНЫ И КАФЕ", "icon" => "fas fa-wine-glass-alt"],
    ["id" => 941, "title" => "СВЯЗЬ", "icon" => "fas fa-mobile"],
    ["id" => 950, "title" => "СПОРТ", "icon" => "fas fa-volleyball-ball"],
    ["id" => 951, "title" => "СТРАХОВАНИЕ", "icon" => "fas fa-car-crash"],
    ["id" => 957, "title" => "СТРОИТЕЛЬНЫЕ ТОВАРЫ", "icon" => "fas fa-tools"],
    ["id" => 952, "title" => "ТАКСИ", "icon" => "fas fa-taxi"],
    ["id" => 953, "title" => "ТОВАРЫ ДЛЯ ДОМА", "icon" => "fas fa-home"],
    ["id" => 958, "title" => "ТОВАРЫ И МАГАЗИНЫ", "icon" => "fas fa-store-alt"],
    ["id" => 954, "title" => "ТУРИЗМ", "icon" => "fas fa-plane"],
    ["id" => 955, "title" => "УСЛУГИ", "icon" => "fab fa-aws"],
    ["id" => 956, "title" => "ФИНАНСЫ", "icon" => "fas fa-coins"]
];
$sqlCategories = $q->fetchAll(PDO::FETCH_ASSOC);

$categories = [];

foreach ($sqlCategories as $category) {
    foreach ($categoriesWithIcons as $categoryWithIcon) {
        if ((int)$category['id'] === (int)$categoryWithIcon['id']) {
            $icon = $categoryWithIcon['icon'];
            $categories[] = array_merge((array)$category, ['icon' => $icon]);
        }
    }
}

return $categories;