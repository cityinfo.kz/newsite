<?php
$totalVar = $modx->getOption('totalVar', $scriptProperties, 'total');
$modx->setPlaceholder($totalVar, 1000);
$limit = $modx->getOption('limit', $scriptProperties, 1000);
$offset = $modx->getOption('offset', $scriptProperties, 0);

/** @var pdoTools $pdoTools */
$pdoTools = $modx->getService('pdoTools');
$pdoFetch = $modx->getService('pdoFetch');

$where = [
    'user_id' => $modx->user->id,
    'deleted' => 0,
];
$config = [
    'class' => 'exchangeRate',
    'select' => [
        'exchangeRate' => '*',
    ],
    'where' => $where,
    'return' => 'data',
];
$config['sortby'] = 'id';
$config['sortdir'] = 'DESC';

$pdoFetch->setConfig($config);

$points = $pdoFetch->run();

$rows = '';
if (!empty($points)) {

    foreach ($points as $point) {

        $query = $modx->newQuery('exchCityName');
        $query->select(['exchCityName.*']);
        $query->where(['id' => $point['city_id']]);
        $query->prepare();
        $query->stmt->execute();
        $city = $query->stmt->fetchAll(PDO::FETCH_ASSOC)[0];
        $point['city_name'] = $city['name'];

        $rows .= $pdoTools->parseChunk('_exchange_point', $point);
    }
    $modx->setPlaceholder('total', count($points)); //new
}

return $pdoTools->parseChunk('_exchange_points', [
    'rows' => $rows,
]);