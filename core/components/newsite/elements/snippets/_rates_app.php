<?php
/** @var modX $modx */
$city = $modx->getOption('city', $scriptProperties, 4);
$mapCenter = $modx->getOption('mapCenter', $scriptProperties, [49.95, 82.61]);

$modx->regClientScript("
    <script>
        window.initialData = {
            cityId: {$city},
            mapCenter: [{$mapCenter[0]}, {$mapCenter[1]}]
        };
    </script>
", true);

echo '<div id="root"></div>';