<h4 class="text-center text-pelorous-600 text-xl mb-4">Продажа настенных карт-схем городов: Усть-Каменогорск, Семей,
    Зыряновск, Риддер.</h4>
<p>
    <b>Настенные схемы</b> состоят из листов в формате А1. Стоимость одного листа - 3000 тенге.
</p>
<p>
    <a class="highslide" onclick="return hs.expand(this)" href="assets/images/sell_maps/map_U-ka.jpg">
        <img title="Нажмите на картинку" src="assets/images/sell_maps/map_U-ka_1.jpg" alt="Highslide JS"/>
    </a>
</p>
<table class="table">
    <tbody>
    <tr>
        <td><b>Наименование </b></td>
        <td><b>Размер</b></td>
        <td><b>Стоимость</b></td>
    </tr>
    <tr>
        <td>Схема из 4-х листов. Дома и улицы мелкие, видно только общий контур города.</td>
        <td>1682 х 1188 мм.</td>
        <td>12 000 тенге</td>
    </tr>
    <tr>
        <td>Схема из 6 листов. Дома и улицы видно хорошо. (Рекомендуем)</td>
        <td>1781 х 1682 мм.</td>
        <td>18 000 тенге</td>
    </tr>
    <tr>
        <td>Схема из 9 листов. Дома и улицы видно хорошо. (Рекомендуем)</td>
        <td>2523 х 1781 мм.</td>
        <td>27 000 тенге</td>
    </tr>
    <tr>
        <td>Схема из 12 листов. Детализация домов крупная, улицы видно хорошо.</td>
        <td>2523 х 2378 мм.</td>
        <td>36 000 тенге</td>
    </tr>
    <tr>
        <td>Ламинирование (холодное) 1лист (А1) лицевая сторона</td>
        <td>А1</td>
        <td>750 тенге</td>
    </tr>
    <tr>
        <td colspan="3">Бумага (ватман) размер А1 - 841х594 мм.</td>
    </tr>
    </tbody>
</table>

<div class="flex mb-2">
    <div class="mr-2 text-center">
        <a class="highslide" onclick="return hs.expand(this)" href="assets/images/sell_maps/logistic.jpg">
            <img title="Нажмите на картинку" src="assets/images/sell_maps/logistic_2.jpg" alt=""/>
        </a>
        <p>Логистика</p>
    </div>
    <div class="mr-2 text-center">
        <a class="block highslide" onclick="return hs.expand(this)" href="assets/images/sell_maps/photo.jpg">
            <img title="Нажмите на картинку" src="assets/images/sell_maps/photo_2.jpg" alt=""/>
        </a>
        <p>Фотографии</p>
    </div>
    <div class="mr-2 text-center">
        <a class="block highslide" onclick="return hs.expand(this)" href="assets/images/sell_maps/object.jpg">
            <img title="Нажмите на картинку" src="assets/images/sell_maps/object_2_2.jpg" alt=""/>
        </a>
        <p>Выделенный объект</p>
    </div>
    <div class="text-center">
        <a class="block highslide" onclick="return hs.expand(this)" href="assets/images/sell_maps/object_2.jpg">
            <img title="Нажмите на картинку" src="assets/images/sell_maps/object2_2.jpg" alt=""/>
        </a>
        <p>Фрагмент</p>
    </div>
</div>

<p class="mb-2">
    Стоимость за размещение логотипов, фото, схем и т.д. зависит от объема работ и рассматриваются в индивидуальном
    порядке.
</p>
<p class="mb-2">
    Также, распечатаем Восточно-Казахстанскую область, и карту Казахстана с населенными пунктами и областными дорогами.
</p>

<div class="mb-2">
    <h4 class="text-lg">Отдел продаж.</h4>
    <p>т. 8 (7232) 24-40-55</p>
    <p>
        <b>E-mail</b>:info<a href="mailto:support@cityinfo.kz">@cityinfo.kz</a>
    </p>
    <p>
        <b><span class="underline">Наш адрес:</span></b>
    </p>
    <p>070004, г. Усть-Каменогорск, пр.Ауэзова,14/1 , офис 306</p>
</div>

<p>
    <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A72770915009a0e35e88f9c6419b536e042db095dcde52f3c5981747a44b5c2ca&amp;source=constructor"
            frameborder="0" width="100%" height="400"></iframe>
</p>


<script type="text/javascript" src="/assets/imageslide/js/highslide-with-gallery.js"></script>
<script type="text/javascript" src="/assets/imageslide/js/highslide-with-html.js"></script>

<link rel="stylesheet" type="text/css" href="/assets/imageslide/css/highslide.css"/>

<script type="text/javascript">
    hs.graphicsDir = '/assets/imageslide/css/graphics/';
    hs.align = 'center';
    hs.transitions = ['expand', 'crossfade'];
    hs.outlineType = 'rounded-white';
    hs.wrapperClassName = 'draggable-header';
    hs.fadeInOut = true;
    //hs.dimmingOpacity = 0.75;
    // Add the controlbar
    hs.addSlideshow({
        //slideshowGroup: 'group1',
        interval: 5000,
        repeat: false,
        useControls: true,
        fixedControls: 'fit',
        overlayOptions: {
            opacity: 0.75,
            position: 'bottom center',
            hideOnMouseOut: true
        }
    });
</script>
