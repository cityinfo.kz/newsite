{set $items = [
['title' => 'Новости (Каз)', 'link' => $_modx->config.atameken_video_kaz],
['title' => 'Новости (Рус)', 'link' => $_modx->config.atameken_video_rus],
]}
<div class="flex flex-wrap">
    {foreach $items as $item}
        <div class="w-full md:w-1/2 flex flex-col items-center">
            <h4 class="text-lg text-center mb-4">{$item.title}</h4>
            <div class="text-center">
                <iframe width="421" height="243"
                        src="https://www.youtube.com/embed/{$item.link | _get_youtube_video_id}"
                        allowfullscreen></iframe>
            </div>
        </div>
    {/foreach}
</div>