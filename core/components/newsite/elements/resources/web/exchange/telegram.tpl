<div class="mb-4 text-center">
    <img class="inline" alt="Telegram Bot" src="/assets/components/newsite/images/exchange/ExchangeRatesKzBot.png"/>
</div>

<p class="mb-2">Telegram бот <a href="https://t.me/ExchangeRatesKzBot" target="_blank">@ExchangeRatesKzBot</a> позволяет
    получить
    информацию по выгодным курсам валют в обменных пунктах не открывая браузер. Для использования бота должен быть
    установлен мессенджер telegram&nbsp;<a href="https://telegram.org/" target="_blank">https://telegram.org/</a>.</p>

<p class="mb-4"><b>Инструкция по получению выгодных курсов валют через Telegram (изображения кликабельны):</b></p>

<p class="mb-4 text-center">1. Перейти по ссылке <a href="https://t.me/ExchangeRatesKzBot" target="_blank">@ExchangeRatesKzBot</a>
    и
    нажать
    Start:</p>

<p class="text-center mb-4">
    <a href="/assets/components/newsite/images/exchange/step2.png" data-lightbox="telegram-bot">
        <img class="inline" alt="" height="400" src="/assets/components/newsite/images/exchange/step1.png" width="225"/>
    </a>
</p>

<p class="text-center  mb-4">2. Выбрать город:</p>

<p class="text-center mb-4">
    <a href="/assets/components/newsite/images/exchange/step2.png" data-lightbox="telegram-bot">
        <img class="inline" alt="" height="400" src="/assets/components/newsite/images/exchange/step2.png" width="225"/>
    </a>
</p>

<p class="text-center mb-4">3. Выбрать интересующую котировку:</p>

<p class="text-center mb-4">
    <a href="/assets/components/newsite/images/exchange/step3.png" data-lightbox="telegram-bot">
        <img class="inline" alt="" height="400" src="/assets/components/newsite/images/exchange/step3.png" width="225"/>
    </a>
</p>

<p class="text-center mb-4">4. Получить результат:</p>

<p class="text-center mb-4">
    <a href="/assets/components/newsite/images/exchange/step4.png" data-lightbox="telegram-bot">
        <img class="inline" alt="" height="400" src="/assets/components/newsite/images/exchange/step4.png" width="225"/>
    </a>
</p>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/css/lightbox.min.css"
      integrity="sha256-tBxlolRHP9uMsEFKVk+hk//ekOlXOixLKvye5W2WR5c=" crossorigin="anonymous"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/js/lightbox-plus-jquery.min.js"
        integrity="sha256-j4lH4GKeyuTMQAFtmqhxfZbGxx+3WS6n2EJ/NTB21II=" crossorigin="anonymous"></script>
<script>
    lightbox.option({
        'resizeDuration': 200,
        'fadeDuration': 300,
        'wrapAround': true
    })
</script>
