<p><strong>Информацию о сеансах можно уточнять по т. 13-55 (звонок бесплатный) и 8-777-646-13-55.</strong></p>

<hr style="width: 100%; clear: right;" />
<h1 style="color: #2671ad; text-align: center;">Кинотеатр «ЭХО»</h1>
<p style="color: #2671ad; text-align: center; font-size: 16px; font-weight: 600;">Смотрите на большом экране</p>

<p>{*Выводит изображения, описания, трейлеры*}
    {$_modx->runSnippet('!pdoResources', [
    'parents' => 108,
    'resources'=> $_modx->config.active_films,
    'tpl' => '@INLINE <div>{$content}</div>',
    'select' => ['modResource' => 'id,pagetitle,content'],
    ])}
</p>
<div style="clear: both;"></div>
<p>
    {*Выводит таблицу сеансов и стоимости билетов*}
    {$_modx->runSnippet('!pdoResources', [
    'parents'=> 794,
    'limit'=> 1,
    'tpl' => '@INLINE <div>{$content}</div>',
    'sortby' => 'id',
    'select' => ['modResource' => 'id,pagetitle,content'],
    ])}
</p>