<h4 class="text-center text-xl">График движения поездов по вокзалу ст. «Защита» 2019 г.</h4>
<p>В расписании возможны изменения, информацию уточняйте по телефону: <strong>105</strong></p>
<p>
    <iframe style="-moz-box-shadow: 0 2px 3px rgba(0, 0, 0, 0.5); -webkit-box-shadow: 0 2px 3px rgba(0, 0, 0, 0.5); box-shadow: 0 2px 3px rgba(0, 0, 0, 0.5); overflow: hidden; border: 0; width: 100%; height: 302px;"
            src="https://rasp.yandex.kz/informers/station/9619142/?type=train" frameborder="0" width="320"
            height="240"></iframe>
</p>
<h1>График движения поездов по вокзалу ст. «Усть-Каменогорск» 2017 г.</h1>
<p>
    <iframe style="-moz-box-shadow: 0 2px 3px rgba(0, 0, 0, 0.5); -webkit-box-shadow: 0 2px 3px rgba(0, 0, 0, 0.5); box-shadow: 0 2px 3px rgba(0, 0, 0, 0.5); overflow: hidden; border: 0; width: 100%; height: 302px;"
            src="https://rasp.yandex.kz/informers/station/9619282/?size=5&amp;color=1&amp;type=schedule" frameborder="0"
            width="320" height="240"></iframe>
</p>
<h1>График движения поездов по вокзалу ст. «Ново-Усть-Каменогорск» 2017 г.</h1>
<p>
    <iframe style="-moz-box-shadow: 0 2px 3px rgba(0, 0, 0, 0.5); -webkit-box-shadow: 0 2px 3px rgba(0, 0, 0, 0.5); box-shadow: 0 2px 3px rgba(0, 0, 0, 0.5); overflow: hidden; border: 0; width: 100%; height: 302px;"
            src="https://rasp.yandex.kz/informers/station/9619294/?size=5&amp;color=1&amp;type=schedule" frameborder="0"
            width="320" height="240"></iframe>
</p>