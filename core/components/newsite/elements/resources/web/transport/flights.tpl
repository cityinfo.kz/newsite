<h4 class="text-center text-xl">Расписание самолетов на  2019 г.</h4>
<p> *В расписании самолетов указано местное время для каждого аэропорта.<br/> Будьте внимательны! В расписании возможны
    изменения, информацию уточняйте по телефону: <strong>77-84-84</strong></p>
<hr style="width: 100%;"/>
<p><strong>Расписание вылетов из г. Усть-Каменогоск</strong><span style="line-height: 1.5em;"> </span></p>
<p>
    <iframe style="-moz-box-shadow: 0 2px 3px rgba(0, 0, 0, 0.5); -webkit-box-shadow: 0 2px 3px rgba(0, 0, 0, 0.5); box-shadow: 0 2px 3px rgba(0, 0, 0, 0.5); overflow: hidden; border: 0; width: 100%; height: 302px;"
            src="https://rasp.yandex.kz/informers/station/9623902/?type=tablo" frameborder="0" width="320"
            height="240"></iframe>
</p>
<p>Источник: <a href="http://aeroport.kz/">http://aeroport.kz/</a></p>
<p>Справочная аэропорта - 77-84-84</p>