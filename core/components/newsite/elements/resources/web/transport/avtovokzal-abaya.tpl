<div class="overflow-x-auto">
    <table class="table is-bordered" border="1">
        <colgroup>
            <col/>
            <col/>
            <col/>
            <col/>
            <col/>
            <col/>
        </colgroup>
        <tbody>
        <tr>
            <td>
                <p align="CENTER">Наименование</p>
                <p align="CENTER"> </p>
                <p align="CENTER">маршрута</p>
            </td>
            <td>
                <p align="CENTER">Протяж-ть</p>
                <p align="CENTER"> </p>
                <p align="CENTER">(км.)</p>
            </td>
            <td>
                <p align="CENTER">Время в пути</p>
            </td>
            <td colspan="2">
                <p align="CENTER">Отпр. с нач. пункта</p>
            </td>
            <td>
                <p align="CENTER">Стоимость билета</p>
                <p align="CENTER"> </p>
                <p align="CENTER">(тенге)</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск – Алматы</p>
            </td>
            <td>
                <p align="CENTER">1134</p>
            </td>
            <td>
                <p align="CENTER">24.10</p>
            </td>
            <td colspan="2">
                <p align="CENTER">  19.00</p>
            </td>
            <td>
                <p align="CENTER">7000</p>
            </td>
        </tr>
        <tr>
            <td colspan="1">Усть-Каменогорск - Алтайский</td>
            <td colspan="1">      59</td>
            <td style="text-align: center;" colspan="1">01.45</td>
            <td style="text-align: center;" colspan="2"> 9.05  11.30  14.05    18.30</td>
            <td style="text-align: center;" colspan="1">500</td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск – Нур-Султан</p>
            </td>
            <td>
                <p align="CENTER">1084</p>
            </td>
            <td>
                <p align="CENTER">23.00</p>
            </td>
            <td colspan="2">
                <p align="CENTER">      12.00 16.40</p>
            </td>
            <td>
                <p align="CENTER">7000</p>
            </td>
        </tr>
        <tr>
            <td colspan="1">Усть-Каменогорск - Асу-Булак</td>
            <td colspan="1">      96</td>
            <td colspan="1">       2.00</td>
            <td colspan="2">
                <div style="text-align: center;">  11.35 14.35  16.25  18.00 </div>
                <div style="text-align: center;"> </div>
            </td>
            <td style="text-align: center;" colspan="1">800</td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск - Аягуз</p>
            </td>
            <td>
                <p align="CENTER">320</p>
            </td>
            <td>
                <p align="CENTER">7.30</p>
            </td>
            <td colspan="2">
                <p align="CENTER">17.00</p>
            </td>
            <td>
                <p align="CENTER">1900</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск – Белоусовка</p>
            </td>
            <td>
                <p align="CENTER">25</p>
            </td>
            <td>
                <p align="CENTER">0.51</p>
            </td>
            <td colspan="2">
                <p align="CENTER">6.30 8.10 9.15  10.40 12.20 14.05 15.00 16.15 17.20 18.20  20.30</p>
            </td>
            <td>
                <p align="CENTER">230</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск – Барнаул</p>
                <p>ч\з Змеиногорск</p>
            </td>
            <td>
                <p align="CENTER">549</p>
            </td>
            <td>
                <p align="CENTER">12.00</p>
            </td>
            <td colspan="2">
                <p align="CENTER">8.30</p>
                <p align="CENTER">20.00</p>
            </td>
            <td>
                <p align="CENTER">5500</p>
                <p align="CENTER">6700</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск – Бобровка</p>
            </td>
            <td>
                <p align="CENTER">29</p>
            </td>
            <td>
                <p align="CENTER">0.52</p>
            </td>
            <td colspan="2">
                <p align="CENTER">8.10 13.25 17.15</p>
            </td>
            <td>
                <p align="CENTER">280</p>
            </td>
        </tr>
        <tr>
            <td colspan="1">Усть-Каменогорск - Большенарым</td>
            <td colspan="1">     282</td>
            <td style="text-align: center;" colspan="1">6.30</td>
            <td style="text-align: center;" colspan="2">8.10</td>
            <td style="text-align: center;" colspan="1">2300</td>
        </tr>
        <tr>
            <td colspan="1">Усть-Каменогорск - Глубокое</td>
            <td colspan="1">      34</td>
            <td style="text-align: center;" colspan="1">1.15</td>
            <td style="text-align: center;" colspan="2">7.30  8.20 10.35  14.30 17.50  18.50 </td>
            <td style="text-align: center;" colspan="1">300</td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск – Зайсан</p>
            </td>
            <td>
                <p align="CENTER">477</p>
            </td>
            <td>
                <p align="CENTER">10.55</p>
            </td>
            <td colspan="2">
                <p align="CENTER">07.00</p>
            </td>
            <td>
                <p align="CENTER">3300</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск – Змеиногорск</p>
            </td>
            <td>
                <p align="CENTER">195</p>
            </td>
            <td>
                <p align="CENTER">5.20</p>
            </td>
            <td colspan="2">
                <p align="CENTER">8.30</p>
            </td>
            <td>
                <p align="CENTER">2025</p>
            </td>
        </tr>
        <tr>
            <td colspan="1">Усть-Каменогорск - Алтай</td>
            <td colspan="1">     182</td>
            <td colspan="1">       3.50</td>
            <td colspan="2">                                      7.30   9.30   11.30      15.00   18.30 </td>
            <td colspan="1">           1800</td>
        </tr>
        <tr>
            <td colspan="1"> Усть-Каменогорск - Алаколь</td>
            <td colspan="1">    635</td>
            <td colspan="1">     12.00</td>
            <td colspan="2">                                                     летний график</td>
            <td colspan="1">         5000</td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск – Караганда</p>
            </td>
            <td>
                <p align="CENTER">1032</p>
            </td>
            <td>
                <p align="CENTER">21.00</p>
            </td>
            <td colspan="2">
                <p align="CENTER">11.10 (ежедневно)</p>
                <p align="CENTER"> </p>
            </td>
            <td>
                <p align="CENTER">6000</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск - Красноярск</p>
            </td>
            <td>
                <p align="CENTER">1633</p>
            </td>
            <td>
                <p align="CENTER">27.40</p>
            </td>
            <td colspan="2">
                <p align="CENTER">10.00(сб ч/з неделю )</p>
            </td>
            <td>
                <p align="CENTER">15000</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск – Катон ч\з Алтай</p>
            </td>
            <td>
                <p align="CENTER">358</p>
            </td>
            <td>
                <p align="CENTER">7.30</p>
            </td>
            <td colspan="2">
                <p align="CENTER">8.25 11.00</p>
            </td>
            <td>
                <p align="CENTER">2700</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск - Курчум</p>
            </td>
            <td>
                <p align="CENTER">215</p>
            </td>
            <td>
                <p align="CENTER">5.20</p>
            </td>
            <td colspan="2">
                <p align="CENTER">9.00 13.45</p>
            </td>
            <td>
                <p align="CENTER">1800</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск – Маканчи</p>
            </td>
            <td>
                <p align="CENTER">540</p>
            </td>
            <td>
                <p align="CENTER">12.35</p>
            </td>
            <td colspan="2">
                <p align="CENTER">18.00 </p>
            </td>
            <td>
                <p align="CENTER">3200</p>
            </td>
        </tr>
        <tr>
            <td colspan="1">Усть-Каменогорск - п. Касыма Кайсенова</td>
            <td colspan="1">      18</td>
            <td style="text-align: center;" colspan="1">0.35</td>
            <td colspan="2">
                <div style="text-align: center;">6.05 6.30 7.15 7.35 8.05 8.40 9.05 9.40 10.10 </div>
                <div style="text-align: center;">10.40 11.40 12.40 13.20 14.00  15.30 16.35 17.05 17.40 18.05 18.35
                    19.15 19.45 20.25 
                </div>
            </td>
            <td style="text-align: center;" colspan="1">120</td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск – Новосибирск</p>
            </td>
            <td>
                <p align="CENTER">860</p>
            </td>
            <td>
                <p align="CENTER">18.00</p>
            </td>
            <td colspan="2">
                <p align="CENTER"><strong> </strong>17.00  18.30 20.00  21.00 </p>
                <p align="CENTER"> </p>
            </td>
            <td>
                <p align="CENTER"><strong> </strong> 9000<strong> </strong></p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск – Новокузнецк</p>
            </td>
            <td>
                <p align="CENTER">1042</p>
            </td>
            <td>
                <p align="CENTER">19.40</p>
            </td>
            <td colspan="2">
                <p align="CENTER"> временно отменен</p>
            </td>
            <td>
                <p align="CENTER">10500</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск – Омск</p>
            </td>
            <td>
                <p align="CENTER">1003</p>
            </td>
            <td>
                <p align="CENTER">20.15</p>
            </td>
            <td colspan="2">
                <p align="CENTER">9.45 </p>
            </td>
            <td>
                <p align="CENTER">10000</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск – Октябрьский</p>
            </td>
            <td>
                <p align="CENTER">112</p>
            </td>
            <td>
                <p align="CENTER">3.00</p>
            </td>
            <td colspan="2">
                <p align="CENTER">10.00 12.30 17.30</p>
            </td>
            <td>
                <p align="CENTER">1300</p>
            </td>
        </tr>
        <tr>
            <td colspan="1">Усть-Каменогорск - Первомайский</td>
            <td colspan="1">      73</td>
            <td style="text-align: center;" colspan="1">2.00</td>
            <td style="text-align: center;" colspan="2">10.00  12.15   15.35</td>
            <td style="text-align: center;" colspan="1">600</td>
        </tr>
        <tr>
            <td colspan="1">Усть-Каменогорск - Предгорное (ч/з Уварово)</td>
            <td colspan="1">      58</td>
            <td style="text-align: center;" colspan="1">2.00</td>
            <td style="text-align: center;" colspan="2">7.55  13.00  17.25</td>
            <td style="text-align: center;" colspan="1">450</td>
        </tr>
        <tr>
            <td colspan="1">Усть-Каменогорск - Предгорное</td>
            <td colspan="1">      49</td>
            <td style="text-align: center;" colspan="1">1.30</td>
            <td style="text-align: center;" colspan="2">11.30  12.00  16.25</td>
            <td style="text-align: center;" colspan="1">450</td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск –</p>
                <p>Павлодар ч\з Таврическое</p>
            </td>
            <td>
                <p align="CENTER">562</p>
            </td>
            <td>
                <p align="CENTER">12.00</p>
            </td>
            <td colspan="2">
                <p align="CENTER"> 21.00</p>
            </td>
            <td>
                <p align="CENTER">4000</p>
                <p align="CENTER"> </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск –</p>
                <p>Павлодар ч\з Шемонаиху</p>
            </td>
            <td>
                <p align="CENTER">616</p>
            </td>
            <td>
                <p align="CENTER">12.50</p>
            </td>
            <td colspan="2">
                <p align="CENTER">19.10</p>
            </td>
            <td>
                <p align="CENTER">4000</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск –Риддер</p>
            </td>
            <td>
                <p align="CENTER">118</p>
            </td>
            <td>
                <p align="CENTER">3.00</p>
            </td>
            <td colspan="2">
                <p align="CENTER">7.30 9.00 10.30 12.00 13.30  15.00 16.30 17.30  19.00</p>
            </td>
            <td>
                <p align="CENTER">1200</p>
            </td>
        </tr>
        <tr>
            <td colspan="1">Усть-Каменогорск - Самарское- Белое </td>
            <td colspan="1">     195</td>
            <td style="text-align: center;" colspan="1">5.30</td>
            <td style="text-align: center;" colspan="2">14.10</td>
            <td style="text-align: center;" colspan="1">1500</td>
        </tr>
        <tr>
            <td colspan="1">Усть-Каменогорск - Самарское - Каракол</td>
            <td colspan="1">     250</td>
            <td colspan="1">       6.30</td>
            <td style="text-align: center;" colspan="2">9.30</td>
            <td style="text-align: center;" colspan="1">1600</td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск – Серебрянск</p>
            </td>
            <td>
                <p align="CENTER">112</p>
            </td>
            <td>
                <p align="CENTER">2.40</p>
            </td>
            <td colspan="2">
                <p align="CENTER">11.30  15.10    18.00</p>
            </td>
            <td>
                <p align="CENTER">1200</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск - Семей</p>
            </td>
            <td>
                <p align="CENTER">220</p>
            </td>
            <td>
                <p align="CENTER">3.30</p>
            </td>
            <td colspan="2">
                <p align="CENTER">6.45 8.00  13.00</p>
            </td>
            <td>
                <p align="CENTER">2100</p>
            </td>
        </tr>
        <tr>
            <td colspan="1">Усть-Каменогорк - Семей</td>
            <td colspan="1">     249</td>
            <td colspan="1">       3.50</td>
            <td style="text-align: center;" colspan="2">14.00 15.15 (ч/з Шульбинск)16.10 17.15 18.10 18.45</td>
            <td style="text-align: center;" colspan="1">2100</td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск –Тарханка</p>
            </td>
            <td>
                <p align="CENTER">35,5</p>
            </td>
            <td>
                <p align="CENTER">1.10</p>
            </td>
            <td colspan="2">
                <p align="CENTER">8.00 13.30 17.10</p>
            </td>
            <td>
                <p align="CENTER">300</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск –Томск</p>
            </td>
            <td>
                <p align="CENTER">1120</p>
            </td>
            <td>
                <p align="CENTER">21.05</p>
            </td>
            <td colspan="2">
                <p align="CENTER">10.00 (пт)</p>
            </td>
            <td>
                <p align="CENTER">10000</p>
            </td>
        </tr>
        <tr>
            <td colspan="1">Усть-Каменогорск - Украинка</td>
            <td colspan="1">      45</td>
            <td style="text-align: center;" colspan="1">1.00</td>
            <td style="text-align: center;" colspan="2">7.45  17.25</td>
            <td style="text-align: center;" colspan="1">300</td>
        </tr>
        <tr>
            <td colspan="1">Усть-Каменогорск - Урджар</td>
            <td colspan="1">     493</td>
            <td style="text-align: center;" colspan="1">11.25</td>
            <td style="text-align: center;" colspan="2">18.00</td>
            <td style="text-align: center;" colspan="1">3000</td>
        </tr>
        <tr>
            <td>
                <p>Усть-Каменогорск – Шемонаиха</p>
            </td>
            <td>
                <p align="CENTER">118</p>
            </td>
            <td>
                <p align="CENTER">2.45</p>
            </td>
            <td colspan="2">
                <p align="CENTER">7.35 8.15 10.15 11.30 13.00 13.50 14.40 15.30 16.40 17.20 18.40</p>
            </td>
            <td>
                <p align="CENTER">1500/1800</p>
            </td>
        </tr>
        </tbody>
    </table>
</div>