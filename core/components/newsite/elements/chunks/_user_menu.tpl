{set $menu = [
['url' => $_modx->makeUrl(3077), 'title' => 'Добавить обменный пункт'],
['url' => $_modx->makeUrl(3093), 'title' => 'Мои обменные пункты'],
]}
{* @todo добавить возможность редактирования профиля *}
{* ['url' => $_modx->makeUrl(3943), 'title' => 'Редактирование профиля'],  *}
<h4 class="text-lg">
    Добро пожаловать!
</h4>
<p class="text-lg mb-4">{$_pls['fullname']}</p>
<ul class="list-none">
    {foreach $menu as $item}
        <li class="w-full mb-3">
            <a class="text-pelorous-600 font-medium text-sm" href="{$item.url}">{$item.title}</a>
        </li>
    {/foreach}
</ul>
<div class="my-4">
    <form action="{$_modx->makeUrl($_modx->resource.id)}" method="post">
        <input type="hidden" name="action" value="logout">
        <button type="submit" class="text-red-600 font-bold text-sm">
            Выход
        </button>
    </form>
</div>

