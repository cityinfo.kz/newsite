{set $inputs = [
['label' => 'Имя', 'name' => 'name', 'type' => 'text', 'placeholder' => 'Например: Роман Андреевич'],
['label' => 'Email', 'name' => 'email', 'type' => 'email', 'placeholder' => 'info@cityinfo.kz'],
['label' => 'Пароль', 'name' => 'password', 'type' => 'password', 'placeholder' => '******************'],
['label' => 'Подтверждение пароля', 'name' => 'password_confirm', 'type' => 'password', 'placeholder' => '******************'],
]}
<form class="w-full max-w-sm mx-auto" action="{$_modx->makeUrl($_modx->resource.id)}" method="POST">
    {foreach $inputs as $input}
        <div class="flex flex-wrap -mx-3 mb-2">
            <div class="w-full px-3">
                <label class="block uppercase tracking-wide text-black text-xs font-bold mb-2" for="{$input.name}">
                    {$input.label}
                </label>
                {set $placeholder = "reg.{$input.name}"}
                {set $errorKey = "reg.error.{$input.name}"}
                {set $error = $_modx->getPlaceholder($errorKey)}
                <input
                        id="{$input.name}"
                        class="appearance-none block w-full {if $error != ''}border border-red-500{else}border border-gray-600{/if} bg-pelorous-100 text-gray-900 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
                        type="{$input.type}"
                        placeholder="{$input.placeholder}"
                        name="{$input.name}"
                        value="{$_modx->getPlaceholder($placeholder)}"
                >
                {if $error != ''}
                    <p class="text-red-500 text-xs italic">{$_modx->getPlaceholder($errorKey)}</p>
                {/if}
            </div>
        </div>
    {/foreach}
    <div class="w-full mb-6">
        <p class="text-sm italic">Все поля обязательны для заполнения</p>
    </div>
    <div class="flex items-center justify-between">
        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-3 px-4 rounded focus:outline-none" type="submit">
           Зарегистрироваться
        </button>
    </div>

</form>