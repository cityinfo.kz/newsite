{var $headImage = "/assets/components/newsite/images/logov3.png"}
{set $version = 2}
{var $sitePageTitle = "{$_modx->resource.longtitle != '' ? $_modx->resource.longtitle : $_modx->resource.pagetitle} | {$_modx->config.site_name}" }
{var $description = $_modx->resource.description}
{$_modx->runSnippet('!title_text', [])}
{if $_modx->getPlaceholder('card_title') != ""}
    {var $sitePageTitle = "{$_modx->getPlaceholder('card_title')}" }
{/if}
{if $_modx->getPlaceholder('card_description') != ""}
    {var $description = "{$_modx->getPlaceholder('card_description')}" }
{/if}


<head>
    <title>{$sitePageTitle | escape}</title>
    <meta name="description" content="{$description | escape}"/>
    <base href="{$modx->getOption('site_url')}">
    <meta charset="UTF-8">

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=1">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=1">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=1">
    <link rel="manifest" href="/site.webmanifest?v=1">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <meta http-equiv="X-UA-Compatible" content="IE=10; IE=EDGE"/>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
    <meta name="format-detection" content="telephone=no"/>
    <meta name="rating" content="safe for kids"/>
    <meta name="geo.region" content="KZ-VOS"/>
    <meta name="geo.placename" content="Ust-Kamenogorsk"/>
    <meta name="geo.position" content="49.946619;82.616833"/>
    <meta name="ICBM" content="49.946619, 82.616833"/>

    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{$sitePageTitle | escape}"/>
    <meta property="og:description" content="{$description | escape}"/>

    <link rel="image_src" href="{$headImage}"/>

    <meta property="og:image" content="{$headImage}"/>
    <meta property="og:url" content="{$modx->getOption('site_url')}"/>
    <meta property="og:site_name" content="{$modx->getOption('site_name')}"/>

    <link crossorigin="anonymous" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" rel="stylesheet">
    <link crossorigin="anonymous" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.1/tiny-slider.css"
          integrity="sha256-6biQaot1QLisz9KkkcCCHWvW2My9SrU6VtqJBv8ChCM=" rel="stylesheet">
</head>
