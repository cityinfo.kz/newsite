<div class="show-bar">
    <div class="container mx-auto">
        <div class="flex flex-wrap justify-between p-4">
            <div class="logo w-full text-center  sm:w-1/2  sm:text-left">
                <a href="/">
                    <img class="logo " src="/assets/components/cityinfo/images/logov3.png"
                         alt="{$_modx->config.site_name}"/>
                </a>
            </div>
            <div class="w-full text-center  sm:w-1/2  sm:text-right">
                {$_modx->runSnippet('!pdoResources', [
                'parents' => 3874,
                'sortby' => 'RAND()',
                'limit' => 1,
                'tpl' => '@INLINE {$content}',
                'select' => ['modResource' => 'pagetitle,content'],
                ])}
            </div>
        </div>
    </div>
</div>