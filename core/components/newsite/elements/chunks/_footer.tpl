<footer class="container bg-black-light p-4 mx-auto">
    <div class="flex w-full justify-between">
        <div class="flex items-center text-white w-1/2">
            © 1997–{'' | date_format : '%Y'} «Справочная по товарам и услугам»
        </div>
        <div class="text-xl lg:text-3xl w-1/2">
            <ul class="list-none flex w-full justify-end">
                <li class="mx-2">
                    <a href="https://vk.com/cityinfokz" target="_blank" rel="noreferrer noopener">
                        <i class="fab fa-vk"></i>
                    </a>
                </li>
                <li class="mx-2">
                    <a href="https://twitter.com/cityinfo_kz" target="_blank" rel="noreferrer noopener">
                        <i class="fab fa-twitter"></i>
                    </a>
                </li>
                <li class="mx-2">
                    <a href="https://www.instagram.com/cityinfo.kz/" target="_blank" rel="noreferrer noopener">
                        <i class="fab fa-instagram"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</footer>