<div class="banners flex flex-col w-full lg:w-1/5 xl:w-1/6 py-4 items-end">
    <p class="mb-2">
        <a href="{$_modx->makeUrl(4073)}">
            <img src="/assets/components/newsite/images/exchange/telegram.jpg" style="width: 180px; height: auto"
                 alt="Telegram Бот"/>
        </a>
    </p>

    {$_modx->runSnippet('pdoResources', [
    'parents' => 3880,
    'tpl' => '@INLINE <div class="mb-2">{$content}</div>',
    'sortdir'=> 'ASC',
    'sortby' => 'menuindex',
    'select' => ['modResource' => 'pagetitle,content'],
    ])}

    {$_modx->runSnippet('!pdoResources', [
    'parents' => 191,
    'limit' => 3,
    'tpl' => '@INLINE <div>{$content}</div>',
    'sortby' => 'RAND()',
    'select' => ['modResource' => 'pagetitle,content'],
    ])}
</div>