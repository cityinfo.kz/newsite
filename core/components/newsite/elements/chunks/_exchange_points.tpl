{* KZ Валюта Ауэзовский 258 *}
{*if $modx->user->id === 30}
    <div class="container mx-auto px-2 mb-4">
        <div class="bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4" role="alert">
            <p class="font-bold mb-2">Предупреждение!</p>
            <p class="mb-1"><b>06.10.2020 в 10:46</b> вашим обменным пунктом было <b>допущено нарушение</b> п. 2.4.</p>
            <p class="mb-3">В этой связи информируем вас, что <b>при повторном нарушении</b> исполнения по договору «в
                части предоставления достоверной информации курсов валют", нашей стороной по договору предусмотрено
                право на временное <b>отключение</b> от ресурса сайта Cityinfo.kz</p>
            <p class="text-right">Директор ТОО «Cityinfo.kz»<br>
                Конакова Нина Александровна
                <br>
                06.10.2020</p>
        </div>
    </div>
{/if*}
{*{set $banks = [62, 292,50, 300, 319]}*}
{*{foreach $banks as $bankId}*}
{*    {if $modx->user->id === $bankId}*}
{*        <div class="container mx-auto px-2 mb-4">*}
{*            <div class="bg-blue-100 border-t-4 border-blue-500 rounded-b text-blue-900 px-4 py-3 shadow-md"*}
{*                 role="alert">*}
{*                <div class="flex">*}
{*                    <div class="py-1">*}
{*                        <svg class="fill-current h-6 w-6 text-blue-500 mr-4" xmlns="http://www.w3.org/2000/svg"*}
{*                             viewBox="0 0 20 20">*}
{*                            <path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/>*}
{*                        </svg>*}
{*                    </div>*}
{*                    <div class="py-1">*}
{*                        <p class="text-sm">Уведомляем, что с <b>01.01.20г.</b> стоимость услуг ТОО «Cityinfo.kz» в месяц*}
{*                            составит <b>6500 тг</b>.</p>*}
{*                    </div>*}
{*                </div>*}
{*            </div>*}
{*        </div>*}
{*    {/if}*}
{*{/foreach}*}
<div class="flex flex-wrap">
    {$rows}
</div>
