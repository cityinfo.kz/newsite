<table class="table is-bordered">
    <thead>
    <tr>
        <th class="text-center" style="min-width: 150px;">Название / Время обновления</th>
        <th class="text-center">АИ 80</th>
        <th class="text-center">АИ 92</th>
        <th class="text-center">АИ 92 RU</th>
        <th class="text-center">АИ 93</th>
        <th class="text-center">АИ 95</th>
        <th class="text-center">АИ 95Prime</th>
        <th class="text-center">АИ 96</th>
        <th class="text-center">АИ 98</th>
        <th class="text-center">Д/т</th>
        <th class="text-center">Д/т*</th>
        {* <th class="text-center">Газ</th> *}
        <th class="text-center">Адрес</th>
    </tr>
    </thead>
    <tbody>
    {$rows}
    </tbody>
</table>