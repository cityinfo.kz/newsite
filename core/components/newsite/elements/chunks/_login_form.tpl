<form method="post" class="bg-white shadow-md px-8 pt-6 pb-8 mb-4" action="{$_modx->makeUrl($_modx->resource.id)}">
    <div class="mb-4">
        <label class="block text-black text-sm font-bold mb-2" for="username">
            Логин
        </label>
        <input class="appearance-none block w-full bg-pelorous-100 text-gray-900 border border-gray-600 py-2 px-2 leading-tight focus:outline-none focus:bg-white"
               id="username" type="text" placeholder="info@cityinfo.kz" name="username"
               required="" autofocus="">
    </div>
    <div class="mb-6">
        <label class="block text-black text-sm font-bold mb-2" for="password">
            Пароль
        </label>
        <input class="appearance-none block w-full bg-pelorous-100 text-gray-900 border border-gray-600 py-2 px-2 leading-tight focus:outline-none focus:bg-white"
               id="password" type="password" name="password" placeholder="******************">
    </div>
    {if $_pls['errors']}
        <div id="info-result" class="border px-4 py-3 my-4 bg-red-100 border-red-400 text-red-700" role="alert">
            {$_pls['errors']}
        </div>
    {/if}
    <div class="flex items-center justify-between">
        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 focus:outline-none"
                type="submit">
            Войти
        </button>
        <a class="inline-block align-baseline font-bold text-sm text-blue-500 hover:text-blue-800"
           href="{$_modx->makeUrl(3080)}">
            Регистрация
        </a>
    </div>
    <input type="hidden" name="action" value="login">
</form>