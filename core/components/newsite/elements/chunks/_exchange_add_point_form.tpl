{set $item = [
'id' => $_pls['id'] !: 0,
'cities' => $_pls['cities'],
'city_id' => $_pls['city_id'],
'name' => $_pls['name'] !: '',
'info' =>  $_pls['info'] !: '',
'phones' => $_pls['phones'] !: '',
'buyUSD' => $_pls['buyUSD'] !: '',
'sellUSD' => $_pls['sellUSD'] !: '',
'buyEUR' => $_pls['buyEUR'] !: '',
'sellEUR' => $_pls['sellEUR'] !: '',
'buyRUB' => $_pls['buyRUB'] !: '',
'sellRUB' => $_pls['sellRUB'] !: '',
'buyCNY' => $_pls['buyCNY'] !: '',
'sellCNY' => $_pls['sellCNY'] !: '',
'buyGBP' => $_pls['buyGBP'] !: '',
'sellGBP' => $_pls['sellGBP'] !: '',
'buyXAU' => $_pls['buyXAU'] !: '',
'sellXAU' => $_pls['sellXAU'] !: '',
'longitude' => $_pls['longitude'] !: '',
'latitude' => $_pls['latitude'] !: '',
'gross' => $_pls['gross'],
'buttonTitle' => $_pls['id'] ? 'Обновить данные': 'Добавить обменный пункт',
]}
<form method="post" id="form" class="w-full">
    <input type="hidden" name="id" value="{$item.id}"/>
    <input type="hidden" value="exchange/create" name="action">


    <div class="flex flex-wrap mb-6">
        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
            <label class="block uppercase tracking-wide text-black text-xs font-bold mb-2" for="city_id">
                Выберите город
            </label>
            <div class="relative">
                <select class="block appearance-none w-full bg-pelorous-100 text-gray-900 border border-gray-600 py-2 px-2 pr-8 leading-tight focus:outline-none focus:bg-white rounded-none"
                        name="city_id" id="city_id" tabindex="1">
                    {foreach $item.cities as $city}
                        <option value="{$city.id}"
                                {if $item.city_id == $city.id}selected=""{/if}>{$city.name}</option>
                    {/foreach}
                </select>
                <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                    </svg>
                </div>
            </div>
            <p class="error text-red-500 text-xs italic"></p>
        </div>
        <div class="w-full md:w-1/2 px-3">
            <label class="block uppercase tracking-wide text-black text-xs font-bold mb-2" for="name">
                Название обменного пункта
            </label>
            <input class="appearance-none block w-full bg-pelorous-100 text-gray-900 border border-gray-600 py-2 px-2 leading-tight focus:outline-none focus:bg-white"
                   type="text"
                   placeholder="Название обменного пункта"
                   name="name"
                   value="{$item.name}"
                   maxlength="50"
                   id="name"
                   tabindex="2"
            >
            <p class="error text-red-500 text-xs italic"></p>
        </div>
    </div>

    <div class="mb-6 w-full px-3">
        <label class="block uppercase tracking-wide text-black text-xs font-bold mb-2">Тип обменного пункта</label>
        <label class="text-black font-bold mr-4">
            <input class="mr-2 leading-tight" type="radio" {if $item.gross == 0}checked{/if} name="gross" value="0">
            <span class="text-sm">Розница</span>
        </label>

        <label class="text-black font-bold">
            <input class="mr-2 leading-tight" type="radio" {if $item.gross == 1}checked{/if} name="gross" value="1">
            <span class="text-sm">Оптовый</span>
        </label>
    </div>

    <div class="flex flex-wrap mb-6">
        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
            <label class="block uppercase tracking-wide text-black text-xs font-bold mb-2" for="info">
                Адрес
            </label>
            <input class="appearance-none block w-full bg-pelorous-100 text-gray-900 border border-gray-600 py-2 px-2 leading-tight focus:outline-none focus:bg-white"
                   type="text"
                   placeholder="Адрес"
                   name="info"
                   value="{$item.info}"
                   maxlength="50"
                   id="info"
                   tabindex="3"
            >
            <p class="error text-red-500 text-xs italic"></p>
        </div>
        <div class="w-full md:w-1/2 px-3">
            <label class="block uppercase tracking-wide text-black text-xs font-bold mb-2" for="info">
                Телефон
            </label>
            <input class="appearance-none block w-full bg-pelorous-100 text-gray-900 border border-gray-600 py-2 px-2 leading-tight focus:outline-none focus:bg-white"
                   type="text"
                   placeholder="+7 701 2345678"
                   name="phones"
                   value="{$item.phones}"
                   maxlength="50"
                   id="phones"
                   tabindex="4"
            >
            <p class="error text-red-500 text-xs italic"></p>
        </div>
    </div>

    {set $currencies = [
    ['name' => 'buyUSD', 'title' => 'Покупка USD', 'value' => $item.buyUSD, 'tabindex' => 5],
    ['name' => 'buyEUR', 'title' => 'Покупка EUR', 'value' => $item.buyEUR, 'tabindex' => 7],
    ['name' => 'buyRUB', 'title' => 'Покупка RUB', 'value' => $item.buyRUB, 'tabindex' => 9],
    ['name' => 'buyCNY', 'title' => 'Покупка CNY', 'value' => $item.buyCNY, 'tabindex' => 11],
    ['name' => 'buyGBP', 'title' => 'Покупка GBP', 'value' => $item.buyGBP, 'tabindex' => 13],
    ['name' => 'buyXAU', 'title' => 'Покупка Золота', 'value' => $item.buyXAU, 'tabindex' => 15],

    ['name' => 'sellUSD', 'title' => 'Продажа USD', 'value' => $item.sellUSD, 'tabindex' => 6],
    ['name' => 'sellEUR', 'title' => 'Продажа EUR', 'value' => $item.sellEUR, 'tabindex' => 8],
    ['name' => 'sellRUB', 'title' => 'Продажа RUB', 'value' => $item.sellRUB, 'tabindex' => 10],
    ['name' => 'sellCNY', 'title' => 'Продажа CNY', 'value' => $item.sellCNY, 'tabindex' => 12],
    ['name' => 'sellGBP', 'title' => 'Продажа GBP', 'value' => $item.sellGBP, 'tabindex' => 14],
    ['name' => 'sellXAU', 'title' => 'Продажа Золота', 'value' => $item.sellXAU, 'tabindex' => 16],
    ]}
    <div class="flex flex-wrap">
        {foreach $currencies as $currency last=$last}
            <div class="w-1/2 md:w-1/4 lg:w-1/6 px-2 mb-4">
                <div class="flex flex-col">
                    <label class="block uppercase tracking-wide text-black text-xs font-bold mb-1"
                           for="{$currency.name}">
                        {$currency.title}
                    </label>
                    <input id="{$currency.name}"
                           class="appearance-none block w-full bg-pelorous-100 text-gray-900 border border-gray-600 py-2 px-2 leading-tight focus:outline-none focus:bg-white"
                           type="text"
                           name="{$currency.name}"
                           maxlength="10"
                           value="{$currency.value}"
                           tabindex="{$currency.tabindex}"
                    >
                    <p class="error text-red-500 text-xs italic"></p>
                </div>
            </div>
        {/foreach}
    </div>

    <div class="flex flex-col">
        {*<div class="w-full">
            <h4 class="text-lg mb-4">
                Расположение обменного пункта на карте
            </h4>
            <input type="hidden" name="latitude" value="{$item.latitude}" id="latitude">
            <input type="hidden" name="longitude" value="{$item.longitude}" id="longitude">
            <div id="map" style="height: 350px;"></div>
            <p class="error text-red-500 text-xs italic mt-2"></p>
        </div>*}

        <div id="info-result" class="border px-4 py-3 hidden my-4"
             role="alert">
        </div>
    </div>

    <div class="flex justify-end mt-2">
        <input id="save"
               class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 focus:outline-none"
               type="button"
               name="publish"
               value="{$item.buttonTitle}"
               title="{$item.buttonTitle}"
               tabindex="16"
        />
    </div>
</form>
