<div id="delete-modal-{$id}" class="modal" aria-hidden="true">
    <div class="overlay fixed w-full h-full top-0 left-0 flex items-center justify-center"
         tabindex="-1" data-micromodal-close>
        <div class="w-full md:w-1/2 lg:w-1/3 bg-white p-4">
            <h4 class="text-xl text-centered">Удаление обменного пункта!</h4>
            <button class="delete"></button>
            <div class="mb-4">
                <p class="mb-2">Вы подтверждаете удаление следующего обменного пункта?</p>
                <p class="mb-2 text-lg"><b>"{$name}"</b></p>
                <p class="text-red-500">Все данные будут полностью удалены.</p>
            </div>

            <form method="post" class="delete-ep">
                <input type="hidden"
                       name="id"
                       value="{$id}">
                <input type="hidden"
                       name="delete"
                       value="1">
                <input type="hidden"
                       name="action"
                       value="exchange/delete">

                <div class="flex justify-center">
                    <button type="submit"
                            class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 mx-2"
                            data-micromodal-close>Удалить
                    </button>
                    <button type="button"
                            class="cancel bg-pelorous-500 hover:bg-pelorous-700 text-white font-bold py-2 px-4 mx-2"
                            data-micromodal-close>Отмена
                    </button>
                </div>
            </form>
        </div>
    </div>

</div>
<div class="w-full md:w-1/2 lg:w-1/3 px-2 pb-2">
    <div class="flex flex-col border border-gray-600 p-2">
        <div class="text-center">
            <a class="block text-2xl border-b border-gray-500 mb-2 pb-2 underline"
               href="{$_modx->makeUrl(3077)}?pid={$id}">
                {$name}
            </a>
            <p class="mb-2"><b>{$_pls['city_name']}</b></p>
            <div class="mb-2">
                <p class="mb-1">Последнее изменение:</p>
                <p><b>{$_modx->runSnippet('dateformat_exchange', [ 'str' => $_pls['date_update'] ])}</b></p>
            </div>
            {if $_pls['published'] < 1}
                <p class="bg-orange-100 border border-orange-400 text-orange-700 px-2 py-2 my-2">На
                    модерации</p>
            {/if}
            <p class="mb-2">{$_pls['info']}</p>
            {if $_pls['gross']}
                <p class="text-red-600">
                    <b>Оптовый</b>
                </p>
            {else}
                <p class="text-pelorous-600">
                    <b>Розничный</b>
                </p>
            {/if}
        </div>


        <div class="overflow-x-auto my-2">
            <table class="table">
                <thead>
                <tr>
                    <th class="text-center">Валюта</th>
                    <th class="text-center">Покупка</th>
                    <th class="text-center">Продажа</th>
                </tr>
                </thead>
                <tbody>
                {set $currencies = [
                ['image' => '/assets/images/flags/flag_usa_1.png', 'title' => 'Американский Доллар', 'key' => 'USD'],
                ['image' => '/assets/images/flags/European Union_1.png', 'title' => 'Евро', 'key' => 'EUR'],
                ['image' => '/assets/images/flags/flag_russia_1.png', 'title' => 'Российский Рубль', 'key' => 'RUB'],
                ['image' => '/assets/images/flags/flag_china_1.png', 'title' => 'Китайский Юань', 'key' => 'CNY'],
                ['image' => '/assets/images/flags/flag_gb_1.png', 'title' => 'Британский фунт', 'key' => 'GBP'],
                ['image' => '/assets/images/flags/gold.png', 'title' => 'Золото', 'key' => 'XAU'],
                ]}
                {foreach $currencies as $currency}
                    <tr>
                        <td class="text-center">
                            <img width="24"
                                 height="24"
                                 class="inline"
                                 alt="{$currency.title}"
                                 title="{$currency.title}"
                                 src="{$currency.image}">
                        </td>
                        <td class="text-center">{$_pls["buy{$currency.key}"]}</td>
                        <td class="text-center">{$_pls["sell{$currency.key}"]}</td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>

        <div class="flex justify-around mb-2">
            <a href="{$_modx->makeUrl(3077)}?pid={$id}"
               class=" bg-pelorous-500 hover:bg-pelorous-700 text-white font-bold py-2 px-4">
                &nbsp;Изменить&nbsp;</a>
            <button type="button"
                    class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4"
                    data-micromodal-trigger="delete-modal-{$id}"
            >
                Удалить
            </button>
        </div>
    </div>
</div>
