<div class="navbar bg-black-light flex justify-between border-t-4 border-pelorous-500 h-10 lg:h-auto flex-row">
    <div class="flex lg:hidden">
        <button class="nav-toggle appearance-none mx-2" type="button">
            <svg class="fill-current text-white w-6 h-6" xmlns="http://www.w3.org/2000/svg" viewbox="0 0 20 20">
                <path d="M16.4 9H3.6c-.552 0-.6.447-.6 1 0 .553.048 1 .6 1h12.8c.552 0 .6-.447.6-1 0-.553-.048-1-.6-1zm0 4H3.6c-.552 0-.6.447-.6 1 0 .553.048 1 .6 1h12.8c.552 0 .6-.447.6-1 0-.553-.048-1-.6-1zM3.6 7h12.8c.552 0 .6-.447.6-1 0-.553-.048-1-.6-1H3.6c-.552 0-.6.447-.6 1 0 .553.048 1 .6 1z"></path>
            </svg>
        </button>
    </div>
    <nav class="hidden lg:flex nav z-10 absolute mt-8 lg:relative lg:mt-0">
        <ul class="list-none p-0 lg:flex block font-semibold uppercase bg-black-light">
            <li class="block">
                <a class="block px-4 py-3 text-white hover:bg-pelorous-500" href="/">
                    Главная
                </a>
            </li>
            <li class="block">
                <a class="block px-4 py-3 text-white hover:bg-pelorous-500" href="{$_modx->makeUrl(3218)}"
                   title="Курсы валют">
                    Курсы валют
                </a>
            </li>
            <li class="block">
                <a class="block px-4 py-3 text-white hover:bg-pelorous-500" href="{$_modx->makeUrl(33)}">
                    Изготовление Карт-схем
                </a>
            </li>
            <li class="block">
                <a class="block px-4 py-3 text-white hover:bg-pelorous-500" href="{$_modx->makeUrl(4006)}">
                    Расписание транспорта
                </a>
            </li>
            <li class="block">
                <a class="block px-4 py-3 text-white hover:bg-pelorous-500" href="{$_modx->makeUrl(4490)}">
                    Личные кабинеты
                </a>
            </li>
        </ul>
    </nav>
    <div class="flex flex-col justify-center flex-grow">
        <div class="search flex justify-end mr-4">
            <form class="flex justify-end" action="">
                <input id="searchInputHead" class="px-2 py-1" placeholder="Поиск..." type="search"/>
                <button id="searchButtonHead" class="text-white mx-2 flex self-center" title="Поиск">
                    <i class="fa fa-search text-xl"></i>
                </button>
                <a class="text-white flex self-center" href="{$_modx->makeUrl(4490)}" title="Личные кабинеты">
                    <i class="fa fa-user text-xl"></i>
                </a>
            </form>
        </div>
    </div>
</div>