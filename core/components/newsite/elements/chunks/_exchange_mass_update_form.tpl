{if $_pls['showForm']}
    <div class="overflow-x-auto container mx-auto px-2 mb-4">
        <div class="border border-gray-600 p-2">
            <form method="post" id="massUpdateForm">
                <h4 class="text-center text-xl mt-2 mb-4">Массовое обновление курсов</h4>
                <div id="massUpdateStatus"
                     class="border px-4 py-3 rounded relative hidden"
                     role="alert">
                </div>
                <div class="flex flex-wrap lg:flex-no-wrap mb-4 px-2">
                    <div class="flex flex-row w-full mb-2 lg:mb-0 lg:w-1/4">
                        <label for="type-retail"
                               class="{if $_pls['retail']}text-black{else}text-gray-600 cursor-not-allowed{/if} text-black text-xs font-bold mb-1">
                            <input id="type-retail"
                                   class="{if !$_pls['retail']}cursor-not-allowed{/if} mr-2 leading-tight" type="radio"
                                   name="type" {if $_pls['retail']} checked {else} disabled{/if} value="retail">
                            <span class="text-sm">Розница</span>
                        </label>

                        <label for="type-gross"
                               class="{if $_pls['gross']}text-black{else}text-gray-600 cursor-not-allowed{/if} text-xs font-bold mb-1 ml-6">
                            <input id="type-gross"
                                   class="{if !$_pls['gross']}cursor-not-allowed{/if} mr-2 leading-tight" type="radio"
                                   name="type" {if $_pls['gross'] && !$_pls['retail']} checked{/if} {if !$_pls['gross']}disabled{/if}
                                   value="gross">
                            <span class="text-sm">Опт</span>
                        </label>
                    </div>
                    <div class="flex w-full lg:w-3/4 mb-2 lg:mb-0 flex-wrap">
                        {if $_pls['retail']}
                            <div class="retailPoints">
                                {foreach $_pls['retailPoints'] as $point}
                                    <label class="text-black font-bold mr-2">
                                        <input class="mr-2 leading-tight" type="checkbox" value="{$point.id}"
                                               name="points[]">
                                        <span class="text-sm">{$point.name}</span>
                                    </label>
                                {/foreach}
                            </div>
                        {/if}
                        {if $_pls['gross']}
                            <div class="grossPoints" style="{if $_pls['retail']}display: none;{/if}">
                                {foreach $_pls['grossPoints'] as $point}
                                    <label class="text-black font-bold mr-2">
                                        <input class="mr-2 leading-tight" type="checkbox" value="{$point.id}"
                                               name="points[]">
                                        <span class="text-sm">{$point.name}</span>
                                    </label>
                                {/foreach}
                            </div>
                        {/if}
                    </div>
                    <div class="flex w-full lg:w-1/4 mb-2 lg:mb-0 justify-end">
                        <button type="button" class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4"
                                id="massUpdateButton">Сохранить
                        </button>
                    </div>
                </div>

                <input type="hidden" value="exchange/massUpdate" name="action">
                {$_pls['buyRows']}
                {$_pls['sellRows']}
            </form>
        </div>
    </div>
{/if}
