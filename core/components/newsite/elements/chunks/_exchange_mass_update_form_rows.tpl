<div class="flex flex-wrap">
    {foreach $currencies as $key => $currency}
        <div class="w-1/2 md:w-1/2 lg:w-1/6 px-2 mb-4">
            <div class="flex flex-col">
                <label class="block uppercase tracking-wide text-black text-xs font-bold mb-1"
                       for="{$key}">{$currency.label}</label>
                <input type="text"
                       id="{$key}"
                       class="appearance-none block w-full bg-pelorous-100 text-gray-900 border border-gray-600 py-2 px-2 leading-tight focus:outline-none focus:bg-white"
                       name="{$key}"
                       maxlength="10"
                       value=""
                       tabindex="{$currency.tabindex}"
                />
            </div>
        </div>
    {/foreach}
</div>