{extends 'template:_page'}
{block 'main'}
    {set $items = [
    ["title" => 'Драмтеатр', 'icon' => 'fas fa-theater-masks', 'id' => 109],
    ["title" => 'Кинотеатр ЭХО', 'icon' => 'fas fa-film', 'id' => 794],
    ]}
    <div class="w-full flex flex-wrap px-2 mt-2 mb-4 justify-center">
        {foreach $items as $item}
            <div class="w-1/2 md:w-1/4 px-4">
                <a class="flex flex-col justify-around h-64 py-12 px-1 m-2 text-center cursor-pointer bg-pelorous-500 text-white hover:bg-orange-500"
                   href="{$_modx->makeUrl($item.id)}">
                    <h4 class="py-2 font-semibold text-xl">{$item.title}</h4>
                    <div class="text-5xl">
                        <i class="fas {$item.icon}"></i>
                    </div>
                </a>
            </div>
        {/foreach}
    </div>
{/block}