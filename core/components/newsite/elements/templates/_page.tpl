{extends 'template:_base'}
{block 'content'}
    <div class="w-full lg:w-4/5 xl:w-5/6">
        <div class="container w-full mx-auto">
            <h4 class="text-xl text-center mb-6 border-b pb-6 mx-3 border-gray-600">{$_modx->resource.pagetitle}</h4>
            <div class="block mx-auto">
                {block 'main'}
                    {$_modx->resource.content}
                {/block}
            </div>
        </div>
    </div>
    {include 'file:chunks/_banners_right.tpl'}
{/block}
{block 'scripts'}

{/block}