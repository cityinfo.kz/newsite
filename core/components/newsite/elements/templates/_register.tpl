{extends 'template:_base'}
{block 'content'}
    <div class="w-full block mx-auto py-4">
        <h3 class="text-xl text-center mb-4">Регистрация личного кабинета</h3>
        {if $modx->user->isAuthenticated($modx->context->key)}
            <div class="bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4 max-w-lg mx-auto" role="alert">
                <p class="font-bold mb-2">Упс!</p>
                <p>Вы уже зарегестрированны, переходите в личный кабинет по <a href="{$_modx->makeUrl(4490)}">ссылке</a>
                </p>
            </div>
        {else}
            {$_modx->runSnippet('!Register', [
            'activationResourceId'=> 3089,
            'activationEmailTpl' => '_register_email',
            'activationEmailSubject' => 'Вы зарегистрированы на сайте cityinfo.kz',
            'placeholderPrefix' => 'reg.',
            'successMsg' => 'Спасибо за регистрацию!',
            'usernameField' => 'email',
            'customValidators' => 'valueIn',
            'validate' => 'name:required:minLength=^3^,password:required:minLength=^6^,password_confirm:password_confirm=^password^,email:required:email',
            'usergroups' => 'Ukg',
            ])}
            {if $_modx->getPlaceholder('error.message')}
                <div class="bg-blue-500 border-l-4 border-blue-700 text-white p-4 max-w-lg mx-auto" role="alert">
                    <p class="font-bold mb-2">{$_modx->getPlaceholder('error.message')}</p>
                    <p class="mb-2">На вашу электронную почту отправлено письмо со ссылкой на активацию аккаунта. </p>
                    <p>Пройдите по этой ссылке, чтобы завершить регистрацию.</p>
                </div>
            {else}
                {$_modx->getChunk('!_register_form')}
            {/if}
        {/if}
    </div>
{/block}