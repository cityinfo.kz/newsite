{extends 'template:_base'}
{block 'content'}
    <div class="mx-auto flex mb-3">
        <a href="{$_modx->makeUrl(99)}"
           class="m-1 hover:bg-gray-200 hover:border-gray-600 py-2 px-4 border border-gray-500">
            Автовокзал на Мызы
        </a>
        <a href="{$_modx->makeUrl(571)}"
           class="m-1 hover:bg-gray-200 hover:border-gray-600 py-2 px-4 border border-gray-500">
            Автовокзал на Абая
        </a>
        <a href="{$_modx->makeUrl(105)}"
           class="m-1 hover:bg-gray-200 hover:border-gray-600 py-2 px-4 border border-gray-500">
            Расписание движения самолётов
        </a>
        <a href="{$_modx->makeUrl(115)}"
           class="m-1 hover:bg-gray-200 hover:border-gray-600 py-2 px-4 border border-gray-500">
            Расписание движения поездов
        </a>
    </div>
    <div class="block w-full">
        {$_modx->resource.content}
    </div>
{/block}
{block 'scripts'}

{/block}