{extends 'template:_exchange_base'}
{block 'main'}
    {var $exchAdd = $_modx->makeUrl(3077)}
    {$_modx->runSnippet('!_exchange_mass_update', [])}

    {if $modx->user->isAuthenticated($modx->context->key)}

        {$_modx->runSnippet('!pdoPage', [
        'element' => '!_exchange_get_points',
        'limit' => 0,
        ])}

        {if $_modx->getPlaceholder('total') == 0}
            <p>Вы не добавили ни одного обменного пункта, перейдите на <a
                        href="{$exchAdd}">страницу</a> добавления обменных пунктов.</p>
        {/if}
        {$_modx->getPlaceholder('page.nav')}
    {else}
        {$_modx->sendRedirect($exchAdd)}
        <p class="lead">Вы не авторизованы.</p>
    {/if}
{/block}