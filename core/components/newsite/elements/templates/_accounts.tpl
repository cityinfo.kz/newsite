{extends 'template:_base'}
{block 'content'}
    {set $accounts = [
    ["title" => 'Регистрация личного кабинета', 'icon' => 'fas fa-user-plus', 'id' => 3080],
    ["title" => 'Управление курсами обменного пункта', 'icon' => 'fas fa-hand-holding-usd', 'id' => 3093],
    ["title" => 'Управление прайс-листами', 'icon' => 'fas fa-list-ol', 'id' => 4016],
    ]}
    <div class="w-full flex flex-wrap px-2 mt-2 mb-4">
        {foreach $accounts as $account}
            <div class="w-full md:w-1/3">
                <a class="flex flex-col justify-around h-64 py-12 px-1 m-2  text-center cursor-pointer bg-pelorous-500  text-white hover:bg-orange-500" href="{$_modx->makeUrl($account.id)}">
                    <h4 class="py-2 font-semibold text-xl">{$account.title}</h4>
                    <div class="text-5xl">
                        <i class="fas {$account.icon}"></i>
                    </div>
                </a>
            </div>
        {/foreach}
    </div>
{/block}