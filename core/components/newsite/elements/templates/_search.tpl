{extends 'template:_base'}
{block 'content'}
    {set $searchCategories = $_modx->runSnippet('!_search_categories', [])}
    <div class="flex flex-col mx-auto p-4 leading-loose w-full lg:w-5/6 relative">
        <div class="flex flex-wrap justify-center mb-4">
            {foreach $searchCategories as $category}
                <a class="search-category plate plate--search plate--search-page" title="{$category.name}"
                   data-id="{$category.id}">
                    <div class="plate__icon">
                        <i class="{$category.icon}"></i>
                    </div>
                    <h4 class="plate__title">{$category.name}</h4>
                </a>
            {/foreach}
        </div>

        <div class="search max-w-xl leading-tight w-full flex items-center mb-4 mx-auto">
            <input
                    id="searchInput"
                    class="transition focus:outline-0 border border-gray-600 bg-white focus:border-blue placeholder-gray-800 py-2 px-4 mr-2 block w-full appearance-none"
                    type="text" placeholder="Поиск" value="">
            <button
                    id="searchButton"
                    class="flex-shrink-0 bg-pelorous-500 hover:bg-orange-500 border-pelorous-500 hover:border-pelorous-500 text-sm text-white py-2 px-4">
                Найти
            </button>
        </div>
        <div class="search-results flex flex-wrap justify-between">

        </div>
        <div id="spinner" class="spinner-container absolute w-full h-full" style="max-height: 100%; display: none;">
            <div class="spinner spinner--red">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
            </div>
        </div>
    </div>
    {include 'file:chunks/_banners_right.tpl'}
{/block}
