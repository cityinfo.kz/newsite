{extends 'template:_base'}
{block 'content'}
    <div class="container mx-auto">
        {if $modx->user->isAuthenticated($modx->context->key) && $.post.action !== 'logout'}
            <div class="flex flex-col-reverse lg:flex-row mt-4">
                <div class="flex flex-col mx-auto w-full lg:w-4/5">
                    <h4 class="text-xl text-center mb-6 border-b pb-6 mx-3 border-gray-600">{$_modx->resource.pagetitle}</h4>
                    {block 'main'}

                    {/block}
                </div>
                <div class="flex flex-col mx-auto w-full lg:w-1/5 pl-4 border-l border-gray-600">
                    {$_modx->runSnippet('!pdoUsers', [
                    'users' => $_modx->user.id,
                    'tpl' => '_user_menu'
                    ])}
                </div>
            </div>
        {else}
            <div class="mx-auto max-w-lg my-4">
                {$_modx->runSnippet('!Login', [
                'loginTpl' => '_login_form',
                'actionKey' => 'action',
                'loginKey' => 'login',
                'logoutResourceId' => $_modx->resource.id,
                'loginResourceId' => 3093,
                ])}
            </div>
        {/if}
    </div>
{/block}