{extends 'template:_page'}
{block 'main'}
    {set $context = 'mgr'}
    {if $modx->user->isAuthenticated($context)}
        <div class="container mx-auto px-2">
            <div class="mb-4">
                <p class="mb-2">
                    Последовательность действий для редактирования курсов валют обменных пунктов:
                </p>
                <div class="ml-4 mb-2">
                    <ol class="list-decimal">
                        <li>Выберите город</li>
                        <li>Выберите обменный пункт</li>
                        <li>Внесите изменения курсов валют</li>
                        <li>Проверьте все введенные данные</li>
                        <li>Нажмите на красную кнопку «Сохранить»</li>
                        <li>Наслаждайтесь!</li>
                    </ol>
                </div>
                <p>P.S: Не пугайтесь нулей в значениях курсов валют, при выводе они преобразуются в «-»</p>
            </div>
            <div class="mb-4">
                <div class="flex flex-wrap -mx-3 mb-2">
                    <div class="w-full md:w-1/4 px-3 mb-6 md:mb-0">
                        <div class="relative">
                            <select id="city"
                                    class="block appearance-none w-full bg-pelorous-100 text-gray-900 border border-gray-600 focus:outline-none focus:bg-white py-2 px-2 pr-8 rounded leading-tight">
                                <option value="-1">Выберите город</option>
                            </select>
                            <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                     viewBox="0 0 20 20">
                                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="w-full md:w-2/4 px-3 mb-6 md:mb-0">
                        <div class="relative">
                            <select name="exchange" id="exchange" disabled
                                    class="block appearance-none w-full bg-pelorous-100 text-gray-900 border border-gray-600 focus:outline-none focus:bg-white py-2 px-2 pr-8 rounded leading-tight">
                                <option value="-1">Выберите обменный пункт</option>
                            </select>
                            <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                     viewBox="0 0 20 20">
                                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div class="flex items-center justify-between w-full md:w-1/4 px-3 mb-6 md:mb-0">
                        <button type="button" id="saveButton"
                                class="shadow bg-red-500 hover:bg-red-400 focus:outline-none text-white font-bold py-2 px-4 rounded"
                                disabled="">
                            Сохранить
                        </button>
                        <button type="button"
                                class="shadow bg-yellow-500 hover:bg-yellow-400 focus:outline-none text-black font-bold py-2 px-4 rounded"
                                id="showModalButton"
                                disabled=""
                                data-micromodal-trigger="delete-modal">
                            Удалить
                        </button>
                    </div>

                </div>
            </div>
            <div id="message" class="border px-4 py-3 rounded relative" role="alert"
                 style="display: none">

            </div>
            <form class="w-full mx-auto max-w-2xl" id="exchangeData" style="display: none">
                <input type="hidden" name="id" value="">

                {set $checkboxes = [
                ['name' => 'published', 'value' => 0, 'label' => 'Опубликован?', 'text' => 'Да'],
                ['name' => 'gross', 'value' => 1, 'label' => 'Оптовый?', 'text' => 'Да'],
                ['name' => 'day_and_night', 'value' => 1, 'label' => 'Круглосуточный?', 'text' => 'Да'],
                ]}

                {foreach $checkboxes as $checkbox}
                    <div class="md:flex md:items-center mb-6">
                        <div class="md:w-1/3">
                            <label class="block text-black font-bold md:text-right mb-1 md:mb-0 pr-4">
                                {$checkbox.label}
                            </label>
                        </div>
                        <label class="md:w-2/3 block text-black font-bold">
                            <input class="mr-2 leading-tight" type="checkbox" name="{$checkbox.name}"
                                   value="{$checkbox.value}">
                            <span class="text-sm">{$checkbox.text}</span>
                        </label>
                    </div>
                {/foreach}
                {set $fields = [
                ['name' => 'buyUSD', 'title' => 'Покупка USD',],
                ['name' => 'sellUSD', 'title' => 'Продажа USD',],

                ['name' => 'buyEUR', 'title' => 'Покупка EUR',],
                ['name' => 'sellEUR', 'title' => 'Продажа EUR',],

                ['name' => 'buyRUB', 'title' => 'Покупка RUB',],
                ['name' => 'sellRUB', 'title' => 'Продажа RUB',],

                ['name' => 'buyCNY', 'title' => 'Покупка CNY',],
                ['name' => 'sellCNY', 'title' => 'Продажа CNY',],

                ['name' => 'buyGBP', 'title' => 'Покупка GBP',],
                ['name' => 'sellGBP', 'title' => 'Продажа GBP',],

                ['name' => 'buyXAU', 'title' => 'Покупка XAU',],
                ['name' => 'sellXAU', 'title' => 'Продажа XAU',],

                ['name' => 'name', 'title' => 'Название',],
                ['name' => 'info', 'title' => 'Адрес',],
                ['name' => 'phones', 'title' => 'Телефоны',],
                ['name' => 'company_id', 'title' => 'ID карточки предприятия',],
                ['name' => 'sorting', 'title' => 'Сортировка',],

                ]}

                {foreach $fields as $field}
                    <div class="md:flex md:items-center my-2">
                        <div class="md:w-1/3 flex justify-end">
                            <label class="block text-black text-sm font-bold pr-4"
                                   for="{$field.name}">
                                {$field.title}
                            </label>
                        </div>
                        <div class="md:w-2/3">
                            <input id="{$field.name}"
                                   class="appearance-none block w-full bg-pelorous-100 text-gray-900 border border-gray-600 focus:outline-none focus:bg-white py-2 px-2 leading-tight "
                                   type="text" placeholder="{$field.title}" name="{$field.name}" value="">
                        </div>
                    </div>
                {/foreach}
                <div class="md:flex md:items-center my-2">
                    <div class="md:w-1/3 flex justify-end">
                        <label class="block text-black text-sm font-bold pr-4"
                               for="atms">
                            Привязка к банкоматам
                        </label>
                    </div>
                    <div class="md:w-2/3">
                        <div class="relative">
                            <select name="atms" id="atms"
                                    class="block appearance-none w-full bg-pelorous-100 text-gray-900 border border-gray-600 focus:outline-none focus:bg-white py-2 px-2 pr-8 rounded leading-tight">
                                <option value="">Нет</option>
                                {set $atms = [
                                "halyk"=> 'Народный банк',
                                "kaspi"=> 'Каспи банк',
                                "alfa"=> 'Альфа-банк',
                                "bta"=> 'БТА Банк',
                                "atf"=> 'АТФ Банк',
                                "bcc"=> 'Банк ЦентрКредит',
                                "nurbank"=> 'Нурбанк',
                                "tsb"=> 'Цесна банк',
                                "eubank"=> 'Eurasian Bank',
                                "sberbank"=>   'Сбербанк',
                                ]}
                                {foreach $atms as $name => $title}
                                    <option value="{$name}">{$title}</option>
                                {/foreach}
                            </select>
                            <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                     viewBox="0 0 20 20">
                                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="latitude" value="" id="latitude">
                <input type="hidden" name="longitude" value="" id="longitude">
                <div id="manager-map" style="height: 350px;"></div>
            </form>
            <div id="delete-modal" class="modal" aria-hidden="true">
                <div class="overlay fixed w-full h-full top-0 left-0 flex items-center justify-center"
                     tabindex="-1" data-micromodal-close>
                    <div class="w-full md:w-1/2 lg:w-1/3 bg-white p-4">
                        <h4 class="text-xl text-centered">Удаление обменного пункта!</h4>
                        <button class="delete"></button>
                        <div class="mb-4">
                            <p class="mb-2">Вы подтверждаете удаление следующего обменного пункта?</p>
                            <p class="text-red-500">Все данные будут полностью удалены.</p>
                        </div>
                        <div class="flex justify-center">
                            <button id="deleteButton" type="button"
                                    class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 mx-2"
                                    data-micromodal-close>Удалить
                            </button>
                            <button type="button"
                                    class="cancel bg-pelorous-500 hover:bg-pelorous-700 text-white font-bold py-2 px-4 mx-2"
                                    data-micromodal-close>Отмена
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {else}
        <div class="mx-auto max-w-lg my-4">
            {$_modx->runSnippet('!Login', [
            'loginTpl' => '_login_form',
            'actionKey' => 'action',
            'loginKey' => 'login',
            'logoutResourceId' => $_modx->resource.id,
            'loginResourceId' => $_modx->resource.id,
            'contexts' => $context,
            'loginContext' => $context
            ])}
        </div>
    {/if}
{/block}