<!DOCTYPE html>
<html lang="en">
{include 'file:chunks/_head.tpl'}
<body class="flex flex-col min-h-screen bg-gray-500 font-sans text-sm leading-tight">
<header class="container bg-white mx-auto">
    {include 'file:chunks/_headline.tpl'}
    {include 'file:chunks/_showbar.tpl'}
    {include 'file:chunks/_navbar.tpl'}
</header>
<section class="flex flex-wrap container mx-auto py-3 bg-white content px-4 flex-grow">
    {block 'content'}

    {/block}
</section>
{include 'file:chunks/_footer.tpl'}
{block 'scripts'}

{/block}
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
  (function (d, w, c) {
    (w[c] = w[c] || []).push(function () {
      try {
        w.yaCounter35354850 = new Ya.Metrika({
          id: 35354850,
          clickmap: true,
          trackLinks: true,
          accurateTrackBounce: true,
          webvisor: true,
          trackHash: true,
          ut: 'noindex'
        })
      } catch (e) { }
    })

    var n = d.getElementsByTagName('script')[0],
      s = d.createElement('script'),
      f = function () { n.parentNode.insertBefore(s, n) }
    s.type = 'text/javascript'
    s.async = true
    s.src = 'https://mc.yandex.ru/metrika/watch.js'

    if (w.opera == '[object Opera]') {
      d.addEventListener('DOMContentLoaded', f, false)
    } else { f() }
  })(document, window, 'yandex_metrika_callbacks')
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/35354850?ut=noindex" style="position:absolute; left:-9999px;" alt=""/>
    </div>
</noscript>
<!-- /Yandex.Metrika counter -->
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107573642-1"></script>
<script>
  window.dataLayer = window.dataLayer || []
  function gtag () {
    dataLayer.push(arguments)
  }
  gtag('js', new Date())
  gtag('config', 'UA-107573642-1')
</script>
</body>
</html>