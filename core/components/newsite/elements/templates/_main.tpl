{extends 'template:_base'}
{block 'content'}
    <div class="w-full p-4 sm:w-1/2">
        <div class="my-slider">
            <div class="relative">
                <a>
                    <img alt="" class="object-contain"
                         src="/assets/components/newsite/images/main/1.jpg"/>
                    <div class="slide-text absolute text-white">
                        <h3 class="text-xl">
              <span>
                Информационный Центр 13-55
              </span>
                        </h3>
                    </div>
                </a>
            </div>
            <div class="relative">
                <a>
                    <img alt="" class="object-contain"
                         src="/assets/components/newsite/images/main/2.jpg"/>
                    <div class="slide-text absolute text-white">
                        <h3 class="text-xl">
                            <span>Звоните спрашивайте! 13-55 и 8-777-646-13-55</span>
                        </h3>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="w-full flex flex-col p-4  sm:w-1/2">
        <a class="block bg-black w-full p-12 text-white text-center text-3xl main-courses"
           href="{$_modx->makeUrl(3218)}">
            <span class="p-2">Курсы валют</span>
        </a>
        <a class="block bg-red w-full p-12 text-white text-center mt-6 text-3xl main-atameken" href="{$_modx->makeUrl(4493)}">
            <span>Новости «Атамекен»</span>
        </a>
    </div>
    <div class="w-full flex flex-wrap px-2 mt-2 mb-4">
        <div class="w-1/2 sm:w-1/3 md:w-1/6">
            <a class="plate plate--site border-r border-gray-500" href="{$_modx->makeUrl(33)}">
                <div class="plate__icon text-pelorous-500">
                    <i class="fas fa-map-marked-alt"></i>
                </div>
                <h4 class="plate__title">Карта</h4>
                <p class="plate__annotation">
                    Изготовление, печать
                </p>
            </a>
        </div>

        <div class="w-1/2 sm:w-1/3 md:w-1/6">
            <a class="plate plate--site border-r border-gray-500" href="{$_modx->makeUrl(4001)}">
                <div class="plate__icon text-pelorous-500">
                    <i class="fas fa-gas-pump"></i>
                </div>
                <h4 class="plate__title">Бензин</h4>
                <p class="plate__annotation">Цены на ГСМ</p>
            </a>
        </div>

        <div class="w-1/2 sm:w-1/3 md:w-1/6">
            <a class="plate plate--site border-r border-gray-500" href="{$_modx->makeUrl(4006)}">
                <div class="plate__icon text-pelorous-500">
                    <i class="fas fa-bus-alt"></i>
                </div>
                <h4 class="plate__title">Расписание транспорта</h4>
                <p class="plate__annotation">Аэропорт, вокзалы</p>
            </a>
        </div>

        <div class="w-1/2 sm:w-1/3 md:w-1/6">
            <a class="plate plate--site border-r border-gray-500" href="{$_modx->makeUrl(3948)}">
                <div class="plate__icon text-pelorous-500">
                    <i class="far fa-money-bill-alt"></i>
                </div>
                <h4 class="plate__title">Банкоматы</h4>
                <p class="plate__annotation">Отображение на карте</p>
            </a>
        </div>

        <div class="w-1/2 sm:w-1/3 md:w-1/6">
            <a class="plate plate--site border-r border-gray-500" href="{$_modx->makeUrl(4494)}">
                <div class="plate__icon text-pelorous-500">
                    <i class="far fa-calendar-alt"></i>
                </div>
                <h4 class="plate__title">Афиша</h4>
                <p class="plate__annotation">Театры и кинотеатры</p>
            </a>
        </div>

        <div class="w-1/2 sm:w-1/3 md:w-1/6">
            <a class="plate plate--site" href="{$_modx->makeUrl(3928)}">
                <div class="plate__icon text-pelorous-500">
                    <i class="fas fa-phone-square"></i>
                </div>
                <h4 class="plate__title">Телефонные коды</h4>
                <p class="plate__annotation">Городов Казахстана</p>
            </a>
        </div>

    </div>
    {set $categories = [
    ["id" => 944, "title" => "Медицина", "icon"=> "fas fa-heartbeat" ],
    ["id" => 955, "title"=> "Услуги", "icon"=> "fab fa-aws" ],
    ["id" => 949, "title"=> "Рестораны и кафе", "icon"=> "fas fa-wine-glass-alt" ],
    ["id" => 954, "title"=> "Туризм", "icon"=> "fas fa-plane" ],
    ["id" => 937, "title"=> "Бизнес", "icon"=> "fas fa-business-time" ],
    ["id" => '', "title"=> "Ещё", "icon"=> "fas fa-ellipsis-h" ]
    ]}
    <div class="flex flex-wrap w-full px-2">
        {foreach $categories as $category}
            <div class="w-1/2 sm:w-1/3 md:w-1/6">
                <a class="plate plate--search plate--main-page"
                   href="{$_modx->makeUrl(4489, '', ['catID' => $category.id])}">
                    <div class="plate__icon">
                        <i class="{$category.icon}"></i>
                    </div>
                    <h4 class="plate__title">{$category.title}</h4>
                </a>
            </div>
        {/foreach}
    </div>
{/block}
{block 'scripts'}
    <script crossorigin="anonymous" integrity="sha256-pau+EjagELvmW7XoDOgz4DCKHep0GuC+kw6U8GQKo94="
            src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.1/min/tiny-slider.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            tns({
                container: '.my-slider',
                slideBy: 'page',
                autoplay: true,
                mouseDrag: true,
                swipeAngle: false,
                controls: false,
                nav: false,
                autoplayButtonOutput: false,
                autoplayTimeout: 10000
            })
        })
    </script>
{/block}