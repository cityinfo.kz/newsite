{extends 'template:_base'}
{block 'content'}
    {set $item = $_modx->runSnippet('!_company_card', [])}
    <div class="w-full flex flex-wrap mx-auto company-card leading-normal py-4 px-8">
        <div class="w-full lg:w-1/2">
            <div class="flex items-center justify-center p-2 lg:mr-2 mb-2 lg:mb-0 border border-gray-600 rounded">
                {if $item.logo}
                    <div class="flex items-center company-card-logo">
                        <img src="{$item.logo}" class="object-fill" alt="{$item.title}">
                    </div>
                {/if}
                <div class="leading-normal flex items-center h-16">
                    <div class="ml-2">
                        <h3 class="text-lg">{$item.title}</h3>
                    </div>
                </div>
            </div>
        </div>
        {set $photos = $item.photos}
        {set $placeholder = ""}
        <div class="w-full lg:w-1/2">
            <div class="company-card-photo-wrapper lg:ml-2"
                style="background: linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), white url(/assets/components/newsite/images/placeholders/company-card.jpg); background-size: cover !important;">
                <div id="show-photos"
                    class="company-card-photo-counter flex-col leading-none {if $photos}cursor-pointer{/if}">
                    {if $photos}
                        <a class="text-lg text-black">{count($photos)}</a>
                        <span class="text-xs">фото</span>
                    {else}
                        нет фото
                    {/if}
                </div>
            </div>
        </div>
        <div class="w-full my-4">
            <ul id="card-mode-toggle" class="list-none p-0 flex">
                <li>
                    <button class="company-card-mode-toggle active border py-2 px-3 rounded cursor-pointer"
                        data-mode="info">
                        Информация
                    </button>
                </li>
                {if count($item.prices) > 0}
                    <li>
                        <button class="company-card-mode-toggle ml-2 border py-2 px-3 rounded cursor-pointer "
                            data-mode="prices">
                            Прайс-лист
                        </button>
                    </li>
                {/if}
            </ul>
        </div>


        <div id="company-card-info" class="flex w-full">
            <div class="w-full lg:w-1/2">
                <div class="pr-4">
                    <table class="company-card-table">
                        <tbody>
                            <tr>
                                <th>Юридическое наименование:</th>
                                <td>{$item.title_legal}</td>
                            </tr>
                            {if $item.addresses}
                                <tr>
                                    <th>Адрес:</th>
                                    <td>
                                        {foreach $item.addresses as $address}
                                            <p>{$address}</p>
                                        {/foreach}
                                    </td>
                                </tr>
                            {/if}
                            {if $item.phones}
                                <tr>
                                    <th>Телефон:</th>
                                    <td>
                                        {foreach $item.phones as $phone}
                                            <p>{$phone}</p>
                                        {/foreach}
                                    </td>
                                </tr>
                            {/if}
                            {if $item.faxes}
                                <tr>
                                    <th>Факс:</th>
                                    <td>
                                        {foreach $item.faxes as $fax}
                                            <p>{$fax}</p>
                                        {/foreach}
                                    </td>
                                </tr>
                            {/if}
                            {if $item.mobile_phones}
                                <tr>
                                    <th>Сотовый:</th>
                                    <td>
                                        {foreach $item.mobile_phones as $phone}
                                            <p>{$phone}</p>
                                        {/foreach}
                                    </td>
                                </tr>
                            {/if}
                            {if $item.emails}
                                <tr>
                                    <th>Email:</th>
                                    <td>

                                        {foreach $item.emails as $email}
                                            <p><a href="mailto:{$email}">{$email}</a></p>
                                        {/foreach}
                                    </td>
                                </tr>
                            {/if}
                            {if $item.websites}
                                <tr>
                                    <th>Сайт:</th>
                                    <td>
                                        {foreach $item.websites as $website}
                                            <p><a href="{$website}" target="_blank" rel="noopener noreferrer">{$website}</a>
                                            </p>
                                        {/foreach}
                                    </td>
                                </tr>
                            {/if}
                            <tr>
                                <th>Режим работы:</th>
                                <td>
                                    {foreach $item.work_modes as $mode}
                                        <p>{$mode}</p>
                                    {/foreach}
                                </td>
                            </tr>
                            {if $item.landmarks}
                                <tr>
                                    <th>Ориентир:</th>
                                    <td>
                                        {foreach $item.landmarks as $landmark}
                                            <p>{$landmark}</p>
                                        {/foreach}
                                    </td>
                                </tr>
                            {/if}
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="w-full lg:w-1/2">
                <div class="pl-4 py-2">
                    <div id="map" class="company-card-map"></div>
                    <div>
                        <h4 class="text-lg">Категории:</h4>
                        {foreach $item.categories as $category}
                            {$category}{if !$category@last},{/if}
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
        {if $item.prices}
            <div id="company-card-prices" style="display: none;">
                <div id="firm-prices">
                    <div id="price_list_outer">
                        <h4 class="has-text-centered">Прайс-лист компании Cityinfo справочная служба</h4>

                        <table id="price_list">
                            <thead>
                                <tr role="row">
                                    <td>№</td>
                                    <td>Артикул</td>
                                    <td style="width:250px!important;">Наименование</td>
                                    <td>Цена</td>
                                    <td>Группа</td>
                                    <td>Категория</td>
                                    <td>Ед.Изм.</td>
                                    <td>Описание</td>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $item.prices as $row index=$index}
                                    <tr role="row" class="{if $index is odd}odd{else}even{/if}">
                                        <td style="border:1px solid  #ddd;" class="sorting_1">{$index + 1}</td>
                                        <td style="border:1px solid #ddd;">{$row.kod_tcg}</td>
                                        <td style="border:1px solid #ddd;">{$row.name_tcg}</td>

                                        <td style="border:1px solid  #ddd;">{$row.cena_tcg}</td>

                                        <td>{$row.groupsm}</td>
                                        <td>
                                            {$row.name_cat}
                                        </td>
                                        <td style="border:1px solid  #ddd;">{$row.edizm_tcg}</td>
                                        <td style="border:1px solid  #ddd;">{$row.desc_tcg}
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        {/if}
    </div>
{/block}

{block 'scripts'}
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
        integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
        crossorigin="" />
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"
        integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
        crossorigin=""></script>
    <script>
        const map = L
            .map('map')
            .setView([
                {$item.coordinates == '' ? [82.618964, 49.947274] : $item.coordinates }
            ], 16)
        {ignore}
        L
        .tileLayer('https://osm.apihit.com/?z={z}&x={x}&y={y}&s={s}', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
            maxZoom: 18
        })
        .addTo(map)
        {/ignore}
        {if $item.coordinates}
            const marker = L
                .marker([{$item.coordinates}])
                .addTo(map)
        {/if}
    </script>
    {if $item.photos}
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/lightgallery.js@1/dist/css/lightgallery.min.css" />
        <script src="https://cdn.jsdelivr.net/npm/lightgallery.js@1/dist/js/lightgallery.min.js"></script>
        <script>
            document.addEventListener('DOMContentLoaded', () => {
                        document
                            .getElementById('show-photos')
                            .addEventListener('click', function() {
                                    lightGallery(document.getElementById('show-photos'), {
                                            dynamic: true,
                                            dynamicEl: [
                                                {foreach $item.photos as $key => $photo}
                                                    {
                                                        "src": 'https://core.cityinfo.kz{$photo->src}'
                                                        }{if !$photo@last},{/if}
                                                    {/foreach}
                                                ]
                                            })

                                    });
                            });
        </script>
    {/if}
    {if $item.prices}


        {ignore}
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css" />
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
            $('#price_list').DataTable({
                    dom: 'Bfrtip',
                    "columnDefs": [
                        {"visible": false, "targets": 4}, {"visible": false, "targets": 5}
                    ],
                    buttons: [{
                        extend: 'excel',
                        text: 'Скачать Прайс-лист'
                    }, ],
                    language: {
                        url: "/assets/dt/Russian.json",
                    },
                    "drawCallback": function(settings) {
                        var api = this.api();
                        var rows = api.rows( {page:'current'} ).nodes();
                        var last = null;

                        api.column(4, {page:'current'} ).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before(
                                '<tr class="group"><td colspan="8">' + group + '</td></tr>'
                            );

                            last = group;
                        }
                    });
            }

            });
            });
        </script>
        {/ignore}
    {/if}
{/block}
