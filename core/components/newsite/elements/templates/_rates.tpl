{extends 'template:_base'}
{block 'content'}
    {set $activeClasses = 'bg-pelorous-500 text-gray-200'}
    {set $baseClasses = 'm-1 lg:m-0 py-2 px-4'}
    {set $inActiveClasses = 'border hover:bg-gray-200 hover:border-gray-600 border-gray-500'}
    <div class="flex flex-wrap lg:inline-flex p-4 mx-auto">
        <a class="m-1 lg:m-0 lg:mr-6 py-2 px-4 bg-pelorous-500 text-gray-200" href="{$_modx->makeUrl(3080)}">
            Регистрация обменных пунктов
        </a>
        <a href="{$_modx->makeUrl(3078)}"
            class="{$baseClasses} {if $_modx->resource.id === 3078}{$activeClasses}{else}{$inActiveClasses}{/if}">
            Астана
        </a>
        <a href="{$_modx->makeUrl(3079)}"
            class="{$baseClasses} {if $_modx->resource.id === 3079}{$activeClasses}{else}{$inActiveClasses}{/if}">
            Алматы
        </a>
        <a href="{$_modx->makeUrl(3218)}"
            class="{$baseClasses} {if $_modx->resource.id === 3218}{$activeClasses}{else}{$inActiveClasses}{/if}">
            Усть-Каменогорск
        </a>
        <a href="{$_modx->makeUrl(3680)}"
            class="{$baseClasses} {if $_modx->resource.id === 3680}{$activeClasses}{else}{$inActiveClasses}{/if}">
            Риддер
        </a>
    </div>
    <div class="flex flex-col mx-auto py-4 w-full">
        {$_modx->resource.content}
    </div>
    {*include 'file:chunks/_banners_right.tpl'*}
{/block}
{block 'scripts'}
{/block}
