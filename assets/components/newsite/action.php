<?php
define('MODX_API_MODE', true);
/** @noinspection PhpIncludeInspection */

require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/index.php';

if (empty($_REQUEST['action']) && ($modx->getAuthenticatedUser('web') == null || $modx->getAuthenticatedUser('mgr') == null)) {
    die(json_encode('Access denied'));
} else {
    $action = $_REQUEST['action'];
}

$modx->getService('error', 'error.modError');
$modx->getRequest();
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');
$modx->error->message = null;

$modx->switchContext('web');

define('MODX_ACTION_MODE', true);
$scriptProperties = [];

$newSite = $modx->getService('newSite');

$fqn = $modx->getOption('exchangeRates.class', null, 'newsite.exchangerates', true);
$path = $modx->getOption('exchangerates_class_path', null, MODX_CORE_PATH . 'components/newsite/model/', true);

/* @var exchangeRates $exchangeRates */
if (!$exchangeRates = $modx->getService('exchangeRates', 'exchangeRates', MODX_CORE_PATH . 'components/newsite/model/')){
    $modx->log(1, 'could not load exchangeRatesClass');
    return false;
}

$response = "";

if ($modx->getAuthenticatedUser('web')) {
    switch ($action) {
        case 'exchange/create':
        case 'exchange/update':
            $response = $exchangeRates->saveExchangePoint($_POST);
            break;
        case 'exchange/delete':
            $response = $exchangeRates->deleteExchangePoint($_POST);
            break;
        case 'exchange/massUpdate':
            $response = $exchangeRates->massUpdate($_POST);
            break;
        default:
            $response = [
                'success' => false,
                'message' => 'Действие не определено!',
            ];
            break;
    }

    exit(json_encode($response));
}


if ($modx->getAuthenticatedUser('mgr')) {
    switch ($action) {
        case 'courses/list':
            if (isset($_POST['cities']) && $_POST['cities'] == true) {
                /**
                 * returns JSON
                 */
                $response = $exchangeRates->api_getListCities();
            } elseif (isset($_POST['city']) && ((int)$_POST['city'] > 0)) {
                $city = (int)$_POST['city'];
                if ($city > 0) {
                    $response = $exchangeRates->api_getListExchange($city);
                }
            }
            break;
        case 'courses/get':
            if (isset($_POST['exchange']) && ((int)$_POST['exchange'] > 0)) {
                $id = (int)$_POST['exchange'];

                $response = $exchangeRates->api_getExchangeCourses($id);
            }
            break;
        case 'courses/update':
            if (isset($_POST['data'])) {
                $data = $modx->fromJSON($_POST['data']);
                if ((int)$data['id'] > 0) {
                    $response = $exchangeRates->api_setExchangeCourses($data);
                }
            }
            break;
        case 'courses/delete':
            $result = $exchangeRates->deleteExchangePoint($_POST);
            $response = json_encode([
                'success' => $result['success'],
                'message' => $result['data']['message'],
            ]);
            break;
        default:
            $response = json_encode([
                'success' => false,
                'message' => 'Действие не определено!',
            ]);
            break;
    }
}

exit($response);